package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenBanana extends WorldGenTree {

	public WorldGenBanana(ITree tree) {
		super(tree);
	}

	@Override
	public void generate() {
		
		bushiness = .9f;
		
		generateTreeTrunk(height, girth);

		int leafSpawn = height+1;
		
		float width = this.height / randBetween(1.8f, 2.0f);
		int bottom = randBetween(3, 4);
	
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.9f*width, 1, leaf, false);

	}

	@Override
	public void preGenerate() {
		height = determineHeight(6, 1);
		girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
	}

}
