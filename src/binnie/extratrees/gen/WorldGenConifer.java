package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenConifer {
	

	public static class WesternHemlock extends WorldGenTree {
		public WesternHemlock(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 6;
			float bottom = randBetween(2, 3);
			float width = height/randBetween(2f, 2.5f);
			if(width > 7) width = 7;
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(7, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class Cypress extends WorldGenTree {
		public Cypress(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = 1;
			float width = height*randBetween(0.15f, 0.2f);
			if(width > 7) width = 7;
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius *= width-1;
				radius += 1;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class Yew extends WorldGenTree {
		public Yew(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = randBetween(1, 2);
			float width = height*randBetween(0.7f, 0.75f);
			if(width > 7) width = 7;
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius *= 2f-radius;
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(5, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class Cedar extends WorldGenTree {
		public Cedar(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 3;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.7f, 0.75f);
			if(width > 7) width = 7;
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), girth, 1, leaf, false);
			}
			
			leafSpawn = height + 3;
			
			while(leafSpawn>bottom) {
				float cone = 1f-((leafSpawn-bottom)/coneHeight);
				float radius = (0.7f + rand.nextFloat() * 0.3f) * width;
				float xOffset = (-width + rand.nextFloat() * 2 * width)/2f;
				float yOffset = (-width + rand.nextFloat() * 2 * width)/2f;
				cone *= 2f - cone;
				xOffset *= cone;
				yOffset *= cone;
				radius *= cone;
				if(radius < 2) radius = 2f;
				if(xOffset > radius/2) xOffset = radius/2;
				if(yOffset > radius/2) yOffset = radius/2;
				generateCylinder(new Vector(xOffset, leafSpawn--, yOffset), 0.7f*radius, 1, leaf, false);
				generateCylinder(new Vector(xOffset, leafSpawn--, yOffset), radius, 1, leaf, false);
				generateCylinder(new Vector(xOffset, leafSpawn--, yOffset), 0.5f*radius, 1, leaf, false);
			
				leafSpawn += 1 + rand.nextInt(2);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class LoblollyPine extends WorldGenTree {
		public LoblollyPine(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = height*randBetween(0.65f, 0.7f);
			float width = height*randBetween(0.25f, 0.3f);
			if(width > 7) width = 7;

			generateCylinder(new Vector(0, leafSpawn--, 0), 0.6f*width, 1, leaf, false);
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	
	
	public static class MonkeyPuzzle extends WorldGenTree {
		public MonkeyPuzzle(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.4f, 0.45f);
			if(width > 7) width = 7;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.35f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.55f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.75f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.9f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 1f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.5f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(9, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
}
