package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenCitrus extends WorldGenTree {

	public WorldGenCitrus(ITree tree) {
		super(tree);
	}

	@Override
	public void generate() {
		generateTreeTrunk(height, girth);

		int leafSpawn = height;
		
		float width = this.height / randBetween(1.1f, 1.5f);
		int bottom = randBetween(1, 2);
	
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		while (leafSpawn > bottom) {
			generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
		}
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
	}

	@Override
	public void preGenerate() {
		minHeight = randBetween(2, 3);
		height = determineHeight(6, 1);
		girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
	}

}
