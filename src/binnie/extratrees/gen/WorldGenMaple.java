package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenMaple {
	
	public static class RedMaple extends WorldGenTree {
		public RedMaple(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = randBetween(1, 2);
			float bottom2 = (height + bottom) / 2;
			float width = 2f;
			
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
			while(leafSpawn > bottom2) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.4f, 1, leaf, false);
			}
			width += 0.6;
			while(leafSpawn > bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.4f, 1, leaf, false);
			}
			if(leafSpawn == bottom)
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.6f, 1, leaf, false);
			
		}
		
		
		public void preGenerate() {
			height = determineHeight(5, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}

}
