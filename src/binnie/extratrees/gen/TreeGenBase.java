package binnie.extratrees.gen;

import java.util.Random;

import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import forestry.api.arboriculture.ITree;
import forestry.api.world.ITreeGenData;

public class TreeGenBase extends WorldGenerator {

	protected ITree tree;
	protected ITreeGenData treeGen;
	protected World world;
	protected Random rand;
	
	protected int startX;
	protected int startY;
	protected int startZ;

	protected int girth;
	protected int height;
	
	protected int minHeight = 4;
	protected int maxHeight = 80;
	
	protected boolean spawnPods = false;
	protected int minPodHeight = 3;
	
	protected final int randBetween(int a, int b) {
		return a + rand.nextInt(b-a);
	}
	protected final float randBetween(float a, float b) {
		return a + rand.nextFloat()*(b-a);
	}
	
	@Override
	public boolean generate(World world, Random random, int x, int y, int z) {
		
		this.world = world;
		startX = x;
		startY = y;
		startZ = z;
		
		girth = tree.getGirth(world, x, y, z);
		height = (int)( (float)randBetween(getHeight()[0], getHeight()[1]) * tree.getGenome().getHeight());
		
		if(tree.canGrow(world, x, y, z, girth, height))
			generate();
		else
			return false;
		return true;
	}
	
	
	protected void generate() {
		
	}
	
	
	protected int[] getHeight() {
		return new int[] {5, 2};
	}



	private static class Vector {
		int x;
		int y;
		int z;
		public Vector(int x, int y, int z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
	
	


}