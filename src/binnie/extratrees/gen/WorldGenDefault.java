package binnie.extratrees.gen;

import binnie.extratrees.genetics.ExtraTreeSpecies;
import forestry.api.arboriculture.ITree;

public class WorldGenDefault extends WorldGenTree {

	public WorldGenDefault(ITree tree) {
		super(tree);
	}

	@Override
	public void generate() {
		generateTreeTrunk(height, girth);

		int leafSpawn = height + 1;

		generateCylinder(new Vector(0, leafSpawn--, 0), 1, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);

		generateCylinder(new Vector(0, leafSpawn--, 0), 2.9f, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 2.9f, 1, leaf, false);
	}

	@Override
	public void preGenerate() {
		height = determineHeight(5, 2);
		girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
	}

	@Override
	public BlockType getLeaf() {
		return new BlockTypeLeaf();
	}

	@Override
	public BlockType getWood() {
		return new BlockTypeLog(((ExtraTreeSpecies)tree.getGenome().getPrimary()).getLog());
	}

}
