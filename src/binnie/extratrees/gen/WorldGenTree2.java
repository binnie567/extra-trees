package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenTree2 {
	
	public static class Olive extends WorldGenTree {
		public Olive(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.35f, 0.4f);
			if(width < 1.2) width = 1.55f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1f, 1, leaf, false);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.5f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(4, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Sweetgum extends WorldGenTree {
		public Sweetgum(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(1, 2);
			float width = height*randBetween(0.7f, 0.75f);
			if(width > 7) width = 7;
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius *= 2f-radius;
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(5, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Locust extends WorldGenTree {
		public Locust(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height+1;
			float bottom = 2;
			float width = height*randBetween(0.35f, 0.4f);
			if(width < 2f) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1f, 1, leaf, false);
			while(leafSpawn>bottom+1) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.3f, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.7f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.4f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Pear extends WorldGenTree {
		public Pear(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height;
			float bottom = randBetween(1, 2);
			float width = height*randBetween(0.25f, 0.3f);
			if(width < 2f) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1f, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
		}
		public void preGenerate() {
			height = determineHeight(3, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	
	
	
	
	
	
	
	
	
	public static class Iroko extends WorldGenTree {
		public Iroko(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.45f, 0.5f);
			if(width < 2.5f) width = 2.5f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width*0.25f, 1, leaf, false);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width*0.5f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width*0.7f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-2.0f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Gingko extends WorldGenTree {
		public Gingko(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = 2;
			float width = height*randBetween(0.55f, 0.6f);
			if(width > 7) width = 7;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-2.0f, 1, leaf, false);
			
			while(leafSpawn>bottom*2 + 1) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width-1.5f, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.9f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.9f, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Box extends WorldGenTree {
		public Box(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height;
			float bottom = 0;
			float width = 1.5f;
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
		}
		public void preGenerate() {
			height = determineHeight(3, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Clove extends WorldGenTree {
		public Clove(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height;
			float bottom = 2;
			float width = height*randBetween(0.25f, 0.3f);
			if(width < 2f) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1f, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.6f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.4f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(4, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}

}
