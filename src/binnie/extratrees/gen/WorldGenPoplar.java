package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenPoplar {
	
	public static class Aspen extends WorldGenTree {
		public Aspen(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height;
			float bottom = randBetween(height/2, height/2 + 1)+1;
			float width = height*randBetween(0.25f, 0.35f);
			if(width < 2f) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.5f*width, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.9f, 1.1f)*width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(5, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}

}
