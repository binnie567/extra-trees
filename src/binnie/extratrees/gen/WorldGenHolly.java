package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenHolly {
	
	public static class Holly extends WorldGenTree {
		public Holly(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = 1;
			float width = height*randBetween(0.4f, 0.45f);
			if(width < 1.5f) width = 1.5f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), width=1f, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(4, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}

}
