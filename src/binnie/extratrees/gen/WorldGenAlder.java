package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenAlder {
	
	public static class CommonAlder extends WorldGenTree {
		public CommonAlder(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(1, 2);
			float width = height*randBetween(0.4f, 0.45f);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.4f, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(5, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}

}
