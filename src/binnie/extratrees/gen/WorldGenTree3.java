package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenTree3 {
	
	public static class Hazel extends WorldGenTree {
		public Hazel(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height+1;
			float bottom = 3;
			float width = height*randBetween(0.45f, 0.5f);
			if(width < 2.5f) width = 2.5f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1f, 1, leaf, false);
			
			while(leafSpawn>bottom)
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.3f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.6f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1.2f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(5, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Sycamore extends WorldGenTree {
		public Sycamore(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.35f, 0.4f);
			if(width < 2) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1.3f, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.6f, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.8f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Hawthorn extends WorldGenTree {
		public Hawthorn(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(1,2);
			float width = height*randBetween(0.4f, 0.45f);
			if(width<1.5f) width = 1.5f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.5f, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(4, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Pecan extends WorldGenTree {
		public Pecan(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height+1;
			float bottom = 2;
			float width = height*randBetween(0.6f, 0.65f);
			if(width < 2) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.6f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.9f*width, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.6f*width, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Elm extends WorldGenTree {
		public Elm(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height+1;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.45f, 0.5f);
			if(width < 2f) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1f, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.5f, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.5f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public static class Elder extends WorldGenTree {
		public Elder(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height+1;
			float bottom = 3;
			float width = height*randBetween(0.55f, 0.6f);
			if(width < 2f) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1.0f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.4f, 1, leaf, false);
			
			while(leafSpawn>bottom)
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.4f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1.0f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(4, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Hornbeam extends WorldGenTree {
		public Hornbeam(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = 2;
			float width = height*randBetween(0.55f, 0.6f);
			if(width<1.5f) width = 1.5f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
		}
		public void preGenerate() {
			height = determineHeight(5, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class Sallow extends WorldGenTree {
		public Sallow(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height+1;
			float bottom = 2;
			float width = height*randBetween(0.6f, 0.65f);
			if(width < 2) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.6f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.9f*width, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.6f*width, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(4, 1);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class AcornOak extends WorldGenTree {
		public AcornOak(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height+1;
			float bottom = 3;
			float width = height*randBetween(0.45f, 0.5f);
			if(width < 2f) width = 2f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width*0.4f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), width*0.7f, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.2f, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-0.5f, 1, leaf, false);
		}
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}

}
