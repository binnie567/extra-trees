package binnie.extratrees.gen;

import net.minecraft.world.World;
import forestry.api.arboriculture.ITree;

class BlockType {

	int meta;
	int id;

	public BlockType(int id, int meta) {
		this.id = id;
		this.meta = meta;
	}

	public void setBlock(World world, ITree tree, int x, int y, int z) {
		world.setBlock(x, y, z, id, meta, 2);
	}
}
