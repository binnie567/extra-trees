package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenFir  {
	
	public static class DouglasFir extends WorldGenTree {
		public DouglasFir(ITree tree) {
			super(tree);
		}

		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float patchyBottom = height / 2;
			float bottom = randBetween(3, 4);
			float width = height*randBetween(0.35f, 0.4f);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			bushiness = 0.1f;
			while(leafSpawn>patchyBottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.9f, 1.1f)*width, 1, leaf, false);
			}
			bushiness = 0.5f;
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.7f, 1f)*width, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
		}

		@Override
		public void preGenerate() {
			height = determineHeight(7, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class SilverFir extends WorldGenTree {
		public SilverFir(ITree tree) {
			super(tree);
		}
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = randBetween(2, 3);
			float width = height/randBetween(2.5f, 3f);
			if(width > 7) width = 7;
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(7, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}
	
	public static class BalsamFir extends WorldGenTree {
		public BalsamFir(ITree tree) {
			super(tree);
		}
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = 1;
			float width = height/randBetween(2f, 2.5f);
			if(width > 7) width = 7;
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}

}
