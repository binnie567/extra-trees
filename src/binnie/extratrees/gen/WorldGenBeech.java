package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenBeech {
	
	public static class CommonBeech extends WorldGenTree {
		public CommonBeech(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.47f, 0.5f);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.5f, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), width-1, 1, leaf, false);
		}
		
	}
	
	public static class CopperBeech extends CommonBeech {
		public CopperBeech(ITree tree) { super(tree); }
	}

}
