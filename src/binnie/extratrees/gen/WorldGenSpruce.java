package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenSpruce {
	
	public static class GiantSpruce extends WorldGenTree {
		public GiantSpruce(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 3;
			float bottom = randBetween(3, 4);
			float width = height/randBetween(2.5f, 3f);
			
			float coneHeight = leafSpawn - bottom;

			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius = 0.15f + 0.85f*radius;
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(15, 4);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class AlpineSpruce extends WorldGenTree {
		public AlpineSpruce(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 5;
			float bottom = randBetween(2, 3);
			float width = height/randBetween(2f, 2.5f);
			
			float coneHeight = leafSpawn - bottom;

			leafSpawn -= 2;
			
			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius *= radius;
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(5, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	
	public static class WhiteSpruce extends WorldGenTree {
		public WhiteSpruce(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = randBetween(2, 3);
			float width = height/randBetween(2.2f, 2.5f);
			
			float coneHeight = leafSpawn - bottom;
			
			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius = (float) Math.sqrt(radius);
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class BlackSpruce extends WorldGenTree {
		public BlackSpruce(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float bottom = randBetween(2, 3);
			float width = height/randBetween(2.2f, 2.5f);
			
			float coneHeight = leafSpawn - bottom;
			
			while(leafSpawn>bottom) {
				float radius = 1f-((leafSpawn-bottom)/coneHeight);
				radius = (float) Math.sqrt(radius);
				radius *= width;
				generateCylinder(new Vector(0, leafSpawn--, 0), radius, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
}
