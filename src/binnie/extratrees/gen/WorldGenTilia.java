package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenTilia {
	
	public static class Basswood extends WorldGenTree {
		public Basswood(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.4f, 0.5f);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.95f, 1.05f)*width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class WhiteBasswood extends WorldGenTree {
		public WhiteBasswood(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.4f, 0.5f);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.95f, 1.05f)*width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(6, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class CommonLime extends WorldGenTree {
		public CommonLime(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.45f, 0.55f);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.95f, 1.05f)*width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(7, 4);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}

}
