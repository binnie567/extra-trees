package binnie.extratrees.gen;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import binnie.extratrees.genetics.ExtraTreeSpecies;
import forestry.api.arboriculture.ITree;
import forestry.api.world.ITreeGenData;

public abstract class WorldGenTree extends WorldGenerator {

	protected ITree tree;
	protected ITreeGenData treeGen;
	protected World world;
	protected Random rand;
	
	protected int startX;
	protected int startY;
	protected int startZ;

	protected int girth;
	protected int height;
	
	protected int minHeight = 3;
	protected int maxHeight = 80;
	
	protected boolean spawnPods = false;
	protected int minPodHeight = 3;
	
	// Random Functions
	
	protected int randBetween(int a, int b) {
		return a+rand.nextInt(b-a);
	}
	
	protected float randBetween(float a, float b) {
		return a+rand.nextFloat()*(b-a);
	}
	
	public void generate() {
		generateTreeTrunk(height, girth);

		int leafSpawn = height + 1;

		generateCylinder(new Vector(0, leafSpawn--, 0), 1, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);

		generateCylinder(new Vector(0, leafSpawn--, 0), 2.9f, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 2.9f, 1, leaf, false);

	}

	public boolean canGrow() {
		return tree.canGrow(world, startX, startY, startZ, tree.getGirth(world, startX, startY, startZ), height);
	}

	public void preGenerate() {
		height = determineHeight(5, 3);
		girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
	}

	protected int determineGirth(int base) {
		return base;
	}
	
	protected int modifyByHeight(int val, int min, int max) {
		int determined = Math.round(val * treeGen.getHeightModifier());
				//* TreeManager.breedingManager.getTreekeepingMode(world).getHeightModifier(null));
		return determined < min ? min : determined > max ? max : determined;
	}
	
	protected int determineHeight(int required, int variation) {
		int determined = Math.round((required + rand.nextInt(variation)) * treeGen.getHeightModifier());// *
				//TreeManager.breedingManager.getTreekeepingMode(world).getHeightModifier(null));
		return determined < minHeight ? minHeight : determined > maxHeight ? maxHeight : determined;
	}
	
	public BlockType getLeaf() {
		return new BlockTypeLeaf();
	}

	public BlockType getWood() {
		return new BlockTypeLog(((ExtraTreeSpecies)tree.getGenome().getPrimary()).getLog());
	}

	public WorldGenTree(ITree tree) {
		this.tree = tree;
		
		this.spawnPods = ((ITreeGenData)tree).allowsFruitBlocks();
		
		if(tree instanceof ITreeGenData)
			treeGen = (ITreeGenData) tree;
	}

	@Override
	public final boolean generate(World world, Random random, int x, int y, int z) {

		this.world = world;
		this.rand = random;
		this.startX = x;
		this.startY = y;
		this.startZ = z;
		
		this.leaf = getLeaf();
		this.wood = getWood();
		
		preGenerate();
		if (!canGrow())
			return false;
		else {
			generate();
			return true;
		}

	}

	static class Vector {
		public Vector(float f, float h, float g) {
			this.x = f;
			this.y = h;
			this.z = g;
		}

		float x;
		float y;
		float z;

		public static double distance(Vector a, Vector b) {
			return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2) + Math.pow(a.z - b.z, 2));
		}
	}

	BlockType leaf;
	BlockType wood;
	BlockType vine = new BlockType(Block.vine.blockID, 0);
	BlockType air = new BlockTypeVoid();

	public final Vector getStartVector() {
		return new Vector(startX, startY, startZ);
	}

	protected void generateTreeTrunk(int height, int width) {
		generateTreeTrunk(height, width, 0);
	}
	
	protected void generateTreeTrunk(int height, int width, float vines) {
		int offset = (width - 1) / 2;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < width; y++) {
				for (int i = 0; i < height; i++) {
					addWood(x - offset, i, y - offset, true);
					
					if(rand.nextFloat() < vines)
						addVine(x - offset - 1, i, y - offset);
					if(rand.nextFloat() < vines)
						addVine(x - offset + 1, i, y - offset);
					if(rand.nextFloat() < vines)
						addVine(x - offset, i, y - offset - 1);
					if(rand.nextFloat() < vines)
						addVine(x - offset, i, y - offset + 1);
				}
			}
		}
		
		if(!spawnPods)
			return;
		
		for(int y = minPodHeight; y < height; y++) {
			
			for (int x = 0; x < girth; x++) {
				for (int z = 0; z < girth; z++) {

					if((x > 0 && x < girth) && (z > 0 && z < girth))
						continue;

					treeGen.trySpawnFruitBlock(world, startX + x + 1, startY + y, startZ + z);
					treeGen.trySpawnFruitBlock(world, startX + x - 1, startY + y, startZ + z);
					treeGen.trySpawnFruitBlock(world, startX + x, startY + y, startZ + z + 1);
					treeGen.trySpawnFruitBlock(world, startX + x, startY + y, startZ + z - 1);
				}
			}
		}
	}

	protected void generateSupportStems(int height, int girth, float chance, float maxHeight) {
		
		int offset = 1;
		
		for (int x = - offset; x < girth + offset; x++) {
			for (int z = - offset; z < girth + offset; z++) {
				
				if(x == -offset && z == -offset)
					continue;
				if(x == girth+offset && z == girth+offset)
					continue;
				if(x == -offset && z == girth+offset)
					continue;
				if(x == girth+offset && z == -offset)
					continue;
				
				int stemHeight = rand.nextInt(Math.round(height*maxHeight));
				if(rand.nextFloat() < chance) {
					for (int i = 0; i < stemHeight; i++) {
						addWood(x, i, z, true);
					}
				}
			}
		}
		
	}
	
	protected final void clearBlock(int x, int y, int z) {
		air.setBlock(world, tree, startX + x, startY + y, startZ + z);
	}
	
	protected final void addBlock(int x, int y, int z, BlockType type, boolean doReplace) {
		if (doReplace || world.isAirBlock(startX + x, startY + y, startZ + z)) {
			type.setBlock(world, tree, startX + x, startY + y, startZ + z);
		}
	}

	protected final void addWood(int x, int y, int z, boolean doReplace) {
		addBlock(x, y, z, wood, doReplace);
	}

	protected final void addLeaf(int x, int y, int z, boolean doReplace) {
		addBlock(x, y, z, leaf, doReplace);
	}

	protected final void addVine(int x, int y, int z) {
		addBlock(x, y, z, vine, false);
	}
	
	protected final void generateCuboid(Vector start, Vector area, BlockType block, boolean doReplace) {
		for (int x = (int) start.x; x < (int) start.x + area.x; x++) {
			for (int y = (int) start.y; y < (int) start.y + area.y; y++) {
				for (int z = (int) start.z; z < (int) start.z + area.z; z++) {
					addBlock(x, y, z, block, doReplace);
				}
			}
		}
	}
	
	float bushiness = 0f;

	/*
	 * Center is the bottom middle of the cylinder
	 */
	protected final void generateCylinder(Vector center2, float radius, int height, BlockType block, boolean doReplace) {
		float centerOffset = ((float)(this.girth - 1))/2f;
		Vector center = new Vector(center2.x + centerOffset, center2.y, center2.z + centerOffset);
		Vector start = new Vector(center.x - radius, center.y, center.z - radius);
		Vector area = new Vector(radius * 2 + 1, height, radius * 2 + 1);
		for (int x = (int) start.x; x < (int) start.x + area.x; x++) {
			for (int y = (int) start.y; y < (int) start.y + area.y; y++) {
				for (int z = (int) start.z; z < (int) start.z + area.z; z++) {
					if (Vector.distance(new Vector(x, y, z), new Vector(center.x, y, center.z)) <= (radius) + 0.01) {
						if(Vector.distance(new Vector(x, y, z), new Vector(center.x, y, center.z)) >= (radius) -0.5f)
							if(rand.nextFloat()<bushiness) continue;
						addBlock(x, y, z, block, doReplace);
					}
				}
			}
		}
	}

	protected final void generateCircle(Vector center, float radius, int width, int height, BlockType block, boolean doReplace) {
		generateCircle(center, radius, width, height, block, 1.0f, doReplace);
	}
	
	protected final void generateCircle(Vector center2, float radius, int width, int height, BlockType block, float chance, boolean doReplace) {
		float centerOffset = this.girth % 2 == 0 ? 0.5f : 0f;
		Vector center = new Vector(center2.x + centerOffset, center2.y, center2.z + centerOffset);
		
		Vector start = new Vector(center.x - radius, center.y, center.z - radius);
		Vector area = new Vector(radius * 2 + 1, height, radius * 2 + 1);
		
		for (int x = (int) start.x; x < (int) start.x + area.x; x++) {
			for (int y = (int) start.y; y < (int) start.y + area.y; y++) {
				for (int z = (int) start.z; z < (int) start.z + area.z; z++) {
					
					if(rand.nextFloat() > chance)
						continue;
					
					double distance = Vector.distance(new Vector(x, y, z), new Vector(center.x, y, center.z));
					if ((radius - width - 0.01 < distance && distance <= (radius) + 0.01)) {
						addBlock(x, y, z, block, doReplace);
					}
				}
			}
		}
	}
	
	protected final void generateSphere(Vector center2, int radius, BlockType block, boolean doReplace) {
		float centerOffset = ((float)(this.girth - 1))/2f;
		Vector center = new Vector(center2.x + centerOffset, center2.y, center2.z + centerOffset);
		
		Vector start = new Vector(center.x - radius, center.y - radius, center.z - radius);
		Vector area = new Vector(radius * 2 + 1, radius * 2 + 1, radius * 2 + 1);
		for (int x = (int) start.x; x < (int) start.x + area.x; x++) {
			for (int y = (int) start.y; y < (int) start.y + area.y; y++) {
				for (int z = (int) start.z; z < (int) start.z + area.z; z++) {
					if (Vector.distance(new Vector(x, y, z), new Vector(center.x, center.y, center.z)) <= (radius) + 0.01) {
						addBlock(x, y, z, block, doReplace);
					}
				}
			}
		}
	}

}
