package binnie.extratrees.gen;

import binnie.extratrees.block.ILogType;
import forestry.api.arboriculture.ITree;

public class WorldGenEucalyptus {
	
	public static class SwampGum extends WorldGenTree {
		public SwampGum(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 2;
			float weakerBottm = randBetween(0.5f, 0.6f) * height;
			float bottom = randBetween(0.4f, 0.5f)*height;
			float width = height*randBetween(0.15f, 0.2f);
			if(width > 7) width = 7;
			
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			bushiness = 0.3f;
			while(leafSpawn > weakerBottm) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.9f, 1.0f)*width, 1, leaf, false);
			}
			bushiness = 0.6f;
			while(leafSpawn > bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.8f, 0.9f)*width, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.5f*width, 1, leaf, false);
			for(int i = 0; i < 5; i++) {
				generateSphere(new Vector(randBetween(-1, 1), leafSpawn--, randBetween(-1, 1)), randBetween(1, 2), leaf, false);
			}
			
		}
		
		
		public void preGenerate() {
			height = determineHeight(14, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class RoseGum extends WorldGenTree {
		public RoseGum(ITree tree) { super(tree); }
		
		public void generate() {
			
			generateTreeTrunk(height, girth);
			
			int offset = (girth - 1) / 2;
			for (int x = 0; x < girth; x++) {
				for (int y = 0; y < girth; y++) {
					for (int i = 0; i < 2; i++) {
						addBlock(x - offset, i, y - offset, new BlockTypeLog(ILogType.ExtraTreeLog.Eucalyptus2), true);
					}
				}
			}
			
			

			float leafSpawn = height + 2;
			float bottom = randBetween(0.4f, 0.5f)*height;
			float width = height*randBetween(0.05f, 0.1f);
			if(width < 1.5f) width = 1.5f;
			
			bushiness = 0.5f;
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			while(leafSpawn > bottom) {
				bushiness = 0.1f;
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.9f, 1.1f)*width, 1, leaf, false);
			}
		
		}
		
		
		public void preGenerate() {
			height = determineHeight(9, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	public static class RainbowGum extends WorldGenTree {
		public RainbowGum(ITree tree) { super(tree); }
		
		public void generate() {
			
			generateTreeTrunk(height, girth);
			
			float leafSpawn = height + 2;
			float bottom = randBetween(0.5f, 0.6f)*height;
			float width = height*randBetween(0.15f, 0.2f);
			if(width < 1.5f) width = 1.5f;
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
			while(leafSpawn > bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
				generateCylinder(new Vector(0, leafSpawn--, 0), width-0.5f, 1, leaf, false);
			}
		
		}
		
		
		public void preGenerate() {
			height = determineHeight(7, 3);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}
	
	
}
