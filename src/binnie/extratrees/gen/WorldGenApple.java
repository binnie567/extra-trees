package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenApple {
	
	public static class OrchardApple extends WorldGenTree {

		public OrchardApple(ITree tree) {
			super(tree);
		}

		@Override
		public void generate() {
			generateTreeTrunk(height, girth);

			int leafSpawn = height;
		
			generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);
			while (leafSpawn > 2) {
				generateCylinder(new Vector(0, leafSpawn--, 0), 2.5f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), 2f, 1, leaf, false);

		}

		@Override
		public void preGenerate() {
			height = determineHeight(3, 6);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}

	}
	
	public static class SweetCrabapple extends WorldGenTree {

		public SweetCrabapple(ITree tree) {
			super(tree);
		}

		@Override
		public void generate() {
			generateTreeTrunk(height, girth);

			int leafSpawn = height;
		
			generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 2.5f, 1, leaf, false);
			while (leafSpawn > 3) {
				generateCylinder(new Vector(0, leafSpawn--, 0), 3f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), 3.5f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);
		}

		@Override
		public void preGenerate() {
			height = determineHeight(4, 4);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}

	}

	public static class FloweringCrabapple extends WorldGenTree {

		public FloweringCrabapple(ITree tree) {
			super(tree);
		}

		@Override
		public void generate() {
			generateTreeTrunk(height, girth);

			int leafSpawn = height;
		
			generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 3f, 1, leaf, false);
			while (leafSpawn > 3) {
				generateCylinder(new Vector(0, leafSpawn--, 0), 4f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), 2f, 1, leaf, false);
		}

		@Override
		public void preGenerate() {
			height = determineHeight(3, 6);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}

	}
	
	
	public static class PrairieCrabapple extends WorldGenTree {

		public PrairieCrabapple(ITree tree) {
			super(tree);
		}

		@Override
		public void generate() {
			generateTreeTrunk(height, girth);

			int leafSpawn = height;
		
			generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 2.5f, 1, leaf, false);
			while (leafSpawn > 3) {
				generateCylinder(new Vector(0, leafSpawn--, 0), 3f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), 3.0f, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 2f, 1, leaf, false);
		}

		@Override
		public void preGenerate() {
			height = determineHeight(4, 4);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}

	}



}
