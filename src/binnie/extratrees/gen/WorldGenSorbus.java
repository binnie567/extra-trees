package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenSorbus {
	
	public static class Whitebeam extends WorldGenTree {
		public Whitebeam(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			int leafSpawn = height + 1;
			
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.5f, 0.6f);

			generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f * width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.6f * width, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f * width, 1, leaf, false);
			
			while(leafSpawn > bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.95f, 1.05f) * width, 1, leaf, false);
			}

			generateCylinder(new Vector(0, leafSpawn--, 0), .7f * width, 1, leaf, false);

		}
	}
	
	public static class Rowan extends WorldGenTree {
		public Rowan(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			int leafSpawn = height + 1;
			
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.5f, 0.6f);

			generateCylinder(new Vector(0, leafSpawn--, 0), 0.5f * width, 1, leaf, false);
			
			while(leafSpawn > bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.95f, 1.05f) * width, 1, leaf, false);
			}

			generateCylinder(new Vector(0, leafSpawn--, 0), .7f * width, 1, leaf, false);

		}
		
	}
	
	public static class ServiceTree extends WorldGenTree {
		public ServiceTree(ITree tree) { super(tree); }
		public void generate() {
			generateTreeTrunk(height, girth);

			int leafSpawn = height + 1;

			generateCylinder(new Vector(0, leafSpawn--, 0), 1, 1, leaf, false);
			generateCylinder(new Vector(0, leafSpawn--, 0), 1.5f, 1, leaf, false);

			while(leafSpawn>2) {
				generateCylinder(new Vector(0, leafSpawn--, 0), 2.4f+rand.nextFloat()*0.7f, 1, leaf, false);
			}
			generateCylinder(new Vector(0, leafSpawn--, 0), 2.9f, 1, leaf, false);

		}

		public void preGenerate() {
			height = determineHeight(8, 6);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
	}

}
