package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenPrune extends WorldGenTree {

	public WorldGenPrune(ITree tree) {
		super(tree);
	}

	@Override
	public void generate() {
		generateTreeTrunk(height, girth);

		int leafSpawn = height;
		
		float width = this.height / randBetween(1.7f, 2.1f);
		int bottom = randBetween(2, 3);
	
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.4f*width, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		while (leafSpawn > bottom) {
			generateCylinder(new Vector(0, leafSpawn--, 0), width, 1, leaf, false);
		}
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.6f*width, 1, leaf, false);
		generateCylinder(new Vector(0, leafSpawn--, 0), 0.3f*width, 1, leaf, false);
	}

	@Override
	public void preGenerate() {
		height = determineHeight(6, 2);
		girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
	}

}
