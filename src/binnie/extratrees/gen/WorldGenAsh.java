package binnie.extratrees.gen;

import forestry.api.arboriculture.ITree;

public class WorldGenAsh {
	
	public static class CommonAsh extends WorldGenTree {
		public CommonAsh(ITree tree) { super(tree); }
		
		public void generate() {
			generateTreeTrunk(height, girth);

			float leafSpawn = height + 1;
			float bottom = randBetween(2, 3);
			float width = height*randBetween(0.35f, 0.45f);
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.8f*width, 1, leaf, false);
			
			while(leafSpawn>bottom) {
				generateCylinder(new Vector(0, leafSpawn--, 0), randBetween(0.95f, 1.05f)*width, 1, leaf, false);
			}
			
			generateCylinder(new Vector(0, leafSpawn--, 0), 0.7f*width, 1, leaf, false);
		}
		
		
		public void preGenerate() {
			height = determineHeight(5, 2);
			girth = determineGirth(tree.getGirth(world, startX, startY, startZ));
		}
		
	}

}
