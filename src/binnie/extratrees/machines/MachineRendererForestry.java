package binnie.extratrees.machines;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import net.minecraftforge.common.ForgeDirection;

public class MachineRendererForestry {
	
	static Map<String, Object> instances = new HashMap<String, Object>();
	
	static Method renderMethod;
	
	private static void loadMethod(String file, boolean waterTank, boolean productTank) {
		try {
			Class clss = Class.forName("forestry.core.render.RenderMachine");
			Object instance = clss.getConstructor(
					new Class[] {String.class})
					.newInstance(
							new Object[] {file});
			renderMethod = clss.getMethod("render", new Class[] {
					int.class, int.class, ForgeDirection.class,
					double.class, double.class, double.class
			});
			instances.put(file, instance);
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	public static void renderMachine(String name, double x, double y, double z, float var8) {
		
		if(!instances.containsKey(name))
			loadMethod(name, false, false);
		
		try {
			renderMethod.invoke(instances.get(name), new Object[] {
					0, 0, ForgeDirection.UP, x, y, z
			});
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

}
