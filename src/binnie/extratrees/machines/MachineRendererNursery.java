package binnie.extratrees.machines;

import org.lwjgl.opengl.GL11;

import binnie.core.BinnieCore;
import binnie.core.resource.BinnieResource;

public class MachineRendererNursery {

	public static MachineRendererNursery instance = new MachineRendererNursery();
	
	BinnieResource texture;

	public MachineRendererNursery() {
		model = new ModelNursery();
	}

	private ModelNursery model;

	public void renderMachine(BinnieResource texture, double x, double y, double z,
			float var8) {

		this.texture = texture;

		GL11.glPushMatrix();

		GL11.glTranslated(x + 0.5, y + 1.5, z + 0.5);
		GL11.glRotatef(180, 0f, 0f, 1f);

		BinnieCore.proxy.bindTexture(texture);

		GL11.glPushMatrix();

		model.render(null, (float) x, (float) y, (float) z, 0.0625f, 0.0625f, 0.0625f);

		GL11.glPopMatrix();

		GL11.glPopMatrix();

	}

}
