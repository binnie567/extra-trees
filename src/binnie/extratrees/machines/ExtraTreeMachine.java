package binnie.extratrees.machines;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import binnie.core.machines.IMachineType;
import binnie.core.machines.Machine;
import binnie.core.machines.MachineComponent;
import binnie.core.machines.MachinePackage;
import binnie.core.machines.TileEntityMachine;
import binnie.core.machines.component.IInteraction;
import binnie.core.resource.BinnieResource;
import binnie.core.resource.ResourceManager;
import binnie.core.resource.ResourceType;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.core.ExtraTreesGUID;

public enum ExtraTreeMachine implements IMachineType {
	
	Lumbermill(Lumbermill.PackageLumbermill.class),
	Woodworker(Woodworker.PackageWoodworker.class),
	Panelworker(Woodworker.PackagePanelworker.class),
	Nursery(Nursery.PackageNursery.class),
	
	;
	Class<? extends MachinePackage> clss;
	
	
	private ExtraTreeMachine(Class<? extends MachinePackage> clss) {
		this.clss = clss;
	}
	
	@Override
	public Class<? extends MachinePackage> getPackageClass() {
		return clss;
	}

	public static class ComponentExtraTreeGUI extends MachineComponent implements IInteraction.RightClick {

		ExtraTreesGUID id;
		
		ComponentExtraTreeGUI(Machine machine, ExtraTreesGUID id) {
			super(machine);
			this.id = id;
		}

		@Override
		public void onRightClick(World world, EntityPlayer player, int x, int y, int z) {
			ExtraTrees.proxy.openGui(id, player,
					x, y, z);
		}
		
	}
	
	public abstract static class PackageExtraTreeMachine extends MachinePackage {
		
		BinnieResource textureName;

		protected PackageExtraTreeMachine(String uid, String name, String textureName) {
			super(uid, name);
			this.textureName = ResourceManager.getFile(ExtraTrees.instance, ResourceType.Tile, textureName);
		}
		
		protected PackageExtraTreeMachine(String uid, String name, BinnieResource textureName) {
			super(uid, name);
			this.textureName = textureName;
		}

		@Override
		public TileEntity createTileEntity() {
			return new TileEntityMachine(this);
		}

		@Override
		public void register() {
		}

		@Override
		public void renderMachine(Machine machine, double x, double y,
				double z, float var8) {
			MachineRendererForestry.renderMachine(
					textureName.getFullPath(), x, y, z, var8);
		}
		
	}

	@Override
	public boolean isActive() {
		return this != Nursery;
	}
	
	//static MachinePackage createCarpenterPackage() { return new PackageCarpenter(); }

}
