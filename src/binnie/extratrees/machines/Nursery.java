package binnie.extratrees.machines;

import net.minecraft.tileentity.TileEntity;
import binnie.core.machines.Machine;
import binnie.core.machines.inventory.ComponentInventorySlots;
import binnie.extratrees.core.ExtraTreeTexture;


public class Nursery {
	
	public static int slotCaterpillar = 0;
	
	public static class PackageNursery extends ExtraTreeMachine.PackageExtraTreeMachine {
		
		public PackageNursery() { super("nursery", "Nursery", ExtraTreeTexture.Nursery.getTexture()); }

		@Override
		public void createMachine(Machine machine) {
			
			ComponentInventorySlots inventory = new ComponentInventorySlots(machine);
			
			inventory.addSlot(slotCaterpillar, "Caterpillar Slot");
			//inventory.getSlot(slotCaterpillar).setValidator(new SlotValidatorCaterpillar());
		}

		@Override
		public TileEntity createTileEntity() { return new TileEntityNursery(this);}
		@Override
		public void renderMachine(Machine machine, double x, double y,
				double z, float var8) {
			MachineRendererNursery.instance.renderMachine(textureName, x, y, z, var8);
		}
		
	}
	
	
	
}
