package binnie.extratrees.machines;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import binnie.core.machines.TileEntityMachine;
import binnie.extratrees.machines.Nursery.PackageNursery;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.genetics.IIndividual;
import forestry.api.lepidopterology.IButterfly;
import forestry.api.lepidopterology.IButterflyNursery;

public class TileEntityNursery extends TileEntityMachine implements IButterflyNursery {

	public TileEntityNursery(PackageNursery pack) {
		super(pack);
	}

	IButterflyNursery getNursery() {
		return getMachine().getInterface(IButterflyNursery.class);
	}
	
	boolean hasNursery() {
		return getNursery() != null;
	}
	
	@Override
	public String getOwnerName() {
		return hasNursery() ? getNursery().getOwnerName() : "";
	}

	@Override
	public World getWorld() {
		return worldObj;
	}

	@Override
	public int getXCoord() {
		return xCoord;
	}

	@Override
	public int getYCoord() {
		return yCoord;
	}

	@Override
	public int getZCoord() {
		return zCoord;
	}

	@Override
	public int getBiomeId() {
		return 0;
	}

	@Override
	public EnumTemperature getTemperature() {
		return null;
	}

	@Override
	public EnumHumidity getHumidity() {
		return null;
	}

	@Override
	public void setErrorState(int state) {
	}

	@Override
	public int getErrorOrdinal() {
		return 0;
	}

	@Override
	public boolean addProduct(ItemStack product, boolean all) {
		return false;
	}

	@Override
	public IButterfly getCaterpillar() {
		return hasNursery() ? getNursery().getCaterpillar() : null;
	}

	@Override
	public IIndividual getNanny() {
		return null;
	}

	@Override
	public void setCaterpillar(IButterfly butterfly) {
		if(hasNursery()) getNursery().setCaterpillar(butterfly);
	}

	@Override
	public boolean canNurse(IButterfly butterfly) {
		return getCaterpillar() == null;
	}

}
