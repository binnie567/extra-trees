package binnie.extratrees.machines;

import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import binnie.core.machines.Machine;
import binnie.core.machines.component.ComponentRecipe;
import binnie.core.machines.component.IComponentRecipe;
import binnie.core.machines.inventory.ComponentInventorySlots;
import binnie.core.machines.network.INetwork;
import binnie.core.machines.power.ErrorState;
import binnie.core.machines.power.IErrorStateSource;
import binnie.craftgui.minecraft.IMachineInformation;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.api.ICarpentryWood;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.carpentry.BlockCarpentry;
import binnie.extratrees.carpentry.EnumDesign;
import binnie.extratrees.carpentry.ModuleCarpentry;
import binnie.extratrees.core.ExtraTreeTexture;
import binnie.extratrees.core.ExtraTreesGUID;
import binnie.extratrees.machines.ExtraTreeMachine.ComponentExtraTreeGUI;
import cpw.mods.fml.relauncher.Side;

public abstract class Woodworker {

	public static int beeswaxSlot = 0;
	public static int wood1Slot = 1;
	public static int wood2Slot = 2;
	
	public static class PackageWoodworker extends PackageCarpenter {

		public PackageWoodworker() {
			super(false);
		}
		
	}
	
	public static class PackagePanelworker extends PackageCarpenter {

		public PackagePanelworker() {
			super(true);
		}
		
	}

	public abstract static class PackageCarpenter extends ExtraTreeMachine.PackageExtraTreeMachine implements IMachineInformation {

		public PackageCarpenter(boolean panel) {
			super(panel ? "panelworker" : "woodworker", 
					panel ? "Panelworker" : "Woodworker", panel ? ExtraTreeTexture.panelerTexture :
						ExtraTreeTexture.carpenterTexture);
			this.panel = panel;
		}
		
		boolean panel = false;

		@Override
		public String getInformation() {
			return "The " + (panel? "Panelworker" : "Woodworker") + " creates patterned wooden tiles from two different woods.";
		}
		
		@Override
		public void createMachine(Machine machine) {
			new ComponentExtraTreeGUI(machine, 
					ExtraTreesGUID.Woodworker);

			ComponentInventorySlots inventory = new ComponentInventorySlots(machine);
			inventory.addSlot(beeswaxSlot, "Polish Slot");
			inventory.addSlot(wood1Slot, "Wood Slot");
			inventory.addSlot(wood2Slot, "Wood Slot");
			
			inventory.getSlot(beeswaxSlot).setValidator(
					new Validators.SlotValidatorBeeswax());
			inventory.getSlot(wood1Slot).setValidator(
					new Validators.SlotValidatorPlanks());
			inventory.getSlot(wood2Slot).setValidator(
					new Validators.SlotValidatorPlanks());

			new ComponentWoodworkerRecipe(machine, panel);

		}

	}

	public static class ComponentWoodworkerRecipe extends ComponentRecipe
			implements IComponentRecipe, INetwork.GUI, INetwork.CraftGUIAction, IErrorStateSource {

		
		int guiDesignIndex;

		public boolean panel = false;
		
		public ComponentWoodworkerRecipe(Machine machine, boolean panel) {
			super(machine);
			this.panel = panel;
			guiDesignIndex = getUniqueProgressBarID();
		}

		@Override
		public void addGUINetworkData(Map<Integer, Integer> data) {
			data.put(guiDesignIndex,
					CarpentryManager.carpentryInterface.getDesignIndex(getDesign()));
		}

		@Override
		public void recieveGUINetworkData(int id, int data) {
			
			if (id == guiDesignIndex) {
				setDesign(CarpentryManager.carpentryInterface.getDesign(data));
			}
		}

		@Override
		public void readFromNBT(NBTTagCompound nbttagcompound) {
			super.readFromNBT(nbttagcompound);
			setDesign(CarpentryManager.carpentryInterface.getDesign(nbttagcompound
					.getInteger("design")));
		}

		@Override
		public void writeToNBT(NBTTagCompound nbttagcompound) {
			super.writeToNBT(nbttagcompound);
			nbttagcompound.setInteger("design",
					CarpentryManager.carpentryInterface.getDesignIndex(design));
		}

		private IDesign design = EnumDesign.Diamond;

		@Override
		public boolean isRecipe() {
			return getProduct() != null;
		}

		@Override
		public ItemStack getProduct() {
			ItemStack plank1 = getUtil().getStack(
					wood1Slot);
			ItemStack plank2 = getUtil().getStack(
					wood2Slot);
			
			if(plank1 == null && plank2 == null) return null;
			
			ICarpentryWood type1 = CarpentryManager.carpentryInterface.getCarpentryWood(plank1);
			ICarpentryWood type2 = CarpentryManager.carpentryInterface.getCarpentryWood(plank2);
			IDesign design = getDesign();
			
			int stackSize = 2;
			
			if(plank1 == null) {
				type1 = type2;
				stackSize = 1;
			}
			
			if(plank2 == null) {
				type2 = type1;
				stackSize = 1;
			}
			
			if(design == EnumDesign.Blank) {
				type2 = type1;
				stackSize = 1;
			}
				
			
			ItemStack stack = ModuleCarpentry.getItemStack(
					(BlockCarpentry) 
					(panel ? ExtraTrees.blockPanel : ExtraTrees.blockCarpentry)
					, type1, type2,
					design);
			
			stack.stackSize = stackSize;
			return stack;
		}
		
		public int getOutputStackSize() {
			return panel ? 8 : 2;
		}

		@Override
		public ItemStack doRecipe(boolean takeItem) {
			if (!isRecipe())
				return null;
			if(canWork() != null)
				return null;
			ItemStack product = getProduct();
			if (takeItem) {
				ItemStack a = getUtil().decreaseStack(wood1Slot, 1);
				if(a == null)
					getUtil().decreaseStack(wood2Slot, 1);
				else if(design != EnumDesign.Blank) {
					getUtil().decreaseStack(wood2Slot, 1);
				}
				getUtil().decreaseStack(beeswaxSlot, 1);
			}
			return product;
		}

		public IDesign getDesign() {
			return design;
		}

		@Override
		public void recieveNBT(Side side, EntityPlayer player, NBTTagCompound nbt) {
			
			if(nbt.getName().equals("recipe")) {
				InventoryPlayer playerInv = player.inventory;

				ItemStack recipe = doRecipe(false);
				
				if(recipe == null)
					return;

				if (playerInv.getItemStack() == null) {
					playerInv.setItemStack(doRecipe(true));
				}
				else {
					if(playerInv.getItemStack().isItemEqual(recipe)
							&&ItemStack.areItemStackTagsEqual(
									playerInv.getItemStack(), recipe)) {
						int fit = recipe.getItem().getItemStackLimit() 
								-(recipe.stackSize + playerInv.getItemStack().stackSize);
						if(fit>=0) {
							ItemStack rec = doRecipe(true);
							rec.stackSize += playerInv.getItemStack().stackSize;
							playerInv.setItemStack(rec);
						}
							
					}
				}
				
				player.openContainer.detectAndSendChanges();
				if(player instanceof EntityPlayerMP)
					((EntityPlayerMP) player).updateHeldItem();
			}
			else if(nbt.getName().equals("design")) {
				setDesign(CarpentryManager.carpentryInterface.getDesign((int)nbt.getShort("D"))); 
			}

		}

		private void setDesign(IDesign design) {
			this.design = design;
		}

		@Override
		public ErrorState canWork() {
			if(getUtil().isSlotEmpty(beeswaxSlot))
				return new ErrorState.NoItem("No Wood Polish for Woodworking", beeswaxSlot);
			return null;
		}

		@Override
		public ErrorState canProgress() {
			return null;
		}

	}

}
