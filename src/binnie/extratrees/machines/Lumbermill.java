package binnie.extratrees.machines;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import binnie.core.Constants;
import binnie.core.machines.Machine;
import binnie.core.machines.TileEntityMachine;
import binnie.core.machines.inventory.ComponentInventorySlots;
import binnie.core.machines.inventory.ComponentTankContainer;
import binnie.core.machines.inventory.InventorySlot;
import binnie.core.machines.inventory.SlotValidator;
import binnie.core.machines.inventory.TankValidator;
import binnie.core.machines.power.ComponentPowerReceptor;
import binnie.core.machines.power.ComponentProcess;
import binnie.core.machines.power.ErrorState;
import binnie.core.machines.power.IProcess;
import binnie.craftgui.minecraft.IMachineInformation;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.core.ExtraTreeTexture;
import binnie.extratrees.core.ExtraTreesGUID;
import binnie.extratrees.item.ExtraTreeItems;
import binnie.extratrees.machines.ExtraTreeMachine.ComponentExtraTreeGUI;


public class Lumbermill {
	
	public static int slotWood = 0;
	public static int slotPlanks = 1;
	public static int slotBark = 2;
	public static int slotSawdust = 3;
	
	public static int tankWater = 0;
	
	public static class PackageLumbermill extends ExtraTreeMachine.PackageExtraTreeMachine implements IMachineInformation {
		
		public PackageLumbermill() { super("lumbermill", "Lumbermill", ExtraTreeTexture.lumbermillTexture); }

		@Override
		public void createMachine(Machine machine) {
			new ComponentExtraTreeGUI(machine, ExtraTreesGUID.Lumbermill);
			
			ComponentInventorySlots inventory = new ComponentInventorySlots(machine);
			
			inventory.addSlot(slotWood, InventorySlot.NameInput);
			inventory.getSlot(slotWood).setValidator(new SlotValidatorLog());
			inventory.getSlot(slotWood).forbidExtraction();
			
			inventory.addSlot(slotPlanks, InventorySlot.NameOutput);
			inventory.addSlot(slotBark, "Byproduct Slot");
			inventory.addSlot(slotSawdust, "Byproduct Slot");
			inventory.getSlot(slotPlanks).setReadOnly();
			inventory.getSlot(slotBark).setReadOnly();
			inventory.getSlot(slotSawdust).setReadOnly();

			ComponentTankContainer tanks = new ComponentTankContainer(machine);
			tanks.addTank(tankWater, "Water Tank", 10000);
			tanks.getTankSlot(tankWater).setValidator(new TankValidator.Basic(Constants.LiquidWater));

			new ComponentPowerReceptor(machine);
			
			
			new ComponentLumbermillLogic(machine);
		}

		@Override
		public TileEntity createTileEntity() { return new TileEntityMachine(this);}
		@Override public void register() {}
		
		@Override
		public String getInformation() {
			return "The Lumbermill converts logs into planks at an increased rate, as well as giving byproducts such as sawdust and bark.";
		}
		
	}
	
	public static class ComponentLumbermillLogic extends ComponentProcess implements IProcess {

		public ComponentLumbermillLogic(Machine machine) {
			super(machine, 90, 30);
		}

		@Override
		public ErrorState canWork() {
			if(getUtil().isSlotEmpty(slotWood))
				return new ErrorState.NoItem("No Wood", slotWood);
			ItemStack result = getPlankProduct(getUtil().getStack(slotWood));
			if(!getUtil().isSlotEmpty(slotPlanks) && result != null) {
				ItemStack currentPlank = getUtil().getStack(slotPlanks);
				if(!result.isItemEqual(currentPlank) || result.stackSize + currentPlank.stackSize > currentPlank.getMaxStackSize())
					return new ErrorState.NoSpace("No room for new planks", new int[] {slotPlanks});
			}
			
			return super.canWork();
		}
		
		@Override
		public ErrorState canProgress() {
			if(!getUtil().liquidInTank(tankWater, 5))
				return new ErrorState.InsufficientLiquid("Not Enough Water", tankWater);
			return super.canProgress();
		}

		@Override
		protected void onFinishTask() {
			ItemStack result = getPlankProduct(getUtil().getStack(slotWood));
			if(result == null) return;
			getUtil().addStack(slotPlanks, result);
			getUtil().addStack(slotSawdust, ExtraTrees.itemMisc.getStack(ExtraTreeItems.Sawdust, 1));
			getUtil().addStack(slotBark, ExtraTrees.itemMisc.getStack(ExtraTreeItems.Bark, 1));
			getUtil().decreaseStack(slotWood, 1);
		}
		
		@Override
		protected void onTickTask() {
			getUtil().drainTank(tankWater, 10);
		}
		
	}
	
	public static class SlotValidatorLog extends SlotValidator {
		
		public SlotValidatorLog() {
			super(SlotValidator.IconBlock);
		}

		@Override
		public boolean isValid(ItemStack itemStack) {
			String name = OreDictionary.getOreName(OreDictionary.getOreID(itemStack));
			return name.contains("logWood") && getPlankProduct(itemStack)!=null;
		}

		@Override
		public String getTooltip() {
			return "Logs";
		}
		
	}
	
	public static ItemStack getPlankProduct(ItemStack item) {
		ItemStack stack = null;
		for(Object recipeO : CraftingManager.getInstance().getRecipeList()) {
			if(recipeO instanceof ShapelessRecipes) {
				ShapelessRecipes recipe = (ShapelessRecipes) recipeO;
				if(recipe.recipeItems.size()>1) continue;
				Object input = recipe.recipeItems.get(0);
				if(input instanceof ItemStack && ((ItemStack) input).isItemEqual(item)) stack = recipe.getRecipeOutput().copy();
			}
			if(recipeO instanceof ShapedRecipes) {
				ShapedRecipes recipe = (ShapedRecipes) recipeO;
				if(recipe.recipeItems.length>1) continue;
				ItemStack input = (ItemStack) recipe.recipeItems[0];
				if(input.isItemEqual(item)) stack = recipe.getRecipeOutput().copy();
			}
			if(recipeO instanceof ShapelessOreRecipe) {
				ShapelessOreRecipe recipe = (ShapelessOreRecipe) recipeO;
				if(recipe.getInput().size()>1) continue;
				Object input = recipe.getInput().get(0);
				if(input instanceof ItemStack && ((ItemStack) input).isItemEqual(item)) stack = recipe.getRecipeOutput().copy();
			}
			
		}
		if(stack != null)
			stack.stackSize = (int) (stack.stackSize * 1.5f);
		return stack;
	}

}
