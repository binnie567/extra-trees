package binnie.extratrees.machines;

import net.minecraft.item.ItemStack;
import binnie.core.machines.inventory.SlotValidator;
import binnie.core.machines.inventory.ValidatorIcon;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.item.ExtraTreeItems;

public class Validators {
	
	public static class SlotValidatorBeeswax extends SlotValidator {

		public SlotValidatorBeeswax() {
			super(new ValidatorIcon(ExtraTrees.instance, "validator/polish.0", "validator/polish.1"));
		}

		@Override
		public boolean isValid(ItemStack itemStack) {
			return ExtraTreeItems.WoodWax.get(1).isItemEqual(itemStack);
		}

		@Override
		public String getTooltip() {
			return "Wood Wax";
		}

	}

	public static class SlotValidatorPlanks extends SlotValidator {

		public SlotValidatorPlanks() {
			super(SlotValidator.IconBlock);
		}

		@Override
		public boolean isValid(ItemStack itemStack) {
			return itemStack==null ? false : CarpentryManager.carpentryInterface.getCarpentryWood(itemStack) != null;
		}

		@Override
		public String getTooltip() {
			return "Planks";
		}
		
	}

}
