package binnie.extratrees.machines;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.oredict.ShapedOreRecipe;
import binnie.core.BinnieCore;
import binnie.core.machines.MachineGroup;
import binnie.core.plugin.IBinnieModule;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.config.ConfigurationMain;
import binnie.extratrees.item.ExtraTreeItems;
import forestry.api.core.ItemInterface;
import forestry.api.core.Tabs;

public class ModuleMachine implements IBinnieModule {

	@Override
	public void preInit() {
		MachineGroup machineGroup = new MachineGroup("extratrees.machine",
				ConfigurationMain.machineID, "extratrees.block.machine", 
				ExtraTreeMachine.values());
		machineGroup.setCreativeTab(Tabs.tabArboriculture);

		ExtraTrees.blockMachine = machineGroup.getBlock();
		
		BinnieCore.proxy.registerTileEntity(TileEntityNursery.class, "binnie.tile.nursery", 
				BinnieCore.proxy.createObject("binnie.core.machines.RendererMachine"));	
	}

	@Override
	public void doInit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postInit() {
		CraftingManager
		.getInstance()
		.getRecipeList()
		.add(new ShapedOreRecipe(
				new ItemStack(ExtraTrees.blockMachine, 1, 0),
				new Object[] {
						"gAg",
						"GsG",
						"gGg",
						'G',
						Block.glass,
						'g',
						ExtraTreeItems.ProvenGear.get(1),
						'A', Item.axeIron, 's',
						ItemInterface.getItem("sturdyCasing") }));

CraftingManager
		.getInstance()
		.getRecipeList()
		.add(new ShapedOreRecipe(
				new ItemStack(ExtraTrees.blockMachine, 1, 1),
				new Object[] {
						"wGw",
						"GsG",
						"ggg",
						'G',
						Block.glass,
						'g',
						ExtraTreeItems.ProvenGear.get(1),
						'w', Block.planks, 's',
						ItemInterface.getItem("impregnatedCasing") }));

CraftingManager
		.getInstance()
		.getRecipeList()
		.add(new ShapedOreRecipe(
				new ItemStack(ExtraTrees.blockMachine, 1, 2),
				new Object[] {
						"wGw",
						"GsG",
						"ggg",
						'G',
						Block.glass,
						'g',
						ExtraTreeItems.ProvenGear.get(1),
						'w', Block.woodSingleSlab, 's',
						ItemInterface.getItem("impregnatedCasing") }));

CraftingManager
		.getInstance()
		.getRecipeList()
		.add(new ShapedOreRecipe(
				new ItemStack(ExtraTrees.blockMachine, 1, 3),
				new Object[] {
						"gGg",
						"GsG",
						"lGl",
						'G',
						Block.glass,
						'g',
						ItemInterface.getItem("grafterProven"),
						'l',
						ExtraTreeItems.ProvenGear.get(1),
						's', ItemInterface.getItem("impregnatedCasing") }));
	}

}
