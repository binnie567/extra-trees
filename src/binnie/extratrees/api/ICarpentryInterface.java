package binnie.extratrees.api;

import java.util.Collection;
import java.util.List;

import net.minecraft.item.ItemStack;

public interface ICarpentryInterface {
	
	public boolean registerCarpentryWood(int index, ICarpentryWood wood);
	public int getCarpentryWoodIndex(ICarpentryWood wood);
	public ICarpentryWood getCarpentryWood(int index);
	
	public boolean registerDesign(int index, IDesign wood);
	public int getDesignIndex(IDesign wood);
	public IDesign getDesign(int index);
	
	public ICarpentryWood getCarpentryWood(ItemStack plank);
	
	public ILayout getLayout(IPattern pattern, boolean inverted);
	

	public boolean registerDesignCategory(IDesignCategory category);
	public IDesignCategory getDesignCategory(String id);
	public Collection<IDesignCategory> getAllDesignCategories();
	public List<IDesign> getSortedDesigns();

}
