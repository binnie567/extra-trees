package binnie.extratrees.api;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

/**
 * Interface for items that want to be able to rotate carpentry blocks on right click
 * @author Alex Binnie
 */
public interface IToolHammer {
	
	/**
	 * Return true if the hammer works using durability. You could return false if it is electrical and out of power, etc.
	 * @param item The itemstack that has been used on the block. It will be the item that is implementing this IToolHammer
	 * @return Whether this hammer can currently be used to rotate/change faces. 
	 */
	boolean isActive(ItemStack item);
	
	/**
	 * This is where you should apply damage to your hammer.
	 * @param item The hammer itemstack
	 * @param player The player that is using the hammer
	 */
	void onHammerUsed(ItemStack item, EntityPlayer player);

}
