package binnie.extratrees.api;

import net.minecraft.util.Icon;

public interface ILayout {
	
	public IPattern getPattern();
	public boolean isInverted();
	
	ILayout rotateRight();
	ILayout rotateLeft();
	ILayout flipHorizontal();
	ILayout flipVertical();
	
	public ILayout invert();
	
	Icon getPrimaryIcon();
	Icon getSecondaryIcon();

}
