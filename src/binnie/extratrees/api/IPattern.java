package binnie.extratrees.api;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;

public interface IPattern {

	Icon getPrimaryIcon();
	Icon getSecondaryIcon();
	
	ILayout getRotation();
	ILayout getHorizontalFlip();
	
	public void registerIcons(IconRegister register);

}
