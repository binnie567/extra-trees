package binnie.extratrees.api;

import net.minecraft.item.ItemStack;

/**
 * Interface to be inherited by something wanting to be used in the woodworker
 * @author Alex Binnie
 *
 */
public interface ICarpentryWood {
	
	/**
	 * 
	 * @return ItemStack of the plank that is crafted into this woodworker wood
	 */
	ItemStack getPlank();
	
	/**
	 * It is recommended if your wood is named after a real life wood, that you qualify it
	 * (such as Scottish Fir) to avoid confusion with Extra Tree Planks
	 * @return Name such as 'Holly' or 'Fir' that qualifies tiles etc.
	 */
	String getName();
	
	/**
	 * It is recommended that the colour of the plank is taken to be the colour of the pixel 
	 * at 8,5 in the plank texture
	 * @return Integer in the form 0xrrggbb, where r g b are the three colour components
	 */
	int getColour();

}
