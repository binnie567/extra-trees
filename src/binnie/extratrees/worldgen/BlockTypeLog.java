package binnie.extratrees.worldgen;

import net.minecraft.world.World;
import binnie.extratrees.block.ILogType;
import forestry.api.arboriculture.ITree;

public class BlockTypeLog extends BlockType {

	ILogType log;
	
	public BlockTypeLog(ILogType log) {
		super(0, 0);
		this.log = log;
	}
	
	public void setBlock(World world, ITree tree, int x, int y, int z) {
		log.placeBlock(world, x, y, z);
	}

}
