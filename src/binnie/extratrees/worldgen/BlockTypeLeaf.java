package binnie.extratrees.worldgen;

import net.minecraft.world.World;
import forestry.api.arboriculture.ITree;
import forestry.api.core.BlockInterface;
import forestry.api.world.ITreeGenData;

public class BlockTypeLeaf extends BlockType {

	public BlockTypeLeaf() {
		super(BlockInterface.getBlock("leaves").getItem().itemID, 0);
	}

	@Override
	public void setBlock(World world, ITree tree, int x, int y, int z) {
		((ITreeGenData)tree).setLeaves(world, "", x, y, z);
	}
}
