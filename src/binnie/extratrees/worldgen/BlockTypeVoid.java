package binnie.extratrees.worldgen;

import net.minecraft.world.World;
import forestry.api.arboriculture.ITree;

public class BlockTypeVoid extends BlockType {

	public BlockTypeVoid() {
		super(0, 0);
	}

	@Override
	public void setBlock(World world, ITree tree, int x, int y, int z) {
		world.setBlock(x, y, z, 0, 0, 0);
		if(world.getBlockTileEntity(x, y, z) != null)
			world.removeBlockTileEntity(x, y, z);
	}

}
