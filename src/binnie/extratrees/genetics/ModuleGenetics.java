package binnie.extratrees.genetics;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import binnie.core.BinnieCore;
import binnie.core.genetics.BinnieGenetics;
import binnie.core.plugin.IBinnieModule;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IClassification;
import forestry.api.genetics.IClassification.EnumClassLevel;

public class ModuleGenetics implements IBinnieModule {

	public void preInit() {
	}

	public void doInit() {
		
		//VillagerRegistry.instance().registerVillageCreationHandler(new VillageHandler());
		
	}

	public void postInit() {

		ExtraTreeSpecies.init();

		//generateBranches();

		ExtraTreeFruitGene.init();

		for (ExtraTreeFruitGene fruit : ExtraTreeFruitGene.values()) {
			AlleleManager.alleleRegistry.registerAllele(fruit);
		}
		for (ExtraTreeSpecies species : ExtraTreeSpecies.values()) {
			if (species.isFinished() || BinnieCore.proxy.isDebug()) {
				AlleleManager.alleleRegistry.registerAllele(species);
				BinnieGenetics.getTreeRoot().registerTemplate(species.getTemplate());
			}
		}
		
		for(FruitSprite fruit : FruitSprite.values())
			fruit.setup();

		ExtraTreeMutation.init();

		for (ButterflySpecies species : ButterflySpecies.values()) {
			AlleleManager.alleleRegistry.registerAllele(species);
			BinnieGenetics.getButterflyRoot().registerTemplate(species.getTemplate());
			
			String scientific =  species.branchName.substring(0, 1).toUpperCase() + species.branchName.substring(1).toLowerCase();
			String uid = "trees."+species.branchName.toLowerCase();
			IClassification branch = AlleleManager.alleleRegistry.getClassification("genus."+uid);
			if(branch == null) 
				branch = AlleleManager.alleleRegistry.createAndRegisterClassification(EnumClassLevel.GENUS, uid, scientific);
			species.branch = branch;
			species.branch.addMemberSpecies(species);
			
		}

	}

	// Genus Tribe Subfamily Family Order (ales) Class (ids) Division Phylum
	// Kingdom Domain

	String[] branches = new String[] { "Malus Maleae Amygdaloideae Rosaceae",
			"Musa   Musaceae Zingiberales Commelinids Angiosperms", "Sorbus Maleae",
			"Tsuga   Pinaceae", "Fraxinus Oleeae  Oleaceae Lamiales Asterids Angiospems", };
	
	List<List<String>> classifications = new ArrayList<List<String>>();

	private void generateBranches() {
		
		for (String hierarchy : branches) {
			List<String> set = new ArrayList<String>();
			for (String string : hierarchy.split(" ", 0)) {
				set.add(string.toLowerCase());
			}
			classifications.add(set);
		}
		
		for (ExtraTreeSpecies species : ExtraTreeSpecies.values()) {
			IClassification branch = getOrCreateClassification(EnumClassLevel.GENUS,
					species.branchName);
			branch.addMemberSpecies(species);
			species.branch = branch;
			
			
			IClassification clss = branch;
			int currentLevel = EnumClassLevel.GENUS.ordinal();
			
			
			while (clss.getParent() == null) {
				for(List<String> set : classifications) {
					if(set.contains(clss.getScientific().toLowerCase())) {
						String nextLevel = "";
						int index = set.indexOf(clss.getScientific().toLowerCase()) + 1;
						while(nextLevel.length() == 0) {
							try {
								nextLevel = set.get(index++);
							}
							catch(IndexOutOfBoundsException ex) {
								throw new RuntimeException("Reached end point at " + set.get(index-2));
							}
							
							currentLevel--;
						}
						IClassification parent = getOrCreateClassification(EnumClassLevel.values()[currentLevel], nextLevel);
						parent.addMemberGroup(clss);
						System.out.println("Went from " + clss.getScientific() + " to " + parent.getScientific());
						clss = parent;
						break;
					}
				}
				// System.out.println(species.branchName);
			}
		}
	}

	private IClassification getOrCreateClassification(EnumClassLevel level, String name) {
		if(level == EnumClassLevel.GENUS) name = "trees." + name;
		String uid = level.name().toLowerCase(Locale.ENGLISH) + "." + name.toLowerCase();
		if (AlleleManager.alleleRegistry.getClassification(uid) != null)
			return AlleleManager.alleleRegistry.getClassification(uid);
		return AlleleManager.alleleRegistry.createAndRegisterClassification(level,
				name.toLowerCase(), name);
	}
}
