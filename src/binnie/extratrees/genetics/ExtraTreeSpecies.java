package binnie.extratrees.genetics;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.EnumPlantType;
import binnie.core.genetics.BinnieGenetics;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.block.ILogType;
import binnie.extratrees.gen.WorldGenAlder;
import binnie.extratrees.gen.WorldGenApple;
import binnie.extratrees.gen.WorldGenAsh;
import binnie.extratrees.gen.WorldGenBanana;
import binnie.extratrees.gen.WorldGenBeech;
import binnie.extratrees.gen.WorldGenConifer;
import binnie.extratrees.gen.WorldGenDefault;
import binnie.extratrees.gen.WorldGenEucalyptus;
import binnie.extratrees.gen.WorldGenFir;
import binnie.extratrees.gen.WorldGenHolly;
import binnie.extratrees.gen.WorldGenJungle;
import binnie.extratrees.gen.WorldGenMaple;
import binnie.extratrees.gen.WorldGenPoplar;
import binnie.extratrees.gen.WorldGenSorbus;
import binnie.extratrees.gen.WorldGenTree;
import binnie.extratrees.gen.WorldGenTree2;
import binnie.extratrees.gen.WorldGenTree3;
import binnie.extratrees.gen.WorldGenWalnut;
import binnie.extratrees.gen.WorldGenEucalyptus.RoseGum;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.arboriculture.EnumGermlingType;
import forestry.api.arboriculture.EnumTreeChromosome;
import forestry.api.arboriculture.IAlleleFruit;
import forestry.api.arboriculture.IAlleleTreeSpecies;
import forestry.api.arboriculture.ITree;
import forestry.api.arboriculture.ITreeRoot;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.core.IIconProvider;
import forestry.api.core.ItemInterface;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IClassification;
import forestry.api.genetics.IClassification.EnumClassLevel;
import forestry.api.genetics.IFruitFamily;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;

public enum ExtraTreeSpecies implements IAlleleTreeSpecies, IIconProvider {

	OrchardApple("Orchard Apple", "malus", "domestica", 0x648830, 0xFF9CF3, ILogType.ExtraTreeLog.Apple, ExtraTreeFruitGene.Apple, WorldGenApple.OrchardApple.class),
	SweetCrabapple("Sweet Crabapple", "malus", "coronaria", 0x7A9953, 0xFC359F, ILogType.ExtraTreeLog.Apple, ExtraTreeFruitGene.Crabapple, WorldGenApple.SweetCrabapple.class),
	FloweringCrabapple("Flowering Crabapple", "malus", "hopa", 0x7A9953, 0xFC359F, ILogType.ExtraTreeLog.Apple, ExtraTreeFruitGene.Crabapple, WorldGenApple.FloweringCrabapple.class),
	PrairieCrabapple("Prairie Crabapple", "malus", "ioensis", 0x7A9953, 0xFC359F, ILogType.ExtraTreeLog.Apple, ExtraTreeFruitGene.Crabapple, WorldGenApple.PrairieCrabapple.class),

	// Prune Fruits

	Blackthorn("Blackthorn", "prunus", "spinosa ", 0x6D8F1E, 0xFF87C7, ILogType.ForestryLog.PLUM, ExtraTreeFruitGene.Blackthorn, null),
	CherryPlum("Cherry Plum", "prunus", "cerasifera", 0x6D8F1E, 0xFF87C7, ILogType.ForestryLog.PLUM, ExtraTreeFruitGene.CherryPlum, null),
	Peach("Peach", "prunus", "persica", 0x6D8F1E, 0xFF269A, ILogType.ForestryLog.PLUM, ExtraTreeFruitGene.Peach, null),
	Nectarine("Nectarine", "prunus", "nectarina", 0x6D8F1E, 0xFF269A, ILogType.ForestryLog.PLUM, ExtraTreeFruitGene.Nectarine, null),
	Apricot("Apricot", "prunus", "armeniaca", 0x6D8F1E, 0xF5B8D8, ILogType.ForestryLog.PLUM, ExtraTreeFruitGene.Apricot, null),
	Almond("Almond", "prunus", "amygdalus", 0x6D8F1E, 0xF584C0, ILogType.ForestryLog.PLUM, ExtraTreeFruitGene.Almond, null),
	WildCherry("Wild Cherry", "prunus", "avium", 0x6D8F1E, 0xF7EBF6, ILogType.ExtraTreeLog.Cherry, ExtraTreeFruitGene.WildCherry, null),
	SourCherry("Sour Cherry", "prunus", "cerasus", 0x6D8F1E, 0xF7EBF6, ILogType.ExtraTreeLog.Cherry, ExtraTreeFruitGene.SourCherry, null),
	BlackCherry("Black Cherry", "prunus", "serotina", 0x6D8F1E, 0xFAE1F8, ILogType.ExtraTreeLog.Cherry, ExtraTreeFruitGene.BlackCherry, null),

	// Citrus Fruits
	Orange("Orange", "citrus", "sinensis", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Orange, null),
	Manderin("Manderine", "citrus", "reticulata", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Manderin, null),
	Satsuma("Satsuma", "citrus", "unshiu", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Satsuma, null),
	Tangerine("Tangerine", "citrus", "tangerina", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Tangerine, null),
	Lime("Lime", "citrus", "latifolia", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Lime, null),
	KeyLime("Key Lime", "citrus", "aurantifolia", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.KeyLime, null),
	FingerLime("Finger Lime", "citrus", "australasica", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.FingerLime, null),
	Pomelo("Pomelo", "citrus", "maxima", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Pomelo, null),
	Grapefruit("Grapefruit", "citrus", "paradisi", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Grapefruit, null),
	Kumquat("Kumquat", "citrus", "margarita", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Kumquat, null),
	Citron("Citron", "citrus", "medica", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.Citron, null),
	BuddhaHand("Buddha's Hand", "citrus", "sarcodactylus", 0x88af54, 0xa3b850, ILogType.ForestryLog.CITRUS, ExtraTreeFruitGene.BuddhaHand, null),

	// Banana

	Banana("Banana", "musa", "sinensis", 0xA1CD8E, 0x44E500, ILogType.ExtraTreeLog.Banana, ExtraTreeFruitGene.Banana, WorldGenBanana.class),
	RedBanana("Red Banana", "musa", "rubra", 0xA1CD8E, 0x44E500, ILogType.ExtraTreeLog.Banana, ExtraTreeFruitGene.RedBanana, WorldGenBanana.class),
	Plantain("Plantain", "musa", "paradisiaca", 0xA1CD8E, 0x44E500, ILogType.ExtraTreeLog.Banana, ExtraTreeFruitGene.Plantain, WorldGenBanana.class),

	// 28 so far

	Butternut("Butternut", "juglans", "cinerea", 0x82B58C, 0x82DD8C, ILogType.ExtraTreeLog.Butternut, ExtraTreeFruitGene.Butternut, WorldGenWalnut.Butternut.class),
	Rowan("Common Rowan", "sorbus", "aucuparia", 0x9EC79B, 0x9EE8B2, ILogType.ExtraTreeLog.Rowan, null, WorldGenSorbus.Rowan.class),
	Hemlock("Western Hemlock", "tsuga", "heterophylla", 0x5CAC72, 0x5CD172, ILogType.ExtraTreeLog.Hemlock, null, WorldGenConifer.WesternHemlock.class),
	Ash("Common Ash", "fraxinus", "excelsior", 0x488E2B, 0x48E42B, ILogType.ExtraTreeLog.Ash, null, WorldGenAsh.CommonAsh.class),
	Alder("Common Alder", "alnus", "glutinosa", 0x698A33, 0x69AE33, ILogType.ExtraTreeLog.Alder, null, WorldGenAlder.CommonAlder.class),
	Beech("Common Beech", "fagus", "sylvatica", 0x83A04C, 0x83C64C, ILogType.ExtraTreeLog.Beech, ExtraTreeFruitGene.Beechnut, WorldGenBeech.CommonBeech.class),
	CopperBeech("Copper Beech", "fagus", "purpurea", 0x801318, 0xD15B4D, ILogType.ExtraTreeLog.Beech, ExtraTreeFruitGene.Beechnut, WorldGenBeech.CopperBeech.class),
	Aspen("Aspen", "populus", "tremula", 0x8ACC37, 0x8AE18F, ILogType.ForestryLog.POPLAR, null, WorldGenPoplar.Aspen.class),

	Yew("Common Yew", "taxus", "baccata", 0x948A4D, 0x94AE4D, ILogType.ExtraTreeLog.Yew, null, WorldGenConifer.Yew.class),
	Cypress("Lawson Cypress", "chamaecyparis", "lawsoniana", 0x89C9A7, 0x89DDC6, ILogType.ExtraTreeLog.Cypress, null, WorldGenConifer.Cypress.class),
	DouglasFir("Douglas-fir", "pseudotsuga", "menziesii", 0x99B582, 0x99D1AA, ILogType.ExtraTreeLog.Fir, null, WorldGenFir.DouglasFir.class),
	Hazel("Common Hazel", "Corylus", "avellana", 0x9BB552, 0x9BE152, ILogType.ExtraTreeLog.Hazel, ExtraTreeFruitGene.Hazelnut, WorldGenTree3.Hazel.class),
	Sycamore("Sycamore Fig", "ficus", "sycomorus", 0xA0A52F, 0xB4D55C, ILogType.ExtraTreeLog.Fig, ExtraTreeFruitGene.Fig, WorldGenTree3.Sycamore.class),
	Whitebeam("Whitebeam", "sorbus", "aria", 0xBACE99, 0x72863F, ILogType.ExtraTreeLog.Whitebeam, null, WorldGenSorbus.Whitebeam.class),
	Hawthorn("Common Hawthorn", "crataegus", "monogyna", 0x6BA84A, 0x98B77B, ILogType.ExtraTreeLog.Hawthorn, null, WorldGenTree3.Hawthorn.class),
	Pecan("Pecan", "carya", "illinoinensis", 0x85B674, 0x2C581B, ILogType.ExtraTreeLog.Hickory, ExtraTreeFruitGene.Pecan, WorldGenTree3.Pecan.class),

	Elm("Common Elm", "ulmus", "procera", 0x7C9048, 0x7CBE48, ILogType.ExtraTreeLog.Elm, null, WorldGenTree3.Elm.class),
	Elder("Elderberry", "sambucus", "nigra", 0xAEB873, 0xE0E7BD, ILogType.ExtraTreeLog.Elder, ExtraTreeFruitGene.Elderberry, WorldGenTree3.Elder.class),
	Holly("Common Holly", "ilex", "aquifolium", 0x254B4C, 0x6E9284, ILogType.ExtraTreeLog.Holly, null, WorldGenHolly.Holly.class),
	Hornbeam("Common Hornbeam", "carpinus", "betulus", 0x96A71B, 0x96DD1B, ILogType.ExtraTreeLog.Hornbeam, null, WorldGenTree3.Hornbeam.class),
	Sallow("Great Sallow", "salix", "caprea", 0xAEB323, 0xB7EC25, ILogType.ForestryLog.WILLOW, null, WorldGenTree3.Sallow.class),
	AcornOak("Acorn Oak", "quercus", "robur", 0x66733E, 0x9EA231, ILogType.VanillaLog.Oak, ExtraTreeFruitGene.Acorn, WorldGenTree3.AcornOak.class),
	Fir("Silver Fir", "abies", "alba", 0x6F7C20, 0x6FD120, ILogType.ExtraTreeLog.Fir, null, WorldGenFir.SilverFir.class),
	Cedar("Great Cedar", "cedrus", "libani", 0x95A370, 0x95E870, ILogType.ExtraTreeLog.Cedar, null, WorldGenConifer.Cedar.class),

	Olive("Olive", "olea", "europaea", 0x3C4834, 0x3C4834, ILogType.ExtraTreeLog.Olive, ExtraTreeFruitGene.Olive, WorldGenTree2.Olive.class),
	RedMaple("Red Maple", "acer", "ubrum", 0xE82E17, 0xE82E17, ILogType.ForestryLog.MAPLE, null, WorldGenMaple.RedMaple.class),
	BalsamFir("Balsam Fir", "abies", "balsamea", 0x74A07C, 0x74A07C, ILogType.ExtraTreeLog.Fir, null, WorldGenFir.BalsamFir.class),
	LoblollyPine("Loblolly Pine", "pinus", "taeda", 0x6F8A47, 0x6F8A47, ILogType.ForestryLog.PINE, null, WorldGenConifer.LoblollyPine.class),
	Sweetgum("Sweetgum", "liquidambar", "styraciflua", 0x8B8762, 0x8B8762, ILogType.ExtraTreeLog.Sweetgum, null, WorldGenTree2.Sweetgum.class),
	Locust("Black Locust", "robinia", "pseudoacacia", 0x887300, 0x887300, ILogType.ExtraTreeLog.Locust, null, WorldGenTree2.Locust.class),
	Pear("Cultivated Pear", "pyrus", "communis", 0x5E8826, 0x5E8826, ILogType.ExtraTreeLog.Pear, ExtraTreeFruitGene.Pear, WorldGenTree2.Pear.class),

	OsangeOsange("Osange-Orange", "maclura", "pomifera", 0x687A50, 0x687A50, ILogType.ExtraTreeLog.Maclura, ExtraTreeFruitGene.OsangeOsange, WorldGenJungle.OsangeOsange.class),
	OldFustic("Old Fustic", "maclura", "tinctoria", 0x687A50, 0x687A50, ILogType.ExtraTreeLog.Maclura, null, WorldGenJungle.OldFustic.class),
	Brazilwood("Brazilwood", "caesalpinia", "echinata", 0x607459, 0x607459, ILogType.ExtraTreeLog.Brazilwood, null, WorldGenJungle.Brazilwood.class),
	Logwood("Logwood", "haematoxylum", "campechianum", 0x889F6B, 0x889F6B, ILogType.ExtraTreeLog.Logwood, null, WorldGenJungle.Logwood.class),
	Rosewood("Rosewood", "dalbergia", "latifolia", 0x879B22, 0x879B22, ILogType.ExtraTreeLog.Rosewood, null, WorldGenJungle.Rosewood.class),
	Purpleheart("Purpleheart", "peltogyne", "spp.", 0x778F55, 0x778F55, ILogType.ExtraTreeLog.Purpleheart, null, WorldGenJungle.Purpleheart.class),

	Iroko("Iroko", "milicia", "excelsa", 0xAFC86C, 0xAFC86C, ILogType.ExtraTreeLog.Iroko, null, WorldGenTree2.Iroko.class),
	Gingko("Ginkgo", "ginkgo", "biloba", 0x719651, 0x719651, ILogType.ExtraTreeLog.Gingko, ExtraTreeFruitGene.GingkoNut, WorldGenTree2.Gingko.class),
	Brazilnut("Brazil Nut", "bertholletia", "excelsa", 0x7C8F7B, 0x7C8F7B, ILogType.VanillaLog.Jungle, ExtraTreeFruitGene.BrazilNut, WorldGenJungle.BrazilNut.class),
	RoseGum("Rose Gum", "eucalyptus", "grandis", 0x9CA258, 0x9CA258, ILogType.ExtraTreeLog.Eucalyptus, null, WorldGenEucalyptus.RoseGum.class),
	SwampGum("Swamp Gum", "eucalyptus", "grandis", 0xA2C686, 0xA2C686, ILogType.ExtraTreeLog.Eucalyptus2, null, WorldGenEucalyptus.SwampGum.class),
	Box("Boxwood", "boxus", "sempervirens", 0x72996D, 0x72996D, ILogType.ExtraTreeLog.Box, null, WorldGenTree2.Box.class),
	Clove("Clove", "syzygium", "aromaticum", 0x7A821F, 0x7A821F, ILogType.ExtraTreeLog.Syzgium, ExtraTreeFruitGene.Clove, WorldGenTree2.Clove.class),
	Coffee("Coffee", "coffea", "arabica", 0x6F9065, 0x6F9065, ILogType.VanillaLog.Jungle, ExtraTreeFruitGene.Coffee, WorldGenJungle.Coffee.class),

	MonkeyPuzzle("Monkey Puzzle", "araucaria", "araucana", 0x576158, 0x576158, ILogType.ForestryLog.PINE, null, WorldGenConifer.MonkeyPuzzle.class),
	RainbowGum("Rainbow Gum", "eucalyptus", "deglupta", 0xB7F025, 0xB7F025, ILogType.ExtraTreeLog.Eucalyptus3, null, WorldGenEucalyptus.RainbowGum.class),

	PinkIvory("Pink Ivory", "berchemia", "zeyheri", 0x7C9159, 0x7C9159, ILogType.ExtraTreeLog.PinkIvory, null, WorldGenTree.class),
	
	// 1.7.1
/*
	Cinnamon("Cinnamon", "cinnamomum", "verum", 0x0, 0x0, ILogType.ExtraTreeLog.Cinnamon, null, WorldGenJungle.Shrub15.class),
	Coconut("Coconut", "cocos", "nucifera", 0x0, 0x0, ILogType.ForestryLog.PALM, null, null),
	Cashew("Cashew", "anacardium", "occidentale", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, WorldGenJungle.Shrub15.class),
	Avacado("Avacado", "persea", "americana", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, null),
	// Nutmeg("Nutmeg", "myristica", "fragrans", 0x0, 0x0,
	// ILogType.VanillaLog.Jungle, null, WorldGenJungle.Shrub5.class),
	Allspice("Allspice", "pimenta", "dioica", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, WorldGenJungle.Shrub15.class),
	// Chilli("Chilli", "capsicum", "annuum", 0x0, 0x0,
	// ILogType.VanillaLog.Jungle, null, WorldGenJungle.Shrub5.class),
	// StarAnise("Star Anise", "illicium", "verum", 0x0, 0x0,
	// ILogType.VanillaLog.Jungle, null, WorldGenJungle.Shrub10.class),
	Mango("Star Anise", "mangifera", "indica", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, null),
	// StarFruit("Starfruit", "averrhoa", "carambola", 0x0, 0x0,
	// ILogType.VanillaLog.Jungle, null, WorldGenJungle.Shrub10.class),
	Candlenut("Candlenut", "aleurites", "moluccana", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, null),
*/
	/*
	 * YellowAutumn("Yellow Autumn", "autumna", "fluva", 0xFDB731, 0xFDB731,
	 * null, null, null), RedAutumn("Red Autumn", "autumna", "rubra", 0x963939,
	 * 0xD13939, null, null, null), OrangeAutumn("Orange Autumn", "autumna",
	 * "croceus", 0xB04F1F, 0xB04F1F, null, null, null),
	 * PurpleAutumn("Purple Autumn", "autumna", "purpureus", 0x723C21, 0xA0558D,
	 * null, null, null),
	 */

	/*
	 * Cinnamon("Cinnamon", "cinnamomum", "verum", 0x0, 0x0,
	 * ILogType.ExtraTreeLog.Cinnamon, null, null), Coconut("Coconut", "cocos",
	 * "nucifera", 0x0, 0x0, ILogType.ForestryLog.PALM, null, null),
	 * Cashew("Cashew", "anacardium", "occidentale", 0x0, 0x0,
	 * ILogType.VanillaLog.Jungle, null, null), Avacado("Avacado", "persea",
	 * "americana", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, null),
	 * Nutmeg("Nutmeg", "myristica", "fragrans", 0x0, 0x0,
	 * ILogType.VanillaLog.Jungle, null, null), Allspice("Allspice", "pimenta",
	 * "dioica", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, null),
	 * Chilli("Chilli", "capsicum", "annuum", 0x0, 0x0,
	 * ILogType.VanillaLog.Jungle, null, null), Pepper("Black Pepper", "piper",
	 * "nigrum", 0x0, 0x0, ILogType.VanillaLog.Jungle, null, null),
	 * StarAnise("Star Anise", "illicium", "verum", 0x0, 0x0,
	 * ILogType.VanillaLog.Jungle, null, null),
	 * 
	 * StonePine("Stone Pine", "pinus", "pinea", 0x0, 0x0,
	 * ILogType.ForestryLog.PINE, null, null), Redcedar, SitkaSpruce, Kauri,
	 * Macadamia, Pistashio, SandPear,
	 */

	;

	private static enum LeafType {

		Normal((short) 10, (short) 11, (short) 12),
		Conifer((short) 15, (short) 16, (short) 17),
		Jungle((short) 20, (short) 21, (short) 22),
		Willow((short) 25, (short) 26, (short) 27),
		Maple((short) 30, (short) 31, (short) 32),
		Palm((short) 35, (short) 36, (short) 37);

		public final short fancyUID;
		public final short plainUID;
		public final short changedUID;

		private LeafType(short fancyUID, short plainUID, short changedUID) {
			this.fancyUID = fancyUID;
			this.plainUID = plainUID;
			this.changedUID = changedUID;
		}
	}

	public static enum SaplingType {
		Default, Jungle, Conifer, Fruit, Poplar, Palm;
		Icon[] icon;
	}

	private LeafType leafType = LeafType.Normal;
	private SaplingType saplingType = SaplingType.Default;

	public static void init() {

		String bookArborist = "Arborist Manual";

		for (ExtraTreeSpecies species : values()) {
			species.preInit();
		}

		OrchardApple.setHeight(TreeHeight.Smaller);
		OrchardApple.setSappiness(Sappiness.Low);

		SweetCrabapple.setSappiness(Sappiness.Low);
		FloweringCrabapple.setSappiness(Sappiness.Average);
		PrairieCrabapple.setSappiness(Sappiness.Low);

		OrchardApple.finished();
		SweetCrabapple.finished();
		FloweringCrabapple.finished();
		PrairieCrabapple.finished();

		// Prune

		ExtraTreeSpecies[] pruneSpecies = new ExtraTreeSpecies[] { Blackthorn, CherryPlum, Almond,
				Apricot, Peach, Nectarine, WildCherry, SourCherry, BlackCherry };

		for (ExtraTreeSpecies species : pruneSpecies) {
			species.setSappiness(Sappiness.Low);
			IAlleleTreeSpecies citrus = (IAlleleTreeSpecies) AlleleManager.alleleRegistry
					.getAllele("forestry.treePlum");
			species.setWorldGen(citrus.getGeneratorClasses()[0]);
			species.setSappiness(Sappiness.Average);
			species.saplingType = SaplingType.Fruit;
			species.finished();
		}

		Blackthorn.setHeight(TreeHeight.Smallest);
		CherryPlum.setHeight(TreeHeight.Smallest);
		Almond.setHeight(TreeHeight.Smallest);
		Apricot.setHeight(TreeHeight.Smallest);
		Peach.setHeight(TreeHeight.Smallest);
		Nectarine.setHeight(TreeHeight.Smallest);
		SourCherry.setHeight(TreeHeight.Smallest);

		WildCherry.setSappiness(Sappiness.Lower);
		SourCherry.setSappiness(Sappiness.Low);
		BlackCherry.setSappiness(Sappiness.Low);

		// Citrus

		ExtraTreeSpecies[] citrusSpecies = new ExtraTreeSpecies[] { Orange, Manderin, Satsuma,
				Tangerine, Lime, KeyLime, FingerLime, Pomelo, Grapefruit, Kumquat, Citron,
				BuddhaHand };

		for (ExtraTreeSpecies species : citrusSpecies) {
			species.setSappiness(Sappiness.Low);
			species.setLeafType(LeafType.Jungle);
			species.saplingType = SaplingType.Fruit;
			IAlleleTreeSpecies citrus = (IAlleleTreeSpecies) AlleleManager.alleleRegistry
					.getAllele("forestry.treeLemon");
			species.setWorldGen(citrus.getGeneratorClasses()[0]);
			species.setSappiness(Sappiness.Lower);
			species.finished();
		}

		Orange.setHeight(TreeHeight.Smallest);
		Manderin.setHeight(TreeHeight.Smallest);
		Satsuma.setHeight(TreeHeight.Smallest);
		Tangerine.setHeight(TreeHeight.Smallest);

		Lime.setHeight(TreeHeight.Smallest);
		KeyLime.setHeight(TreeHeight.Smallest);
		FingerLime.setHeight(TreeHeight.Smallest);

		Pomelo.setHeight(TreeHeight.Smallest);
		Grapefruit.setHeight(TreeHeight.Smallest);
		Kumquat.setHeight(TreeHeight.Smallest);
		Citron.setHeight(TreeHeight.Smallest);
		BuddhaHand.setHeight(TreeHeight.Smallest);

		// CommonLime.setGirth(2);
		// CommonLime.setHeight(TreeHeight.Average);

		Banana.setLeafType(LeafType.Palm);
		RedBanana.setLeafType(LeafType.Palm);
		Plantain.setLeafType(LeafType.Palm);

		Banana.setSappiness(Sappiness.Low);
		Banana.finished();
		RedBanana.setSappiness(Sappiness.Low);
		RedBanana.finished();
		Plantain.setSappiness(Sappiness.Lower);
		Plantain.finished();

		Hemlock.setLeafType(LeafType.Conifer);
		Hemlock.setSappiness(Sappiness.Lower);

		Butternut.finished();
		Butternut.setHeight(TreeHeight.Average);
		Butternut.setGirth(2);
		Rowan.finished();
		Rowan.setHeight(TreeHeight.Smaller);
		Hemlock.finished();
		Hemlock.setHeight(TreeHeight.Larger);
		Hemlock.setGirth(2);
		Ash.finished();
		Ash.setHeight(TreeHeight.Average);
		Alder.finished();
		Alder.addDescription("A common tree in wet places and marshes.", bookArborist);
		Beech.finished();
		Beech.setHeight(TreeHeight.Average);
		Beech.addDescription(
				"An elegant tree with small inedible nuts, used in the manufacture of seed oil.",
				bookArborist);
		CopperBeech.finished();
		CopperBeech.setHeight(TreeHeight.Average);
		CopperBeech.addDescription(
				"Beautiful purple brown leaves make this tree a favourite of gardeners.",
				bookArborist);
		Aspen.finished();

		Yew.setLeafType(LeafType.Conifer);
		Cypress.setLeafType(LeafType.Conifer);
		DouglasFir.setLeafType(LeafType.Conifer);

		Yew.setSappiness(Sappiness.Lower);
		Cypress.setSappiness(Sappiness.Lower);
		DouglasFir.setSappiness(Sappiness.Lower);

		Yew.finished();
		Cypress.finished();
		Cypress.setHeight(TreeHeight.Large);
		Cypress.saplingType = SaplingType.Poplar;
		DouglasFir.finished();
		DouglasFir.setHeight(TreeHeight.Larger);
		DouglasFir.setGirth(2);
		Hazel.finished();
		Hazel.setHeight(TreeHeight.Smaller);
		Sycamore.finished();
		Sycamore.setHeight(TreeHeight.Average);
		Whitebeam.finished();
		Hawthorn.finished();
		Hawthorn.setHeight(TreeHeight.Smaller);
		Pecan.finished();
		Pecan.setHeight(TreeHeight.Average);

		Fir.setLeafType(LeafType.Conifer);
		Cedar.setLeafType(LeafType.Conifer);
		Sallow.setLeafType(LeafType.Willow);

		Elm.setSappiness(Sappiness.Lower);
		Fir.setSappiness(Sappiness.Lower);
		Cedar.setSappiness(Sappiness.Lower);

		Elm.finished();
		Elm.setHeight(TreeHeight.Large);
		Elder.finished();
		Elder.setHeight(TreeHeight.Smaller);
		Holly.finished();
		Holly.setHeight(TreeHeight.Smaller);
		Hornbeam.finished();
		Hornbeam.setHeight(TreeHeight.Average);
		Sallow.finished();
		Sallow.setHeight(TreeHeight.Smaller);
		AcornOak.finished();
		AcornOak.setHeight(TreeHeight.Large);
		AcornOak.setGirth(2);
		Fir.finished();
		Fir.setHeight(TreeHeight.Large);
		Cedar.finished();
		Cedar.setHeight(TreeHeight.Large);
		Cedar.setGirth(2);

		RedMaple.setLeafType(LeafType.Maple);
		BalsamFir.setLeafType(LeafType.Conifer);
		LoblollyPine.setLeafType(LeafType.Conifer);

		Olive.setSappiness(Sappiness.Lower);
		BalsamFir.setSappiness(Sappiness.Lower);
		LoblollyPine.setSappiness(Sappiness.Lower);
		Sweetgum.setSappiness(Sappiness.Lower);
		Locust.setSappiness(Sappiness.Average);

		Olive.finished();
		Olive.setHeight(TreeHeight.Smaller);
		RedMaple.finished();
		BalsamFir.finished();
		LoblollyPine.finished();
		LoblollyPine.setHeight(TreeHeight.Average);
		Sweetgum.finished();
		Sweetgum.setHeight(TreeHeight.Smaller);
		Locust.finished();
		Locust.setHeight(TreeHeight.Average);
		Pear.finished();
		Pear.setHeight(TreeHeight.Smallest);

		OsangeOsange.setSappiness(Sappiness.Lower);

		OsangeOsange.setLeafType(LeafType.Jungle);
		OldFustic.setLeafType(LeafType.Jungle);
		Brazilwood.setLeafType(LeafType.Jungle);
		Logwood.setLeafType(LeafType.Jungle);
		Rosewood.setLeafType(LeafType.Jungle);
		Purpleheart.setLeafType(LeafType.Jungle);

		OsangeOsange.finished();
		OsangeOsange.setHeight(TreeHeight.Smaller);
		OldFustic.finished();
		Brazilwood.finished();
		Brazilwood.setHeight(TreeHeight.Smaller);
		Logwood.finished();
		Logwood.setHeight(TreeHeight.Smaller);
		Rosewood.finished();
		Rosewood.setHeight(TreeHeight.Average);
		Purpleheart.finished();
		Purpleheart.setHeight(TreeHeight.Average);

		RoseGum.setSappiness(Sappiness.Low);
		SwampGum.setSappiness(Sappiness.Low);
		RainbowGum.setSappiness(Sappiness.Low);

		Gingko.setLeafType(LeafType.Jungle);
		Brazilnut.setLeafType(LeafType.Jungle);
		RoseGum.setLeafType(LeafType.Jungle);
		SwampGum.setLeafType(LeafType.Jungle);
		Coffee.setLeafType(LeafType.Jungle);
		MonkeyPuzzle.setLeafType(LeafType.Conifer);
		RainbowGum.setLeafType(LeafType.Jungle);

		Iroko.finished();
		Iroko.setHeight(TreeHeight.Large);
		Gingko.finished();
		Gingko.setHeight(TreeHeight.Average);
		Brazilnut.finished();
		Brazilnut.setHeight(TreeHeight.Large);
		RoseGum.finished();
		RoseGum.setHeight(TreeHeight.Larger);
		SwampGum.finished();
		SwampGum.setHeight(TreeHeight.Largest);
		SwampGum.setGirth(2);
		Box.finished();
		Box.setHeight(TreeHeight.Smallest);
		Clove.finished();
		Clove.setHeight(TreeHeight.Smaller);
		Coffee.finished();
		Coffee.setHeight(TreeHeight.Smaller);
		MonkeyPuzzle.finished();
		MonkeyPuzzle.setHeight(TreeHeight.Large);
		MonkeyPuzzle.setGirth(2);
		RainbowGum.finished();
		RainbowGum.setHeight(TreeHeight.Average);
		
		
		PinkIvory.setHeight(TreeHeight.Small);
		PinkIvory.finished();

		for (ExtraTreeSpecies species : ExtraTreeSpecies.values()) {
			String scientific = species.branchName.substring(0, 1).toUpperCase()
					+ species.branchName.substring(1).toLowerCase();
			String uid = "trees." + species.branchName.toLowerCase();
			IClassification branch = AlleleManager.alleleRegistry.getClassification("genus." + uid);
			if (branch == null)
				branch = AlleleManager.alleleRegistry.createAndRegisterClassification(
						EnumClassLevel.GENUS, uid, scientific);
			species.branch = branch;
			species.branch.addMemberSpecies(species);
		}

		/*
		 * 
		 * //Walnut BlackWalnut.setHeight(TreeHeight.Average);
		 * Butternut.setHeight(TreeHeight.Average);
		 * 
		 * BlackWalnut.setGirth(2);
		 * 
		 * // Sorbus WildServiceTree.setHeight(TreeHeight.Average);
		 * 
		 * // Hemlocks WesternHemlock.setHeight(TreeHeight.Larger);
		 * MountainHermlock.setHeight(TreeHeight.Large);
		 * EasternHemlock.setHeight(TreeHeight.Large);
		 * 
		 * WesternHemlock.setLeafIndex(leavesConifer);
		 * MountainHermlock.setLeafIndex(leavesConifer);
		 * EasternHemlock.setLeafIndex(leavesConifer);
		 * 
		 * WesternHemlock.setGirth(2);
		 * 
		 * // Spruces GiantSpruce.setHeight(TreeHeight.Larger);
		 * GiantSpruce.setLeafIndex(leavesConifer); GiantSpruce.setGirth(3);
		 * 
		 * 
		 * CommonAsh.setHeight(TreeHeight.Average);
		 * 
		 * CommonAlder.setHeight(TreeHeight.Average);
		 * 
		 * CommonBeech.setHeight(TreeHeight.Average);
		 * CopperBeech.setHeight(TreeHeight.Average);
		 * 
		 * //NorthernMaple.setHeight(TreeHeight.Average);
		 * SycamoreMaple.setHeight(TreeHeight.Average);
		 * SugarMaple.setHeight(TreeHeight.Average);
		 */
	}

	private String description;

	private void addDescription(String string, String book) {
		this.description = string + "|" + book;
	}

	final static ItemStack getEBXLStack(String name) {
		try {
			Class elements = Class.forName("extrabiomes.lib.Element");
			Method getElementMethod = elements.getMethod("valueOf", new Class[] { String.class });
			Method getItemStack = elements.getMethod("get", new Class[] {});
			Object element = getElementMethod
					.invoke(null, new Object[] { "SAPLING_AUTUMN_YELLOW" });
			return (ItemStack) getItemStack.invoke(element, new Object[] {});
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private void setWorldGen(Class<? extends WorldGenerator> gen) {
		this.gen = gen;
	}

	int girth = 1;

	private void setGirth(int i) {
		template[EnumTreeChromosome.GIRTH.ordinal()] = AlleleManager.alleleRegistry
				.getAllele("forestry.i" + i + "d");
	}

	public void preInit() {
		template = BinnieGenetics.getTreeRoot().getDefaultTemplate();
		template[0] = this;
		if (fruit != null)
			template[EnumTreeChromosome.FRUITS.ordinal()] = fruit;

		IClassification clas = AlleleManager.alleleRegistry.getClassification("trees." + branch);
		if (clas != null) {
			clas.addMemberSpecies(this);
			this.branch = clas;
		}

	}

	Class<? extends WorldGenerator> gen;

	ExtraTreeSpecies(String name, String branch, String binomial, int color, int polColor,
			ILogType wood, IAlleleFruit fruit, Class<? extends WorldGenerator> gen) {
		this.name = name;
		this.color = color;
		this.uid = this.toString().toLowerCase();
		this.wood = wood;
		this.fruit = fruit;
		this.gen = gen == null ? WorldGenTree.class : gen;
		this.branchName = branch;
		this.binomial = binomial;
	}

	IAlleleFruit fruit = null;

	IAllele[] template;

	int color;

	String name;
	String binomial;
	String uid;
	ILogType wood;

	String branchName;

	IClassification branch;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public EnumTemperature getTemperature() {
		return EnumTemperature.NORMAL;
	}

	@Override
	public EnumHumidity getHumidity() {
		return EnumHumidity.NORMAL;
	}

	@Override
	public boolean hasEffect() {
		return false;
	}

	@Override
	public boolean isSecret() {
		return false;
	}

	@Override
	public boolean isCounted() {
		return true;
	}

	@Override
	public String getBinomial() {
		return binomial;
	}

	@Override
	public String getAuthority() {
		return "Binnie";
	}

	@Override
	public IClassification getBranch() {
		return branch;
	}

	@Override
	public String getUID() {
		return "extratrees.species." + uid;
	}

	@Override
	public boolean isDominant() {
		return true;
	}

	@Override
	public EnumPlantType getPlantType() {
		return EnumPlantType.Plains;
	}

	@Override
	public int getGirth() {
		return girth;
	}

	@Override
	public WorldGenerator getGenerator(ITree tree, World world, int x, int y, int z) {
		if (gen != null) {
			try {
				return gen.getConstructor(ITree.class).newInstance(tree);
			} catch (Exception e) {
			}
		}
		return new WorldGenDefault(tree);
	}

	@Override
	public Class<? extends WorldGenerator>[] getGeneratorClasses() {
		return null;
	}

	void setLeafType(LeafType type) {
		this.leafType = type;
		if (leafType == LeafType.Conifer)
			saplingType = SaplingType.Conifer;
		if (leafType == LeafType.Jungle)
			saplingType = SaplingType.Jungle;
		if (leafType == LeafType.Palm)
			saplingType = SaplingType.Palm;
	};

	//
	// @Override
	// public int getLeafTextureIndex(ITree tree, boolean fancy) {
	// return leafIndex;
	// }
	//
	// @Override
	// public int getGermlingIconIndex(EnumGermlingType type) {
	// return 48;// + wood.ordinal();
	// }

	public IAllele[] getTemplate() {
		return template;
	}

	@Override
	public ArrayList<IFruitFamily> getSuitableFruit() {
		ArrayList<IFruitFamily> list = new ArrayList<IFruitFamily>();
		list.addAll(AlleleManager.alleleRegistry.getRegisteredFruitFamilies().values());
		return list;
	}

	public ILogType getLog() {
		return wood;
	}

	public void setHeight(TreeHeight height) {
		IAllele allele = height.getAllele();
		if (allele != null)
			template[EnumTreeChromosome.HEIGHT.ordinal()] = allele;
	}

	public void setSappiness(Sappiness height) {
		IAllele allele = height.getAllele();
		if (allele != null)
			template[EnumTreeChromosome.SAPPINESS.ordinal()] = allele;
	}

	public void setGrowthConditions(Growth growth) {
		IAllele allele = growth.getAllele();
		if (allele != null)
			template[EnumTreeChromosome.GROWTH.ordinal()] = allele;
	}

	enum TreeHeight {
		Smallest, Smaller, Small, Average, Large, Larger, Largest, Gigantic;

		public IAllele getAllele() {
			return AlleleManager.alleleRegistry.getAllele("forestry.height"
					+ (this == Average ? "Max10" : toString()));
		}
	}

	enum Growth {
		Tropical;

		public IAllele getAllele() {
			return AlleleManager.alleleRegistry.getAllele("forestry.growth" + toString());
		}
	}

	enum Sappiness {
		Lowest, Lower, Low, Average, High, Higher, Highest, ;

		public IAllele getAllele() {
			return AlleleManager.alleleRegistry.getAllele("forestry.sappiness" + toString());
		}
	}

	boolean finished = false;

	public boolean isFinished() {
		return finished;
	}

	public void finished() {
		finished = true;
	}

	@Override
	public int getLeafColour(ITree tree) {
		return this.color;
	}

	@Override
	public short getLeafIconIndex(ITree tree, boolean fancy) {
		if (!fancy)
			return leafType.plainUID;

		if (tree.getMate() != null)
			return leafType.changedUID;

		return leafType.fancyUID;
	}

	@Override
	public int getIconColour(int renderPass) {
		return renderPass == 0 ? this.color : 0x9F784D;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Icon getGermlingIcon(EnumGermlingType type, int renderPass) {
		if (type == EnumGermlingType.POLLEN)
			return ItemInterface.getItem("pollen").getItem()
					.getIconFromDamageForRenderPass(0, renderPass);

		return renderPass == 0 ? saplingType.icon[1] : saplingType.icon[0];
	}

	@Override
	public IIconProvider getIconProvider() {
		return this;
	}

	@Override
	public Icon getIcon(short texUID) {
		return null;
	}

	@Override
	public void registerItemIcons(IconRegister itemMap) {
		
	}
	
	@Override
	public void registerTerrainIcons(IconRegister register) {
		for (SaplingType type : SaplingType.values()) {
			type.icon = new Icon[2];
			type.icon[0] = ExtraTrees.proxy.getIcon(register, "saplings/"
					+ type.toString().toLowerCase() + ".trunk");
			type.icon[1] = ExtraTrees.proxy.getIcon(register, "saplings/"
					+ type.toString().toLowerCase() + ".leaves");
		}
	}

	@Override
	public ITreeRoot getRoot() {
		return BinnieGenetics.getTreeRoot();
	}

	@Override
	public float getResearchSuitability(ItemStack itemstack) {
		if (itemstack == null)
			return 0f;
		if (this.template[EnumTreeChromosome.FRUITS.ordinal()] instanceof ExtraTreeFruitGene) {
			ExtraTreeFruitGene fruit = (ExtraTreeFruitGene) this.template[EnumTreeChromosome.FRUITS
					.ordinal()];
			for (ItemStack stack : fruit.products.keySet())
				if (stack.isItemEqual(itemstack))
					return 1.0f;
		}

		if (itemstack.itemID == ItemInterface.getItem("honeyDrop").itemID)
			return 0.5f;
		else if (itemstack.itemID == ItemInterface.getItem("honeydew").itemID)
			return 0.7f;
		else if (itemstack.itemID == ItemInterface.getItem("beeComb").itemID)
			return 0.4f;
		else if (AlleleManager.alleleRegistry.isIndividual(itemstack))
			return 1.0f;

		for (Map.Entry<ItemStack, Float> entry : getRoot().getResearchCatalysts().entrySet()) {
			if (entry.getKey().isItemEqual(itemstack))
				return entry.getValue();
		}

		return 0f;
	}

	@Override
	public ItemStack[] getResearchBounty(World world, String researcher, IIndividual individual,
			int bountyLevel) {
		ArrayList<ItemStack> bounty = new ArrayList<ItemStack>();
		ItemStack research = null;
		if (world.rand.nextFloat() < ((float) 10 / bountyLevel)) {
			Collection<? extends IMutation> combinations = getRoot().getCombinations(this);
			if (combinations.size() > 0) {
				IMutation[] candidates = combinations.toArray(new IMutation[0]);
				research = AlleleManager.alleleRegistry.getMutationNoteStack(researcher,
						candidates[world.rand.nextInt(candidates.length)]);
			}
		}

		if (research != null)
			bounty.add(research);

		if (this.template[EnumTreeChromosome.FRUITS.ordinal()] instanceof ExtraTreeFruitGene) {
			ExtraTreeFruitGene fruit = (ExtraTreeFruitGene) this.template[EnumTreeChromosome.FRUITS
					.ordinal()];
			for (ItemStack stack : fruit.products.keySet()) {

				ItemStack stack2 = stack.copy();
				stack2.stackSize = world.rand.nextInt((int) ((float) bountyLevel / 2)) + 1;
				bounty.add(stack2);
			}
		}

		return bounty.toArray(new ItemStack[0]);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getGermlingColour(EnumGermlingType type, int renderPass) {
		if (type == EnumGermlingType.SAPLING)
			return renderPass == 0 ? getLeafColour(null) : getLog().getColour();
		return getLeafColour(null);
	}

	@Override
	public int getComplexity() {
		return 1 + getGeneticAdvancement(this, new ArrayList<IAllele>());
	}

	private int getGeneticAdvancement(IAllele species, ArrayList<IAllele> exclude) {

		int own = 1;
		int highest = 0;
		exclude.add(species);

		for (IMutation mutation : getRoot().getPaths(species, EnumBeeChromosome.SPECIES.ordinal())) {
			if (!exclude.contains(mutation.getAllele0())) {
				int otherAdvance = getGeneticAdvancement(mutation.getAllele0(), exclude);
				if (otherAdvance > highest)
					highest = otherAdvance;
			}
			if (!exclude.contains(mutation.getAllele1())) {
				int otherAdvance = getGeneticAdvancement(mutation.getAllele1(), exclude);
				if (otherAdvance > highest)
					highest = otherAdvance;
			}
		}

		return own + (highest < 0 ? 0 : highest);
	}

}
