package binnie.extratrees.genetics;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import binnie.core.genetics.BinnieGenetics;
import binnie.core.resource.BinnieResource;
import binnie.core.resource.ResourceManager;
import binnie.core.resource.ResourceType;
import binnie.extratrees.ExtraTrees;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.core.IIconProvider;
import forestry.api.core.ItemInterface;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IClassification;
import forestry.api.genetics.IIndividual;
import forestry.api.lepidopterology.EnumFlutterType;
import forestry.api.lepidopterology.IAlleleButterflySpecies;
import forestry.api.lepidopterology.IButterflyRoot;

public enum ButterflySpecies implements IAlleleButterflySpecies {
	
	
	WhiteAdmiral("White Admiral", "Limenitis camilla"),
	PurpleEmperor("Purple Emperor", "Apatura iris"),
	RedAdmiral("Red Admiral", "Vanessa atalanta"),
	PaintedLady("Painted Lady", "Vanessa cardui"),
	SmallTortoiseshell("Small Tortoiseshell", "Aglais urticae"),
	CamberwellBeauty("Camberwell Beauty", "Aglais antiopa"),
	Peacock("Peacock", "Inachis io"),
	Wall("Wall", "Lasiommata megera"),
	CrimsonRose("Crimson Rose", "Atrophaneura hector"),
	KaiserIHind("Kaiser-i-Hind", "Teinopalpus imperialis"),
	GoldenBirdwing("Golden Birdwing", "Troides aeacus"),
	MarshFritillary("Marsh Fritillary", "Euphydryas aurinia"),
	PearlBorderedFritillary("Pearl-bordered Fritillary", "Boloria euphrosyne"),
	QueenOfSpainFritillary("Queen of Spain Fritillary", "Issoria lathonia"),
	SpeckledWood("Speckled Wood", "Pararge aegeria"),
	ScotchAngus("Scotch Angus", "Erebia aethiops"),
	Gatekeeper("Gatekeeper", "Pyronia tithonus"),
	
	/*
	CommonBlue("Common Blue", "Polyommatus icarus"),
	Glasswing("Glasswing", "Greta oto"),
	EmeraldSwallowtail("Emerald Swallowtail", "Papilio palinurus"),
	KaiserHind("Kaiser-i-Hind", "Teinopalpus imperialis"),
	Peacock("Peacock", "Inachis io"),
	CrimsonRose("Crimson Rose", "Atrophaneura hector"),
	Buckeye("Buckeye", "Junonia coenia"),
	Monarch("Monarch", "Danaus plexippus"),
	BlueMorpho("Blue Morpho", "Morpho menelaus"),
	Cinnabar("Cinnabar", "Tyria jacobaeae"),
	DawnCloudedYellow("Dawn Clouded Yellow", "Colias aurorina"),
	OrangeTip("Orange Tip", "Anthocharis cardamines"),
	PaleCloudedYellow("Pale Clouded Yellow", "Colias hyale"),
	ScotchArgus("Scotch Argus", "Erebia aethiops"),
	Creeper("Greater Creep", "Creeperis creepa"),
	GreenSwallowtail("Green Swallowtail", "Papilio blumei"),
	
	PurpleEmperor("Purple Emperor", "Apatura iris"),
	Gatekeeper("Gatekeeper", "Pyronia tithonus"),
	SmallHeath("Small Heath", "Coenonympha pamphilus"),
	GreenHairstreak("Green Hairstreak", "Callophrys rubi"),
	BrownHairstreak("Brown Hairstreak", "Thecla betulae"),
	PurpleHairstreak("Purple Hairstreak", "Neozephyrus quercus"),
	LargeCopper("Large Copper", "Lycaena dispar"),
	SmallBlue("Small Blue", "Cupido minimus"),
	SilverStuddedBlue("Silver-studded Blue", "Plebejus argus"),
	AdonisBlue("Adonis Blue", "Lysandra bellargus"),
	HollyBlue("Holly Blue", "Celastrina argiolus"),
	
	*/
	;
	
	String name;
	String branchName;
	String scientific;
	BinnieResource texture;
	public IClassification branch;
	
	private Map<ItemStack, Float> butterflyLoot = new HashMap<ItemStack, Float>();
	private Map<ItemStack, Float> caterpillarLoot = new HashMap<ItemStack, Float>();
	
	private ButterflySpecies(String name, String scientific) {
		this.name = name;
		this.branchName = scientific.split(" ")[0].toLowerCase();
		this.scientific = scientific.split(" ")[1];
		this.texture = ResourceManager.getPNG(ExtraTrees.instance, ResourceType.Entity, this.toString());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return "";
	}

	@Override
	public EnumTemperature getTemperature() {
		return EnumTemperature.NORMAL;
	}

	@Override
	public EnumHumidity getHumidity() {
		return EnumHumidity.NORMAL;
	}

	@Override
	public boolean hasEffect() {
		return false;
	}

	@Override
	public boolean isSecret() {
		return false;
	}

	@Override
	public boolean isCounted() {
		return true;
	}

	@Override
	public String getBinomial() {
		return scientific;
	}

	@Override
	public String getAuthority() {
		return "Binnie";
	}

	@Override
	public IClassification getBranch() {
		return branch;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIconProvider getIconProvider() {
		return null;
	}

	@Override
	public String getUID() {
		return "extrabutterflies.species."+this.toString().toLowerCase();
	}

	@Override
	public boolean isDominant() {
		return true;
	}

	@Override
	public String getEntityTexture() {
		return texture.getFullPath();
	}
	
	public IAllele[] getTemplate() {
		IAllele[] def =  getRoot().getDefaultTemplate().clone();
		def[0] = this;
		return def;
	}

	@Override
	public IButterflyRoot getRoot() {
		return BinnieGenetics.getButterflyRoot();
	}

	@Override
	public float getRarity() {
		return 0.5f;
	}

	@Override
	public boolean isNocturnal() {
		return false;
	}

	@Override
	public int getIconColour(int renderPass) {
		return 0xFFFFFF;
	}

	@Override
	public Map<ItemStack, Float> getButterflyLoot() {
		return new HashMap<ItemStack, Float>();
	}

	@Override
	public Map<ItemStack, Float> getCaterpillarLoot() {
		return new HashMap<ItemStack, Float>();
	}

	@Override
	public int getComplexity() {
		return (int)((1.35f/0.5f)*1.5);
	}
	
	@Override
	public float getResearchSuitability(ItemStack itemstack) {
		if(itemstack == null)
			return 0f;

		if(itemstack.itemID == Item.glassBottle.itemID)
			return 0.9f;

		for(ItemStack stack : butterflyLoot.keySet())
			if(stack.isItemEqual(itemstack))
				return 1.0f;
		for(ItemStack stack : caterpillarLoot.keySet())
			if(stack.isItemEqual(itemstack))
				return 1.0f;
		
		if(itemstack.itemID == ItemInterface.getItem("honeyDrop").itemID)
			return 0.5f;
		else if(itemstack.itemID == ItemInterface.getItem("honeydew").itemID)
			return 0.7f;
		else if(itemstack.itemID == ItemInterface.getItem("beeComb").itemID)
			return 0.4f;
		else if(AlleleManager.alleleRegistry.isIndividual(itemstack))
			return 1.0f;
		
		for(Map.Entry<ItemStack, Float> entry : getRoot().getResearchCatalysts().entrySet()) {
			if(entry.getKey().isItemEqual(itemstack))
				return entry.getValue();
		}
		
		return 0f;
	}

	@Override
	public ItemStack[] getResearchBounty(World world, String researcher,
			IIndividual individual, int bountyLevel) {
		return new ItemStack[] { getRoot().getMemberStack(individual.copy(), EnumFlutterType.SERUM.ordinal()) };
	}

	@Override
	public EnumSet<Type> getSpawnBiomes() {
		return EnumSet.noneOf(BiomeDictionary.Type.class);
	}

	@Override
	public boolean strictSpawnMatch() {
		return false;
	}

	@Override
	public float getFlightDistance() {
		return 5f;
	}
	

}
