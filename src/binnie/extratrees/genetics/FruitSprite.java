package binnie.extratrees.genetics;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import binnie.core.resource.BinnieIcon;
import binnie.core.resource.ResourceManager;
import binnie.extratrees.ExtraTrees;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.core.IIconProvider;

public enum FruitSprite implements IIconProvider {

	Tiny,
	Small,
	Average,
	Large,
	Larger,
	Pear;

	BinnieIcon icon;
	
	public short getIndex() {
		return (short) (ordinal() + 4200);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public
	Icon getIcon(short texUID) {
		int index = texUID - 4200;
		if(index >= 0 && index < values().length)
			return values()[index].icon.getIcon();
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public
	void registerTerrainIcons(IconRegister register) {
	}

	@Override
	@SideOnly(Side.CLIENT)
	public
	void registerItemIcons(IconRegister itemMap) {
	}

	public void setup() {
		icon = ResourceManager.getBlockIcon(ExtraTrees.instance, "fruit/"+toString().toLowerCase());
	}
	
}
