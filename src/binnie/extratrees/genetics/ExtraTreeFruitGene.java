package binnie.extratrees.genetics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import binnie.core.genetics.BinnieGenetics;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;
import binnie.extratrees.block.FruitPod;
import binnie.extratrees.item.Food;
import forestry.api.arboriculture.EnumTreeChromosome;
import forestry.api.arboriculture.IAlleleFruit;
import forestry.api.arboriculture.IFruitProvider;
import forestry.api.arboriculture.ITreeGenome;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IFruitFamily;

public enum ExtraTreeFruitGene implements IAlleleFruit, IFruitProvider {

	Blackthorn("Blackthorn", 10, 0x6D8F1E, 0xDE2F69, FruitSprite.Small),
	CherryPlum("Cherry Plum", 10, 0x6D8F1E, 0xE81C4B, FruitSprite.Small),
	Peach("Peach", 10, 0x6D8F1E, 0xFAA023, FruitSprite.Average),
	Nectarine("Nectarine", 10, 0x6D8F1E, 0xFA5523, FruitSprite.Average),
	Apricot("Apricot", 10, 0x6D8F1E, 0xFACF23, FruitSprite.Average),
	Almond("Almond", 10, 0x6D8F1E, 0x8EE376, FruitSprite.Small),
	WildCherry("Wild Cherry", 10, 0x6D8F1E, 0xFF0000, FruitSprite.Tiny),
	SourCherry("Sour Cherry", 10, 0x6D8F1E, 0x9C092B, FruitSprite.Tiny),
	BlackCherry("Black Cherry", 10, 0x6D8F1E, 0x4A0A19, FruitSprite.Tiny),

	Orange("Orange", 10, 0x37F043, 0xFF940A, FruitSprite.Average),
	Manderin("Manderin", 10, 0x37F043, 0xFF940A, FruitSprite.Average),
	Tangerine("Tangerine", 10, 0x37F043, 0xFF940A, FruitSprite.Average),
	Satsuma("Satsuma", 10, 0x37F043, 0xFF940A, FruitSprite.Average),
	KeyLime("Key Lime", 10, 0x37F043, 0x9BFF44, FruitSprite.Small),
	Lime("Lime", 10, 0x37F043, 0x9BFF44, FruitSprite.Average),
	FingerLime("Finger Lime", 10, 0x37F043, 0xAA3B38, FruitSprite.Small),
	Pomelo("Pomelo", 10, 0x37F043, 0x5CD34A, FruitSprite.Larger),
	Grapefruit("Grape Fruit", 10, 0x37F043, 0xFF940A, FruitSprite.Large),
	Kumquat("Kumquat", 10, 0x37F043, 0xFF940A, FruitSprite.Small),
	Citron("Citron", 10, 0x37F043, 0xFFEC60, FruitSprite.Large),
	BuddhaHand("Buddha's Hand", 10, 0x37F043, 0xFFEC60, FruitSprite.Large),

	Apple("Apple", 10, 0x78C953, 0xF71616, FruitSprite.Average),
	Crabapple("Crabapple", 10, 0x78C953, 0xFFBD4C, FruitSprite.Average),

	Banana("Banana", FruitPod.Banana),
	RedBanana("Red Banana", FruitPod.RedBanana),
	Plantain("Platain", FruitPod.Plantain),

	Hazelnut("Hazelnut", 7, 0x7D791E, 0xDCB276, FruitSprite.Small), //
	Butternut("Butternut", 7, 0xB2B750, 0xF5B462, FruitSprite.Small), //
	Beechnut("Beechnut", 8, 0xDBBE7C, 0x5F3E35, FruitSprite.Tiny), //
	Pecan("Hickory Nut", 8, 0xA2AC4C, 0xF0CF89, FruitSprite.Small), //

	BrazilNut("Brazil Nut", 10, 0x59A769, 0x965530, FruitSprite.Large),

	Fig("Fig", 9, 0xD8B162, 0x6C3F46, FruitSprite.Small),
	Acorn("Acorn", 6, 0x72B226, 0xAD6A1D, FruitSprite.Tiny),
	Elderberry("Elderberry", 9, 0x71975D, 0x515B43, FruitSprite.Tiny),
	Olive("Olive", 9, 0x879E35, 0x62572A, FruitSprite.Small),
	GingkoNut("Gingko Nut", 7, 0x8C975B, 0xE5DAAD, FruitSprite.Tiny),
	Coffee("Coffee", 8, 0x716D1D, 0xF84F66, FruitSprite.Tiny),
	Pear("Pear", 10, 0x9F8F51, 0x9FD551, FruitSprite.Pear),
	OsangeOsange("Osange Osange", 10, 0x979752, 0xA2BF27, FruitSprite.Larger),
	Clove("Clove", 9, 0x687C2C, 0xAB4445, FruitSprite.Tiny), 

	Coconut("Coconut", FruitPod.Coconut),
	Cashew("Cashew", 8, 0xC4851C, 0xE94B17, FruitSprite.Average),
	Avacado("Avacado", 10, 0x9CBE72, 0x211F10, FruitSprite.Pear),
	Nutmeg("Nutmeg", 9, 0xE2C32D, 0xAC8355, FruitSprite.Tiny),
	Allspice("Allspice", 9, 0xE7A47A, 0x714636,FruitSprite.Tiny),
	Chilli("Chilli", 10, 0x716265, 0xE71832, FruitSprite.Small),
	StarAnise("Star Anise", 8, 0x85442E, 0xD45C05, FruitSprite.Tiny),
	Mango("Mango", 10, 0x658C15, 0xF2A636, FruitSprite.Average),
	StarFruit("Starfruit", 10, 0x95C20D, 0xE5D22E, FruitSprite.Average),
	Candlenut("Candlenut", 8, 0x7DA873, 0xDECAB2, FruitSprite.Small),
	
	Papayimar("Papayimar", FruitPod.Papayimar),
	
	
	
	;

	public static void init() {
		IFruitFamily familyPrune = AlleleManager.alleleRegistry.getFruitFamily("forestry.prunes");
		IFruitFamily familyPome = AlleleManager.alleleRegistry.getFruitFamily("forestry.pomes");
		IFruitFamily familyJungle = AlleleManager.alleleRegistry.getFruitFamily("forestry.jungle");
		IFruitFamily familyNuts = AlleleManager.alleleRegistry.getFruitFamily("forestry.nuts");

		Apple.addProduct(new ItemStack(Item.appleRed), 1f);
		Apple.setFamily(familyPome);
		Crabapple.addProduct(Food.Crabapple.get(1), 1f);
		Crabapple.setFamily(familyPome);

		Orange.addProduct(Food.Orange.get(1), 1f);
		Orange.setFamily(familyPrune);
		Manderin.addProduct(Food.Manderin.get(1), 1f);
		Manderin.setFamily(familyPrune);
		Tangerine.addProduct(Food.Tangerine.get(1), 1f);
		Tangerine.setFamily(familyPrune);
		Satsuma.addProduct(Food.Satsuma.get(1), 1f);
		Satsuma.setFamily(familyPrune);
		KeyLime.addProduct(Food.KeyLime.get(1), 1f);
		KeyLime.setFamily(familyPrune);
		Lime.addProduct(Food.Lime.get(1), 1f);
		Lime.setFamily(familyPrune);
		FingerLime.addProduct(Food.FingerLime.get(1), 1f);
		FingerLime.setFamily(familyPrune);
		Pomelo.addProduct(Food.Pomelo.get(1), 1f);
		Pomelo.setFamily(familyPrune);
		Grapefruit.addProduct(Food.Grapefruit.get(1), 1f);
		Grapefruit.setFamily(familyPrune);
		Kumquat.addProduct(Food.Kumquat.get(1), 1f);
		Kumquat.setFamily(familyPrune);
		Citron.addProduct(Food.Citron.get(1), 1f);
		Citron.setFamily(familyPrune);
		BuddhaHand.addProduct(Food.BuddhaHand.get(1), 1f);
		BuddhaHand.setFamily(familyPrune);

		Blackthorn.addProduct(Food.Blackthorn.get(1), 1f);
		Blackthorn.setFamily(familyPrune);
		CherryPlum.addProduct(Food.CherryPlum.get(1), 1f);
		CherryPlum.setFamily(familyPrune);
		Peach.addProduct(Food.Peach.get(1), 1f);
		Peach.setFamily(familyPrune);
		Nectarine.addProduct(Food.Nectarine.get(1), 1f);
		Nectarine.setFamily(familyPrune);
		Apricot.addProduct(Food.Apricot.get(1), 1f);
		Apricot.setFamily(familyPrune);
		Almond.addProduct(Food.Almond.get(1), 1f);
		Almond.setFamily(familyPrune);
		WildCherry.addProduct(Food.WildCherry.get(1), 1f);
		WildCherry.setFamily(familyPrune);
		SourCherry.addProduct(Food.SourCherry.get(1), 1f);
		SourCherry.setFamily(familyPrune);
		BlackCherry.addProduct(Food.BlackCherry.get(1), 1f);
		BlackCherry.setFamily(familyPrune);

		Hazelnut.addProduct(Food.Hazelnut.get(1), 1f);
		Hazelnut.setFamily(familyNuts);
		Butternut.addProduct(Food.Butternut.get(1), 1f);
		Butternut.setFamily(familyNuts);
		Beechnut.addProduct(Food.Beechnut.get(1), 1f);
		Beechnut.setFamily(familyNuts);
		Pecan.addProduct(Food.Pecan.get(1), 1f);
		Pecan.setFamily(familyNuts);

		Banana.addProduct(Food.Banana.get(1), 3.5f);
		Banana.setFamily(familyJungle);
		RedBanana.addProduct(Food.RedBanana.get(1), 3.5f);
		RedBanana.setFamily(familyJungle);
		Plantain.addProduct(Food.Plantain.get(1), 3.5f);
		Plantain.setFamily(familyJungle);

		BrazilNut.addProduct(Food.BrazilNut.get(1), 6.5f);
		BrazilNut.setFamily(familyNuts);

		Fig.addProduct(Food.Fig.get(1), 1f);
		Fig.setFamily(familyPrune);
		Acorn.addProduct(Food.Acorn.get(1), 1f);
		Acorn.setFamily(familyNuts);
		Elderberry.addProduct(Food.Elderberry.get(1), 1f);
		Elderberry.setFamily(familyPrune);
		Olive.addProduct(Food.Olive.get(1), 1f);
		Olive.setFamily(familyPrune);
		GingkoNut.addProduct(Food.GingkoNut.get(1), 1f);
		GingkoNut.setFamily(familyNuts);
		Coffee.addProduct(Food.Coffee.get(1), 1f);
		Coffee.setFamily(familyJungle);
		Pear.addProduct(Food.Pear.get(1), 1f);
		Pear.setFamily(familyPome);
		OsangeOsange.addProduct(Food.OsangeOrange.get(1), 1f);
		OsangeOsange.setFamily(familyPome);
		Clove.addProduct(Food.Clove.get(1), 1f);
		Clove.setFamily(familyNuts);
	}

	IFruitFamily family;

	private void setFamily(IFruitFamily family) {
		this.family = family;
	}

	boolean isRipening = false;

	int diffR, diffG, diffB = 0;

	ExtraTreeFruitGene(String name, int time, int unripe, int colour, FruitSprite index) {
		this.name = name;
		this.colour = colour;
		this.index = index;
		setRipening(time, unripe);
	}

	FruitPod pod = null;

	ExtraTreeFruitGene(String name, FruitPod pod) {
		this.name = name;
		this.pod = pod;
		this.ripeningPeriod = 2;
	}

	public void setRipening(int time, int unripe) {
		this.ripeningPeriod = time;
		this.colourUnripe = unripe;
		isRipening = true;

		diffR = (colour >> 16 & 255) - (unripe >> 16 & 255);
		diffG = (colour >> 8 & 255) - (unripe >> 8 & 255);
		diffB = (colour & 255) - (unripe & 255);
	}

	int ripeningPeriod = 0;
	int colourUnripe;
	int colour;
	FruitSprite index;

	String name;

	HashMap<ItemStack, Float> products = new HashMap<ItemStack, Float>();

	public void addProduct(ItemStack product, float chance) {
		while (chance > 1f) {
			products.put(product.copy(), 1f);
			chance--;
		}
		products.put(product, chance);
	}

	@Override
	public String getUID() {
		return "extratrees.fruit." + this.toString().toLowerCase();
	}

	@Override
	public boolean isDominant() {
		return true;
	}

	@Override
	public IFruitProvider getProvider() {
		return this;
	}

	@Override
	public ItemStack[] getProducts() {
		return products.keySet().toArray(new ItemStack[0]);
	}

	@Override
	public ItemStack[] getSpecialty() {
		return new ItemStack[0];
	}

	@Override
	public String getDescription() {
		return name;
	}

	@Override
	public IFruitFamily getFamily() {
		return family;
	}

	@Override
	public int getColour(ITreeGenome genome, IBlockAccess world, int x, int y, int z,
			int ripeningTime) {

		if (!isRipening)
			return colour;

		float stage = getRipeningStage(ripeningTime);

		int r = (colourUnripe >> 16 & 255) + (int) (diffR * stage);
		int g = (colourUnripe >> 8 & 255) + (int) (diffG * stage);
		int b = (colourUnripe & 255) + (int) (diffB * stage);

		// System.out.println(String.format("Calcultated rgb %s/%s/%s from %s and %s, resulting in %s",
		// r, g, b, colourCallow, stage, (r & 255) << 16 | (g & 255) << 8 | b &
		// 255));
		return (r & 255) << 16 | (g & 255) << 8 | b & 255;
	}

	@Override
	public boolean markAsFruitLeaf(ITreeGenome genome, World world, int x, int y, int z) {
		return pod == null;
	}

	@Override
	public int getRipeningPeriod() {
		return ripeningPeriod;
	}

	@Override
	public ItemStack[] getFruits(ITreeGenome genome, World world, int x, int y, int z,
			int ripeningTime) {

		if (pod != null) {
			if (ripeningTime >= 2) {
				List<ItemStack> product = new ArrayList<ItemStack>();
				for (Map.Entry<ItemStack, Float> entry : products.entrySet()) {
					if (world.rand.nextFloat() <= entry.getValue())
						product.add(entry.getKey().copy());
				}
				return product.toArray(new ItemStack[0]);
			} else {
				return new ItemStack[0];
			}
		}

		ArrayList<ItemStack> product = new ArrayList<ItemStack>();

		float stage = getRipeningStage(ripeningTime);
		if (stage < 0.5f)
			return new ItemStack[0];

		float modeYieldMod = 1f;// TreeManager.breedingManager.getTreekeepingMode(world).getYieldModifier(genome);

		for (Map.Entry<ItemStack, Float> entry : products.entrySet()) {
			if (world.rand.nextFloat() <= genome.getYield() * modeYieldMod * entry.getValue()
					* 5.0f * stage)
				product.add(entry.getKey().copy());
		}

		return product.toArray(new ItemStack[0]);
	}

	private float getRipeningStage(int ripeningTime) {
		if (ripeningTime >= ripeningPeriod)
			return 1.0f;

		return (float) ripeningTime / ripeningPeriod;
	}

	@Override
	public boolean requiresFruitBlocks() {
		return pod != null;
	}

	@Override
	public boolean trySpawnFruitBlock(ITreeGenome genome, World world, int x, int y, int z) {
		if (pod == null)
			return false;

		if (world.rand.nextFloat() > genome.getSappiness())
			return false;

		return BinnieGenetics.getTreeRoot().setFruitBlock(world,
				(IAlleleFruit) genome.getActiveAllele(EnumTreeChromosome.FRUITS.ordinal()),
				genome.getSappiness(), pod.getTextures(), x, y, z);
	}

	public boolean setFruitBlock(World world, IAlleleFruit allele, float sappiness, int x, int y,
			int z) {
		return true;
	}

	public static int getDirectionalMetadata(World world, int x, int y, int z) {
		for (int i = 0; i < 4; i++) {
			if (!isValidPot(world, x, y, z, i))
				continue;
			return i;
		}
		return -1;
	}

	public static boolean isValidPot(World world, int x, int y, int z, int notchDirection) {
		x += Direction.offsetX[notchDirection];
		z += Direction.offsetZ[notchDirection];
		int blockid = world.getBlockId(x, y, z);
		if (blockid == Block.wood.blockID) {
			return BlockLog.limitToValidMetadata(world.getBlockMetadata(x, y, z)) == 3;
		} else if (Block.blocksList[blockid] != null) {
			return Block.blocksList[blockid].isWood(world, x, y, z);
		} else
			return false;
	}

	@Override
	public short getIconIndex(ITreeGenome genome, IBlockAccess world, int x, int y, int z,
			int ripeningTime, boolean fancy) {
		return index.getIndex();
	}

	@Override
	public void registerIcons(/*IconRegister register*/) {
	}

	@Override
	public String getName() {
		return getDescription();
	}

	public String getNameOfFruit() {
		if (this == Apple)
			return "Apple";
		for (ItemStack stack : products.keySet()) {
			if (stack.getItem() == ExtraTrees.itemFood)
				return Food.values()[stack.getItemDamage()].toString();
		}
		return "NoFruit";
	}

}
