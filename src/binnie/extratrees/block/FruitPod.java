package binnie.extratrees.block;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import binnie.core.BinnieCore;
import binnie.extratrees.ExtraTrees;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.core.IIconProvider;

public enum FruitPod implements IIconProvider {
	
	Cocoa,
	Banana,
	Coconut,
	Plantain,
	RedBanana, 
	Papayimar
	;

	
	short[] textures = new short[] {BinnieCore.proxy.getUniqueTextureUID(), 
			BinnieCore.proxy.getUniqueTextureUID(), 
			BinnieCore.proxy.getUniqueTextureUID()};
	
	Icon[] icons = new Icon[3];

	public short[] getTextures() {
		return textures;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public
	Icon getIcon(short texUID) {
		int index = textures[0] - texUID;
		if(index >= 0 && index < 3)
			return icons[index];
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public
	void registerTerrainIcons(IconRegister register) {
		//icons[0] = ExtraTrees.proxy.getIcon(register, "pods/"+this.toString().toLowerCase()+".0");
		//icons[1] = ExtraTrees.proxy.getIcon(register, "pods/"+this.toString().toLowerCase()+".1");
		//icons[2] = ExtraTrees.proxy.getIcon(register, "pods/"+this.toString().toLowerCase()+".2");
		icons[0] = ExtraTrees.proxy.getIcon(register, "pods/"+this.toString().toLowerCase()+".0");
		icons[1] = ExtraTrees.proxy.getIcon(register, "pods/"+this.toString().toLowerCase()+".1");
		icons[2] = ExtraTrees.proxy.getIcon(register, "pods/"+this.toString().toLowerCase()+".2");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public
	void registerItemIcons(IconRegister itemMap) {
		
	}
	
}
