package binnie.extratrees.block;

import net.minecraft.util.Icon;
import binnie.extratrees.api.ICarpentryWood;

public interface IPlankType extends ICarpentryWood {
	
	Icon getIcon();

}
