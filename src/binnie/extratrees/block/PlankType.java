package binnie.extratrees.block;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import binnie.core.block.TileEntityMetadata;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.api.ICarpentryWood;

import com.google.common.base.Optional;

import forestry.api.core.BlockInterface;

public class PlankType {

	public enum ExtraTreePlanks implements IPlankType {

		Fir("Fir", 0xc38c54),
		Cedar("Cedar", 0xd86634),
		Hemlock("Hemlock", 0xc7b56c),
		Cypress("Cypress", 0xf6b85c),
		Fig("Fig", 0xc8882a),
		Beech("Beech", 0xe19951),
		Alder("Alder", 0xb88553),
		Hazel("Hazel", 0xcdb195),
		Hornbeam("Hornbeam", 0xc39860),
		Box("Box", 0xfbf1c6),
		Butternut("Butternut", 0xecaa7a),
		Hickory("Hickory", 0xdab48e),
		Whitebeam("Whitebeam", 0xc9c2b9),
		Elm("Elm", 0xf0a964),
		Apple("Apple", 0x603528),
		Yew("Yew", 0xe0a57a),
		Pear("Pear", 0xb8896d),
		Hawthorn("Hawthorn", 0xcc8362),
		Rowan("Rowan", 0xcfad9a),
		Elder("Elder", 0xbe9279),
		Maclura("Maclura", 0xf3b22e),
		Syzgium("Syzgium", 0xe6c3c1),
		Brazilwood("Brazilwood", 0x723e57),
		Logwood("Logwood", 0xa4372c),
		Iroko("Iroko", 0x753400),
		Locust("Locust", 0xc39160),
		Eucalyptus("Eucalyptus", 0xf6ab8b),
		Purpleheart("Purpleheart", 0x5b1c2f),
		Ash("Ash", 0xf5c768),
		Holly("Holly", 0xfbf6e7),
		Olive("Olive", 0xb0ad88),
		Sweetgum("Sweetgum", 0xd59658),
		Rosewood("Rosewood", 0x761500),
		Gingko("Gingko", 0xf4e7ba), 
		PinkIvory("Pink Ivory", 0xec8ca0);
		String name;
		int color;
		Icon icon;

		ExtraTreePlanks(String name, int color) {
			this.name = name;
			this.color = color;
		}

		public String getName() {
			return name;
		}

		public int getColour() {
			return color;
		}

		@Override
		public ItemStack getPlank() {
			return TileEntityMetadata.getItemStack(ExtraTrees.blockPlanks.blockID, ordinal());
		};

		public Icon loadIcon(IconRegister register) {
			icon = ExtraTrees.proxy.getIcon(register, "planks/" + toString());
			;
			return icon;
		}

		public Icon getIcon() {
			return icon;
		}

	}

	public enum VanillaPlanks implements IPlankType {
		OAK("Oak", 0xB4905A),
		SPRUCE("Spruce", 0x805E36),
		BIRCH("Birch", 0xD7C185),
		JUNGLE("Jungle", 0xB1805C), ;

		static {

		}

		String name;
		int color;

		VanillaPlanks(String name, int color) {
			this.name = name;
			this.color = color;
		}

		public String getName() {
			return name;
		}

		public int getColour() {
			return color;
		}

		@Override
		public ItemStack getPlank() {
			return new ItemStack(Block.planks.blockID, 1, ordinal());
		}

		@Override
		public Icon getIcon() {
			if (getPlank() != null) {
				int meta = getPlank().getItemDamage();
				Block block = Block.planks;
				return block.getIcon(2, meta);
			}
			return null;
		};

	}

	public enum ForestryPlanks implements IPlankType {
		LARCH("Larch", 0xd79f8d),
		TEAK("Teak", 0x7d7963),
		ACACIA("Acacia", 0x94b387),
		LIME("Lime", 0xceaa70),
		CHESTNUT("Chestnut", 0xbbaa5d),
		WENGE("Wenge", 0x5e564a),
		BAOBAB("Baobab", 0x929c62),
		SEQUOIA("Sequoia", 0x995a57),
		KAPOK("Kapok", 0x7c7434),
		EBONY("Ebony", 0x3c3730),
		MAHOGANY("Mahogany", 0x763f38),
		BALSA("Balsa", 0xa9a299),
		WILLOW("Willow", 0xb2b162),
		WALNUT("Walnut", 0x685242),
		GREENHEART("Greenheart", 0x4e7e5c),
		CHERRY("Cherry", 0xb58234),

		MAHOE("Mahoe", 0x7f98aa),
		POPLAR("Poplar", 0xcfcf82),
		PALM("Palm", 0xca804b),
		PAPAYA("Papaya", 0xdccb75),
		PINE("Pine", 0xc49e51),
		PLUM("Plum", 0xad687f),
		MAPLE("Maple", 0xae6d2b),
		CITRUS("Citrus", 0x9ca81d), ;

		String name;
		int color;

		ForestryPlanks(String name, int color) {
			this.name = name;
			this.color = color;
		}

		public String getName() {
			return name;
		}

		public int getColour() {
			return color;
		}

		@Override
		public ItemStack getPlank() {
			ItemStack stack = BlockInterface.getBlock("planks" + (ordinal() / 16 + 1));
			return new ItemStack(stack.getItem(), 1, ordinal() % 16);
		}

		@Override
		public Icon getIcon() {
			if (getPlank() != null) {
				int meta = getPlank().getItemDamage();
				Block block = Block.blocksList[((ItemBlock) getPlank().getItem()).getBlockID()];
				return block.getIcon(2, meta);
			}
			return null;
		};

	}

	public enum ExtraBiomesPlank implements IPlankType {
		Redwood("Redwood", 0x9B6B42),
		Fir("Scottish Fir", 0x7E774A),
		Acacia("Excel Acacia", 0xBFAA7E), ;

		String name;
		int color;

		ExtraBiomesPlank(String name, int color) {
			this.name = name;
			this.color = color;
		}

		public String getName() {
			return name;
		}

		public int getColour() {
			return color;
		}

		@Override
		public ItemStack getPlank() {
			try {
				Class clss = Class.forName("extrabiomes.api.Stuff");
				Block block = ((Optional<? extends Block>) clss.getField("planks").get(null)).get();
				return new ItemStack(block, 1, ordinal());
			} catch (Exception e) {
				return null;
			}
		};

		@Override
		public Icon getIcon() {
			if (getPlank() != null) {
				int meta = getPlank().getItemDamage();
				Block block = Block.blocksList[((ItemBlock) getPlank().getItem()).getBlockID()];
				return block.getIcon(2, meta);
			}
			return null;
		};

	}

	public static void setup() {

		for (VanillaPlanks plank : VanillaPlanks.values())
			CarpentryManager.carpentryInterface.registerCarpentryWood(plank.ordinal(), plank);

		for (ExtraTreePlanks plank : ExtraTreePlanks.values())
			CarpentryManager.carpentryInterface.registerCarpentryWood(plank.ordinal() + 32, plank);

		for (ForestryPlanks plank : ForestryPlanks.values())
			CarpentryManager.carpentryInterface.registerCarpentryWood(plank.ordinal() + 128, plank);

		for (ExtraBiomesPlank plank : ExtraBiomesPlank.values())
			CarpentryManager.carpentryInterface.registerCarpentryWood(plank.ordinal() + 192, plank);
	}

	public static IPlankType getPlank(int index) {
		ICarpentryWood wood = CarpentryManager.carpentryInterface.getCarpentryWood(index);
		if (wood instanceof IPlankType)
			return (IPlankType) wood;
		return ExtraTreePlanks.Fir;
	}

	public static int indexOf(IPlankType type) {
		int index = CarpentryManager.carpentryInterface.getCarpentryWoodIndex(type);
		return index < 0 ? 0 : index;
	}

	public static ItemStack getGate(IPlankType plank) {
		return TileEntityMetadata.getItemStack(ExtraTrees.blockGate.blockID, indexOf(plank));
	}

	public static ItemStack getFence(IPlankType plank) {
		if(plank instanceof ForestryPlanks) {
			int ordinal = ((ForestryPlanks)plank).ordinal();
			while(ordinal >=16) ordinal -= 16;
			
			ItemStack fence = BlockInterface.getBlock("fences"+((((ForestryPlanks) plank).ordinal()/16)+1));
			fence.setItemDamage(ordinal);
			return fence;
		}
		return TileEntityMetadata.getItemStack(ExtraTrees.blockFence.blockID, indexOf(plank));
	}

	public static ItemStack getDoor(IPlankType plank, DoorType type) {
		return TileEntityMetadata.getItemStack(ExtraTrees.blockDoor.blockID, (type.ordinal() * 256) + indexOf(plank));
	}

	public static List<IPlankType> getAllPlankTypes() {
		List<IPlankType> list = new ArrayList<IPlankType>();
		for (IPlankType type : ExtraTreePlanks.values())
			list.add(type);
		for (IPlankType type : ForestryPlanks.values())
			list.add(type);
		for (IPlankType type : ExtraBiomesPlank.values())
			list.add(type);
		for (IPlankType type : VanillaPlanks.values())
			list.add(type);
		return list;
	}

}
