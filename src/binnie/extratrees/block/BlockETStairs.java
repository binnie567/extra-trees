package binnie.extratrees.block;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import binnie.core.block.BlockMetadata;
import binnie.core.block.IBlockMetadata;
import binnie.core.block.TileEntityMetadata;
import binnie.extratrees.PluginExtraTrees;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.core.Tabs;

public class BlockETStairs extends BlockStairs implements IBlockMetadata {

	@Override
	public int getRenderType() {
		return PluginExtraTrees.stairsID;
	}

	public BlockETStairs(int id, Block par2Block) {
		super(id, par2Block, 0);
		setCreativeTab(Tabs.tabArboriculture);
		setUnlocalizedName("stairs");
		setResistance(5.0F);
		setHardness(2.0F);
		setStepSound(soundWoodFootstep);
	}

	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List itemList) {
		for (int i = 0; i < PlankType.ExtraTreePlanks.values().length; i++) {
			itemList.add(new ItemStack(this, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Icon getBlockTexture(IBlockAccess world, int x, int y, int z, int side) {
		TileEntityMetadata tile = TileEntityMetadata.getTile(world, x, y, z);
		if (tile != null)
			return getIcon(side, tile.getTileMetadata());
		return super.getBlockTexture(world, x, y, z, side);
	}

	@Override
	public Icon getIcon(int side, int meta) {
		return PlankType.getPlank(meta).getIcon();
	}

	@Override
	public void dropAsStack(World world, int x, int y, int z, ItemStack drop) {
		this.dropBlockAsItem_do(world, x, y, z, drop);
	}

	@Override
	public ArrayList<ItemStack> getBlockDropped(World world, int x, int y, int z, int blockMeta,
			int fortune) {
		return BlockMetadata.getBlockDropped(this, world, x, y, z, blockMeta);
	}

	@Override
	public boolean removeBlockByPlayer(World world, EntityPlayer player, int x, int y, int z) {
		return BlockMetadata.breakBlock(this, player, world, x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityMetadata();
	}

	@Override
	public boolean hasTileEntity(int meta) {
		return true;
	}

	public boolean onBlockEventReceived(World par1World, int par2, int par3, int par4, int par5,
			int par6) {
		super.onBlockEventReceived(par1World, par2, par3, par4, par5, par6);
		TileEntity tileentity = par1World.getBlockTileEntity(par2, par3, par4);
		return tileentity != null ? tileentity.receiveClientEvent(par5, par6) : false;
	}

	@Override
	public int getPlacedMeta(ItemStack stack, World world, int x, int y, int z,
			ForgeDirection clickedBlock) {
		return TileEntityMetadata.getItemDamage(stack);
	}

	@Override
	public int getDroppedMeta(int blockMeta, int tileMeta) {
		return tileMeta;
	}

	@Override
	public String getBlockName(ItemStack par1ItemStack) {
		int meta = TileEntityMetadata.getItemDamage(par1ItemStack);
		return PlankType.ExtraTreePlanks.values()[meta].getName() + " Stairs";
	}

	@Override
	public void getBlockTooltip(ItemStack par1ItemStack, List par3List) {
	}

	@Override
	public int getBlockID() {
		return blockID;
	}

	public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6) {
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
		par1World.removeBlockTileEntity(par2, par3, par4);
	}

}
