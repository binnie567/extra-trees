package binnie.extratrees.block;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;

import binnie.extratrees.PluginExtraTrees;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class FenceRenderer implements ISimpleBlockRenderingHandler {

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {

		Tessellator tess = Tessellator.instance;

		for (int i = 0; i < 4; ++i) {

			float thickness = 0.125F;

			if (i == 0) {
				block.setBlockBounds(0.5F - thickness, 0.0F, 0.0F, 0.5F + thickness, 1.0F,
						thickness * 2.0F);
			}

			if (i == 1) {
				block.setBlockBounds(0.5F - thickness, 0.0F, 1.0F - thickness * 2.0F,
						0.5F + thickness, 1.0F, 1.0F);
			}

			thickness = 0.0625F;

			if (i == 2) {
				block.setBlockBounds(0.5F - thickness, 1.0F - thickness * 3.0F, -thickness * 2.0F,
						0.5F + thickness, 1.0F - thickness, 1.0F + thickness * 2.0F);
			}

			if (i == 3) {
				block.setBlockBounds(0.5F - thickness, 0.5F - thickness * 3.0F, -thickness * 2.0F,
						0.5F + thickness, 0.5F - thickness, 1.0F + thickness * 2.0F);
			}

			renderer.setRenderBoundsFromBlock(block);

			GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
			tess.startDrawingQuads();
			tess.setNormal(0.0F, -1.0F, 0.0F);
			renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, metadata));
			tess.draw();
			tess.startDrawingQuads();
			tess.setNormal(0.0F, 1.0F, 0.0F);
			renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, metadata));
			tess.draw();
			tess.startDrawingQuads();
			tess.setNormal(0.0F, 0.0F, -1.0F);
			renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, metadata));
			tess.draw();
			tess.startDrawingQuads();
			tess.setNormal(0.0F, 0.0F, 1.0F);
			renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, metadata));
			tess.draw();
			tess.startDrawingQuads();
			tess.setNormal(-1.0F, 0.0F, 0.0F);
			renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, metadata));
			tess.draw();
			tess.startDrawingQuads();
			tess.setNormal(1.0F, 0.0F, 0.0F);
			renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, metadata));
			tess.draw();
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		}

		block.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		renderer.setRenderBoundsFromBlock(block);

	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int par2, int par3, int par4, Block block,
			int modelId, RenderBlocks renderer) {
		/*
		 * BlockFence blockFence = (BlockFence) block;
		 * 
		 * float sth = 1 / 16f;
		 * 
		 * boolean var5 = false; float var6 = 0.375F; float var7 = 0.625F;
		 * renderer.setRenderBounds((double) var6, 0.0D, (double) var6, (double)
		 * var7, 1.0D, (double) var7); renderer.renderStandardBlock(block, par2,
		 * par3, par4); var5 = true; boolean var8 = false; boolean var9 = false;
		 * 
		 * if (blockFence.canConnectFenceTo(renderer.blockAccess, par2 - 1,
		 * par3, par4) || blockFence.canConnectFenceTo(renderer.blockAccess,
		 * par2 + 1, par3, par4)) { var8 = true; }
		 * 
		 * if (blockFence.canConnectFenceTo(renderer.blockAccess, par2, par3,
		 * par4 - 1) || blockFence.canConnectFenceTo(renderer.blockAccess, par2,
		 * par3, par4 + 1)) { var9 = true; }
		 * 
		 * boolean var10 = blockFence.canConnectFenceTo(renderer.blockAccess,
		 * par2 - 1, par3, par4); boolean var11 =
		 * blockFence.canConnectFenceTo(renderer.blockAccess, par2 + 1, par3,
		 * par4); boolean var12 =
		 * blockFence.canConnectFenceTo(renderer.blockAccess, par2, par3, par4 -
		 * 1); boolean var13 =
		 * blockFence.canConnectFenceTo(renderer.blockAccess, par2, par3, par4 +
		 * 1);
		 * 
		 * if (!var8 && !var9) { var8 = true; }
		 * 
		 * var6 = 0.46875F; var7 = 0.53125F; float var14 = 0.75F; float var15 =
		 * 0.8625F; float var16 = var10 ? 0.0F : var6; float var17 = var11 ?
		 * 1.0F : var7; float var18 = var12 ? 0.0F : var6; float var19 = var13 ?
		 * 1.0F : var7;
		 * 
		 * if (var8) { renderer.setRenderBounds((double) var16, (double) var14,
		 * (double) var6, (double) var17, (double) var15, (double) var7);
		 * renderer.renderStandardBlock(blockFence, par2, par3, par4);
		 * 
		 * if(var10) { renderer.setRenderBounds((double) 1*sth, (double) 0,
		 * (double) 6.5f*sth, (double) 4*sth, (double) 15*sth, (double)
		 * 9.5f*sth); renderer.renderStandardBlock(blockFence, par2, par3,
		 * par4);
		 * 
		 * }
		 * 
		 * var5 = true; }
		 * 
		 * if (var9) { renderer.setRenderBounds((double) var6, (double) var14,
		 * (double) var18, (double) var7, (double) var15, (double) var19);
		 * renderer.renderStandardBlock(blockFence, par2, par3, par4); var5 =
		 * true; }
		 * 
		 * var14 = 0.375F; var15 = 0.5F;
		 * 
		 * if (var8) { renderer.setRenderBounds((double) var16, (double) var14,
		 * (double) var6, (double) var17, (double) var15, (double) var7);
		 * renderer.renderStandardBlock(blockFence, par2, par3, par4); var5 =
		 * true; }
		 * 
		 * if (var9) { renderer.setRenderBounds((double) var6, (double) var14,
		 * (double) var18, (double) var7, (double) var15, (double) var19);
		 * renderer.renderStandardBlock(blockFence, par2, par3, par4); var5 =
		 * true; }
		 * 
		 * blockFence.setBlockBoundsBasedOnState(renderer.blockAccess, par2,
		 * par3, par4); return var5;
		 */

		BlockFence blockFence = (BlockFence) block;

		float sth = 1 / 16f;

		boolean var5 = false;
		float var6 = 0.375F;
		float var7 = 0.625F;
		renderer.setRenderBounds((double) var6, 0.0D, (double) var6, (double) var7, 1.0D,
				(double) var7);
		renderer.renderStandardBlock(block, par2, par3, par4);
		var5 = true;
		boolean var8 = false;
		boolean var9 = false;

		if (blockFence.canConnectFenceTo(renderer.blockAccess, par2 - 1, par3, par4)
				|| blockFence.canConnectFenceTo(renderer.blockAccess, par2 + 1, par3, par4)) {
			var8 = true;
		}

		if (blockFence.canConnectFenceTo(renderer.blockAccess, par2, par3, par4 - 1)
				|| blockFence.canConnectFenceTo(renderer.blockAccess, par2, par3, par4 + 1)) {
			var9 = true;
		}

		boolean var10 = blockFence.canConnectFenceTo(renderer.blockAccess, par2 - 1, par3, par4);
		boolean var11 = blockFence.canConnectFenceTo(renderer.blockAccess, par2 + 1, par3, par4);
		boolean var12 = blockFence.canConnectFenceTo(renderer.blockAccess, par2, par3, par4 - 1);
		boolean var13 = blockFence.canConnectFenceTo(renderer.blockAccess, par2, par3, par4 + 1);

		if (!var8 && !var9) {
			var8 = true;
		}

		var6 = 0.4375F;
		var7 = 0.5625F;
		float var14 = 0.75F;
		float var15 = 0.9375F;
		float var16 = var10 ? 0.0F : var6;
		float var17 = var11 ? 1.0F : var7;
		float var18 = var12 ? 0.0F : var6;
		float var19 = var13 ? 1.0F : var7;

		if (var8) {
			renderer.setRenderBounds((double) var16, (double) var14, (double) var6, (double) var17,
					(double) var15, (double) var7);
			renderer.renderStandardBlock(blockFence, par2, par3, par4);
			var5 = true;
		}

		if (var9) {
			renderer.setRenderBounds((double) var6, (double) var14, (double) var18, (double) var7,
					(double) var15, (double) var19);
			renderer.renderStandardBlock(blockFence, par2, par3, par4);
			var5 = true;
		}

		var14 = 0.375F;
		var15 = 0.5625F;

		if (var8) {
			renderer.setRenderBounds((double) var16, (double) var14, (double) var6, (double) var17,
					(double) var15, (double) var7);
			renderer.renderStandardBlock(blockFence, par2, par3, par4);
			var5 = true;
		}

		if (var9) {
			renderer.setRenderBounds((double) var6, (double) var14, (double) var18, (double) var7,
					(double) var15, (double) var19);
			renderer.renderStandardBlock(blockFence, par2, par3, par4);
			var5 = true;
		}

		blockFence.setBlockBoundsBasedOnState(renderer.blockAccess, par2, par3, par4);
		return var5;

	}

	@Override
	public boolean shouldRender3DInInventory() {
		return true;
	}

	@Override
	public int getRenderId() {
		return PluginExtraTrees.fenceID;
	}

}
