package binnie.extratrees.block;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import binnie.core.block.TileEntityMetadata;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemETStairs extends ItemBlock {

	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return PlankType.ExtraTreePlanks.values()[par1].getIcon();
	}

	public ItemETStairs(int i) {
		super(i);
		//this.setTextureFile(ExtraTreeTexture.Log.getTexture());
		this.setCreativeTab(CreativeTabs.tabBlock);
		//this.setIconIndex(0);
		setUnlocalizedName("stairs");
	}
	
	@Override
	public int getMetadata(int i) {
		return 0;
	}
	
	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return PlankType.ExtraTreePlanks.values()[itemstack.getItemDamage()].getName() + " Stairs";
	}

	public boolean placeBlockAt(ItemStack stack, EntityPlayer player,
			World world, int x, int y, int z, int side, float hitX, float hitY,
			float hitZ, int metadata) {
		boolean done = super.placeBlockAt(stack, player, world, x, y, z, side,
				hitX, hitY, hitZ, metadata);
		TileEntityMetadata tile = (TileEntityMetadata) world
				.getBlockTileEntity(x, y, z);
		if (tile != null)
			tile.setTileMetadata(stack.getItemDamage());
		return done;

	}
}
