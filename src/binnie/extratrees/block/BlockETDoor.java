package binnie.extratrees.block;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.IconFlipped;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import binnie.core.block.BlockMetadata;
import binnie.core.block.IBlockMetadata;
import binnie.core.block.TileEntityMetadata;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.core.Tabs;

public class BlockETDoor extends BlockDoor implements IBlockMetadata {

	
	
	
	
	private Icon getFlippedIcon(boolean upper, boolean flip, int tileMeta) {
		DoorType type = getDoorType(tileMeta);
		return upper ? (flip ? type.iconDoorUpperFlip : type.iconDoorUpper) : (flip ? type.iconDoorLowerFlip : type.iconDoorLower);
	}
	
	public static DoorType getDoorType(int tileMeta) {
		int type = (tileMeta & 0xf00) >> 8;
		if(type >= 0 && type < DoorType.values().length)
			return DoorType.values()[type];
		return DoorType.Standard;
	}

	protected BlockETDoor(int par1) {
		super(par1, Material.wood);
		setHardness(3.0F).setStepSound(soundWoodFootstep);
		setCreativeTab(Tabs.tabArboriculture);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	public Icon getIcon(int side, int meta) {
		return DoorType.Standard.iconDoorLower;
	}

	@SideOnly(Side.CLIENT)
	/**
	 * Retrieves the block texture to use based on the display side. Args: iBlockAccess, x, y, z, side
	 */
	public Icon getBlockTexture(IBlockAccess par1IBlockAccess, int par2, int par3, int par4,
			int par5) {
		if (par5 != 1 && par5 != 0) {
			int i1 = this.getFullMetadata(par1IBlockAccess, par2, par3, par4);
			int j1 = i1 & 3;
			boolean flag = (i1 & 4) != 0;
			boolean flag1 = false;
			boolean flag2 = (i1 & 8) != 0;

			if (flag) {
				if (j1 == 0 && par5 == 2) {
					flag1 = !flag1;
				} else if (j1 == 1 && par5 == 5) {
					flag1 = !flag1;
				} else if (j1 == 2 && par5 == 3) {
					flag1 = !flag1;
				} else if (j1 == 3 && par5 == 4) {
					flag1 = !flag1;
				}
			} else {
				if (j1 == 0 && par5 == 5) {
					flag1 = !flag1;
				} else if (j1 == 1 && par5 == 3) {
					flag1 = !flag1;
				} else if (j1 == 2 && par5 == 4) {
					flag1 = !flag1;
				} else if (j1 == 3 && par5 == 2) {
					flag1 = !flag1;
				}

				if ((i1 & 16) != 0) {
					flag1 = !flag1;
				}
			}
			// FIXME: Change to correctly get upper/lower
			int tileMeta = 0;
			if(flag2)
				tileMeta = TileEntityMetadata.getTileMetadata(par1IBlockAccess, par2, par3-1, par4);
			else
				tileMeta = TileEntityMetadata.getTileMetadata(par1IBlockAccess, par2, par3, par4);
			return getFlippedIcon(flag2, flag1, tileMeta);
		} else {
			return DoorType.Standard.iconDoorLower;
		}
	}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		for(DoorType type : DoorType.values()) {
			type.iconDoorLower = ExtraTrees.proxy.getIcon(register, "door."+type.iconName+".lower");
			type.iconDoorUpper = ExtraTrees.proxy.getIcon(register, "door."+type.iconName+".upper");
			type.iconDoorLowerFlip = new IconFlipped(type.iconDoorLower, true, false);
			type.iconDoorUpperFlip = new IconFlipped(type.iconDoorUpper, true, false);
		}
		
	}

	public int getRenderType() {
		return PluginExtraTrees.doorRenderId;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int colorMultiplier(IBlockAccess par1IBlockAccess, int par2, int par3, int par4) {
    	
    		int par5 = 2;
            int i1 = this.getFullMetadata(par1IBlockAccess, par2, par3, par4);
            int j1 = i1 & 3;
            boolean flag = (i1 & 4) != 0;
            boolean flag1 = false;
            boolean flag2 = (i1 & 8) != 0;

            if (flag)
            {
                if (j1 == 0 && par5 == 2)
                {
                    flag1 = !flag1;
                }
                else if (j1 == 1 && par5 == 5)
                {
                    flag1 = !flag1;
                }
                else if (j1 == 2 && par5 == 3)
                {
                    flag1 = !flag1;
                }
                else if (j1 == 3 && par5 == 4)
                {
                    flag1 = !flag1;
                }
            }
            else
            {
                if (j1 == 0 && par5 == 5)
                {
                    flag1 = !flag1;
                }
                else if (j1 == 1 && par5 == 3)
                {
                    flag1 = !flag1;
                }
                else if (j1 == 2 && par5 == 4)
                {
                    flag1 = !flag1;
                }
                else if (j1 == 3 && par5 == 2)
                {
                    flag1 = !flag1;
                }

                if ((i1 & 16) != 0)
                {
                    flag1 = !flag1;
                }
            }
            // FIXME: Change to correctly get upper/lower
            if(flag2) {
            	 int meta = TileEntityMetadata.getTileMetadata(par1IBlockAccess, par2, par3-1, par4);
         		return PlankType.getPlank(meta & 255).getColour();
            }
            else {
            	 int meta = TileEntityMetadata.getTileMetadata(par1IBlockAccess, par2, par3, par4);
         		return PlankType.getPlank(meta & 255).getColour();
            }
           
		
	}

	/*
	 * public boolean canPlaceBlockAt(World par1World, int par2, int par3, int
	 * par4) { return par3 >= 255 ? false :
	 * par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4) &&
	 * super.canPlaceBlockAt(par1World, par2, par3, par4) &&
	 * super.canPlaceBlockAt(par1World, par2, par3 + 1, par4); }
	 */

	public int getFullMetadata(IBlockAccess par1IBlockAccess, int par2, int par3, int par4) {
		int l = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		boolean flag = (l & 8) != 0;
		int i1;
		int j1;

		if (flag) {
			i1 = par1IBlockAccess.getBlockMetadata(par2, par3 - 1, par4);
			j1 = l;
		} else {
			i1 = l;
			j1 = par1IBlockAccess.getBlockMetadata(par2, par3 + 1, par4);
		}

		boolean flag1 = (j1 & 1) != 0;
		return i1 & 7 | (flag ? 8 : 0) | (flag1 ? 16 : 0);
	}

	@SideOnly(Side.CLIENT)
	public void onBlockHarvested(World par1World, int par2, int par3, int par4, int par5,
			EntityPlayer par6EntityPlayer) {
		if (par6EntityPlayer.capabilities.isCreativeMode && (par5 & 8) != 0
				&& par1World.getBlockId(par2, par3 - 1, par4) == this.blockID) {
			par1World.setBlockToAir(par2, par3 - 1, par4);
		}
	}

	@Override
	public ArrayList<ItemStack> getBlockDropped(World world, int x, int y, int z, int blockMeta,
			int fortune) {
		return BlockMetadata.getBlockDropped(this, world, x, y, z, blockMeta);
	}

	@Override
	public boolean removeBlockByPlayer(World world, EntityPlayer player, int x, int y, int z) {
		return BlockMetadata.breakBlock(this, player, world, x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityMetadata();
	}

	@Override
	public boolean hasTileEntity(int meta) {
		return true;
	}

	public boolean onBlockEventReceived(World par1World, int par2, int par3, int par4, int par5,
			int par6) {
		super.onBlockEventReceived(par1World, par2, par3, par4, par5, par6);
		TileEntity tileentity = par1World.getBlockTileEntity(par2, par3, par4);
		return tileentity != null ? tileentity.receiveClientEvent(par5, par6) : false;
	}

	@Override
	public int getPlacedMeta(ItemStack stack, World world, int x, int y, int z,
			ForgeDirection clickedBlock) {
		return TileEntityMetadata.getItemDamage(stack);
	}

	@Override
	public int getDroppedMeta(int blockMeta, int tileMeta) {
		return tileMeta;
	}

	@Override
	public String getBlockName(ItemStack par1ItemStack) {
		int meta = TileEntityMetadata.getItemDamage(par1ItemStack);
		DoorType type = getDoorType(meta);
		return type.name + PlankType.getPlank(meta & 255).getName() + " Wood Door";
	}

	@Override
	public void getBlockTooltip(ItemStack par1ItemStack, List par3List) {
	}

	@Override
	public int getBlockID() {
		return blockID;
	}

	@Override
	public void dropAsStack(World world, int x, int y, int z, ItemStack drop) {
		this.dropBlockAsItem_do(world, x, y, z, drop);
	}

	// Custom

	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List itemList) {
		for (IPlankType type : PlankType.ExtraTreePlanks.values()) {
			itemList.add(PlankType.getDoor(type, DoorType.Standard));
		}
		for (IPlankType type : PlankType.ForestryPlanks.values()) {
			itemList.add(PlankType.getDoor(type, DoorType.Standard));
		}
		for (IPlankType type : PlankType.ExtraBiomesPlank.values()) {
			if(type.getPlank() != null)
				itemList.add(PlankType.getDoor(type, DoorType.Standard));
		}
		for (IPlankType type : PlankType.VanillaPlanks.values()) {
			itemList.add(PlankType.getDoor(type, DoorType.Standard));
		}
	}

	@Override
	public boolean isWood(World world, int x, int y, int z) {
		return true;
	}

	@Override
	public int getFlammability(IBlockAccess world, int x, int y, int z, int metadata,
			ForgeDirection face) {
		return 20;
	}

	@Override
	public boolean isFlammable(IBlockAccess world, int x, int y, int z, int metadata,
			ForgeDirection face) {
		return true;
	}

	@Override
	public int getFireSpreadSpeed(World world, int x, int y, int z, int metadata,
			ForgeDirection face) {
		return 5;
	}
	
	 public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6)
	    {
	        super.breakBlock(par1World, par2, par3, par4, par5, par6);
	        par1World.removeBlockTileEntity(par2, par3, par4);
	    }


}
