package binnie.extratrees.block;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import binnie.core.block.TileEntityMetadata;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;
import binnie.extratrees.api.ICarpentryWood;
import cpw.mods.fml.common.registry.GameRegistry;
import forestry.api.core.BlockInterface;

public interface ILogType {
	
	public enum ExtraTreeLog implements ILogType {
		
		Apple("Apple", 0x7b7a7b, PlankType.ExtraTreePlanks.Apple),
		Fig("Fig", 0x807357, PlankType.ExtraTreePlanks.Fig), 
		Butternut("Butternut", 0xb7ada0, PlankType.ExtraTreePlanks.Butternut), 
		Cherry("Cherry", 0x716850, PlankType.ForestryPlanks.CHERRY),
		Whitebeam("Whitebeam", 0x786a6d, PlankType.ExtraTreePlanks.Whitebeam), 
		Rowan("Rowan", 0xb6b09b, PlankType.ExtraTreePlanks.Rowan),
		Hemlock("Hemlock", 0xada39b, PlankType.ExtraTreePlanks.Hemlock),
		Ash("Ash", 0x898982, PlankType.ExtraTreePlanks.Ash),
		Alder("Alder", 0xc6c0b8, PlankType.ExtraTreePlanks.Alder),
		Beech("Beech", 0xb2917e, PlankType.ExtraTreePlanks.Beech),
		Hawthorn("Hawthorn", 0x5f5745, PlankType.ExtraTreePlanks.Hawthorn), 
		Banana("Banana", 0x85924F), 
		Yew("Yew", 0xd1bbc1, PlankType.ExtraTreePlanks.Yew), 
		Cypress("Cypress", 0x9a8483, PlankType.ExtraTreePlanks.Cypress), 
		Fir("Fir", 0x828382, PlankType.ExtraTreePlanks.Fir), 
		Hazel("Hazel", 0xaa986f, PlankType.ExtraTreePlanks.Hazel), 
		Hickory("Hickory", 0x3e3530, PlankType.ExtraTreePlanks.Hickory), 
		Elm("Elm", 0x848386, PlankType.ExtraTreePlanks.Elm), 
		Elder("Elder", 0xd8b874, PlankType.ExtraTreePlanks.Elder), 
		Holly("Holly", 0xb5aa85, PlankType.ExtraTreePlanks.Holly), 
		Hornbeam("Hornbeam", 0xA39276, PlankType.ExtraTreePlanks.Hornbeam), 
		Cedar("Cedar", 0xad764f, PlankType.ExtraTreePlanks.Cedar), 
		Olive("Olive", 0x7B706A, PlankType.ExtraTreePlanks.Olive), 
		Sweetgum("Sweetgum", 0xA1A19C, PlankType.ExtraTreePlanks.Sweetgum),
		Locust("Locust", 0xADACBC, PlankType.ExtraTreePlanks.Locust),
		Pear("Pear", 0xA89779, PlankType.ExtraTreePlanks.Pear), 
		Maclura("Maclura", 0x8B5734, PlankType.ExtraTreePlanks.Maclura), 
		Brazilwood("Brazilwood", 0x9E8068, PlankType.ExtraTreePlanks.Brazilwood), 
		Logwood("Logwood", 0xF9E2D2, PlankType.ExtraTreePlanks.Logwood), 
		Rosewood("Rosewood", 0x998666, PlankType.ExtraTreePlanks.Rosewood), 
		Purpleheart("Purpleheart", 0x9392A2, PlankType.ExtraTreePlanks.Purpleheart), 
		Iroko("Iroko", 0x605C5B, PlankType.ExtraTreePlanks.Iroko), 
		Gingko("Gingko", 0xADAE9C, PlankType.ExtraTreePlanks.Gingko), 
		Eucalyptus("Eucalyptus", 0xeadeda, PlankType.ExtraTreePlanks.Eucalyptus), 
		Eucalyptus2("Eucalyptus", 0x867e65, PlankType.ExtraTreePlanks.Eucalyptus), 
		Box("Box", 0xAB6F57, PlankType.ExtraTreePlanks.Box), 
		Syzgium("Syzgium", 0xAB6F57, PlankType.ExtraTreePlanks.Syzgium), 
		Eucalyptus3("Eucalyptus", 0x6CB03F, PlankType.ExtraTreePlanks.Eucalyptus), 
		Cinnamon("Cinnamon", 0x86583C, PlankType.VanillaPlanks.JUNGLE),
		PinkIvory("Pink Ivory", 0x7F6554, PlankType.ExtraTreePlanks.PinkIvory)
		
		;
		
		String name;
		int color;
		ICarpentryWood plank = null;
		Icon trunk;
		Icon bark;
		
		ExtraTreeLog(String name, int color) {
			this.name= name;
			this.color = color;
		}
		
		ExtraTreeLog(String name, int color, ICarpentryWood plank) {
			this.name= name;
			this.color = color;
			this.plank = plank;
		}

		public String getName() {
			return name;
		}
		
		public void addRecipe() {
			if(plank==null) return;
			ItemStack log = getItemStack();
			ItemStack result = plank.getPlank();
			result.stackSize = 4;
			GameRegistry.addShapelessRecipe(result, new Object[] {log});
		}
		
		//public int getColor() { return color; }

		@Override
		public void placeBlock(World world, int x, int y, int z) {
			world.setBlock(x, y, z, ExtraTrees.blockLog.blockID, 0, 2);
			if(world.getBlockTileEntity(x, y, z) != null)
				((TileEntityMetadata)world.getBlockTileEntity(x, y, z)).setTileMetadata(this.ordinal());
		}

		public Icon getTrunk() {
			return trunk;
		}
		
		public Icon getBark() {
			return bark;
		}

		public static void registerIcons(IconRegister register) {
			for(ExtraTreeLog log : values()) {
				log.trunk=ExtraTrees.proxy.getIcon(register, "logs/"+log.toString().toLowerCase()+"Trunk");
				log.bark=ExtraTrees.proxy.getIcon(register, "logs/"+log.toString().toLowerCase()+"Bark");
			}
			
		}

		@Override
		public ItemStack getItemStack() {
			return TileEntityMetadata.getItemStack(ExtraTrees.blockLog.blockID, this.ordinal()).copy();
		}

		@Override
		public int getColour() {
			return color;
		};
		
	}
	
	public enum ForestryLog implements ILogType {
		LARCH(1, 0, 0x614c51), 
		TEAK(1, 1, 0x353229), 
		ACACIA(1, 2, 0x737252), 
		LIME(1, 3, 0x716558), 
		CHESTNUT(2, 0, 0x5e5a3c), 
		WENGE(2, 1, 0x62574b), 
		BAOBAB(2, 2, 0xda9a68), 
		SEQUOIA(2, 3, 0xb07355), 
		KAPOK(3, 0, 0x52584f), 
		EBONY(3, 1, 0x9f8051), 
		MAHOGANY(3, 2, 0x8f7c6d), 
		BALSA(3, 3, 0x827b75), 
		WILLOW(4, 0, 0xa19a95),
		WALNUT(4, 1, 0x90927a),
		GREENHEART(4, 2, 0x796652), 
		CHERRY(4, 3, 0x601200),
		MAHOE(5, 0, 0x616248), 
		POPLAR(5, 1, 0x8ca687), 
		PALM(5, 2, 0x886f43), 
		PAPAYA(5, 3, 0x8a6526), 
		PINE(6, 0, 0x735649),
		PLUM(6, 1, 0xb68661), 
		MAPLE(6, 2, 0x8a8781), 
		CITRUS(6, 3, 0x5b4b39);
		
		int block;
		int metadata;
		int colour;

		ForestryLog(int blockOffset, int meta, int colour) {
			block = blockOffset;
			metadata = meta;
			this.colour = colour;
		}
		
		@Override
		public void placeBlock(World world, int x, int y, int z) {
			int blockID = BlockInterface.getBlock("log"+block).getItem().itemID;
			world.setBlock(x, y, z, blockID, metadata, 2);
		}

		@Override
		public ItemStack getItemStack() {
			return new ItemStack(BlockInterface.getBlock("log"+block).getItem().itemID, 1, metadata);
		}

		@Override
		public int getColour() {
			return colour;
		}
		
	}

	public enum VanillaLog implements ILogType {
		Oak(0x614D30), 
		Spruce(0x2A1A0B), 
		Birch(0x614D30), 
		Jungle(0x53411A), 
		;
		int colour;
		private VanillaLog(int colour) {
			this.colour = colour;
		}
		@Override
		public void placeBlock(World world, int x, int y, int z) {
			world.setBlock(x, y, z, Block.wood.blockID, ordinal(), 2);
		}

		@Override
		public ItemStack getItemStack() {
			return new ItemStack(Block.wood, 1, ordinal());
		}

		@Override
		public int getColour() {
			return colour;
		}
		
	}

	void placeBlock(World world, int x, int y, int z);

	ItemStack getItemStack();

	int getColour();
	
	

}
