package binnie.extratrees.block;

import net.minecraft.util.Icon;

public enum DoorType {

	Standard("", "standard"),
	Solid("Solid ", "solid"),
	Double("Split ", "double"),
	Full("Full ", "full"), ;
	String name;
	String iconName;

	private DoorType(String name, String iconName) {
		this.name = name;
		this.iconName = iconName;
	}

	Icon iconDoorLower;
	Icon iconDoorUpper;
	Icon iconDoorLowerFlip;
	Icon iconDoorUpperFlip;
	Icon iconItem;

}
