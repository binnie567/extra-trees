//package binnie.extratrees.block;
//
//import net.minecraft.block.Block;
//import net.minecraft.block.BlockCocoa;
//import net.minecraft.block.BlockDirectional;
//import net.minecraft.client.renderer.RenderBlocks;
//import net.minecraft.client.renderer.Tessellator;
//import net.minecraft.world.IBlockAccess;
//import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
//import cpw.mods.fml.client.registry.RenderingRegistry;
//import forestry.plugins.PluginExtraTrees;
//
//public class FruitPodRenderer implements ISimpleBlockRenderingHandler {
//
//	@Override
//	public void renderInventoryBlock(Block block, int metadata, int modelID,
//			RenderBlocks renderer) {
//	}
//
//	@Override
//	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
//			Block block, int modelId, RenderBlocks renderer) {
//
//		int maturity = 0;
//
//		BlockFruitPod blockPod = (BlockFruitPod) block;
//		TileFruitPod pod = BlockFruitPod.getPodTile(world, x, y, z);
//		if(pod != null) {
//			maturity = pod.getMaturity();
//		}
//
//		Tessellator tess = Tessellator.instance;
//		tess.setBrightness(blockPod.getMixedBrightnessForBlock(world, x, y, z));
//		tess.setColorOpaque_F(1.0f, 1.0f, 1.0f);
//		int metadata = world.getBlockMetadata(x, y, z);
//		int textureIndex = blockPod.getBlockTexture(world, x, y, z, metadata);
//		int notchDirection = BlockDirectional.getDirection(metadata);
//		
//		int var10 = 4 + maturity * 2;
//		int var11 = 5 + maturity * 2;
//		int var12 = (textureIndex & 15) << 4;
//		int var13 = textureIndex & 240;
//		
//		double var14 = 15.0D - (double)var10;
//		double var16 = 15.0D;
//		double var18 = 4.0D;
//		double var20 = 4.0D + (double)var11;
//		double var22 = ((double)var12 + var14) / 256.0D;
//		double var24 = ((double)var12 + var16 - 0.01D) / 256.0D;
//		double var26 = ((double)var13 + var18) / 256.0D;
//		double var28 = ((double)var13 + var20 - 0.01D) / 256.0D;
//		double var30 = 0.0D;
//		double var32 = 0.0D;
//
//		switch (notchDirection) {
//		case 0:
//			var30 = 8.0D - (double)(var10 / 2);
//			var32 = 15.0D - (double)var10;
//			break;
//		case 1:
//			var30 = 1.0D;
//			var32 = 8.0D - (double)(var10 / 2);
//			break;
//		case 2:
//			var30 = 8.0D - (double)(var10 / 2);
//			var32 = 1.0D;
//			break;
//		case 3:
//			var30 = 15.0D - (double)var10;
//			var32 = 8.0D - (double)(var10 / 2);
//		}
//
//		double var34 = (double)x + var30 / 16.0D;
//		double var36 = (double)x + (var30 + (double)var10) / 16.0D;
//		double var38 = (double)y + (12.0D - (double)var11) / 16.0D;
//		double var40 = (double)y + 0.75D;
//		double var42 = (double)z + var32 / 16.0D;
//		double var44 = (double)z + (var32 + (double)var10) / 16.0D;
//		tess.addVertexWithUV(var34, var38, var42, var22, var28);
//		tess.addVertexWithUV(var34, var38, var44, var24, var28);
//		tess.addVertexWithUV(var34, var40, var44, var24, var26);
//		tess.addVertexWithUV(var34, var40, var42, var22, var26);
//		tess.addVertexWithUV(var36, var38, var44, var22, var28);
//		tess.addVertexWithUV(var36, var38, var42, var24, var28);
//		tess.addVertexWithUV(var36, var40, var42, var24, var26);
//		tess.addVertexWithUV(var36, var40, var44, var22, var26);
//		tess.addVertexWithUV(var36, var38, var42, var22, var28);
//		tess.addVertexWithUV(var34, var38, var42, var24, var28);
//		tess.addVertexWithUV(var34, var40, var42, var24, var26);
//		tess.addVertexWithUV(var36, var40, var42, var22, var26);
//		tess.addVertexWithUV(var34, var38, var44, var22, var28);
//		tess.addVertexWithUV(var36, var38, var44, var24, var28);
//		tess.addVertexWithUV(var36, var40, var44, var24, var26);
//		tess.addVertexWithUV(var34, var40, var44, var22, var26);
//		int var46 = var10;
//
//		if (maturity >= 2)
//			var46 = var10 - 1;
//
//		var22 = (double)((float)(var12 + 0) / 256.0F);
//		var24 = ((double)(var12 + var46) - 0.01D) / 256.0D;
//		var26 = (double)((float)(var13 + 0) / 256.0F);
//		var28 = ((double)(var13 + var46) - 0.01D) / 256.0D;
//		tess.addVertexWithUV(var34, var40, var44, var22, var28);
//		tess.addVertexWithUV(var36, var40, var44, var24, var28);
//		tess.addVertexWithUV(var36, var40, var42, var24, var26);
//		tess.addVertexWithUV(var34, var40, var42, var22, var26);
//		tess.addVertexWithUV(var34, var38, var42, var22, var26);
//		tess.addVertexWithUV(var36, var38, var42, var24, var26);
//		tess.addVertexWithUV(var36, var38, var44, var24, var28);
//		tess.addVertexWithUV(var34, var38, var44, var22, var28);
//		var22 = (double)((float)(var12 + 12) / 256.0F);
//		var24 = ((double)(var12 + 16) - 0.01D) / 256.0D;
//		var26 = (double)((float)(var13 + 0) / 256.0F);
//		var28 = ((double)(var13 + 4) - 0.01D) / 256.0D;
//		var30 = 8.0D;
//		var32 = 0.0D;
//		double var47;
//
//		switch (notchDirection) {
//		
//		case 0:
//			var30 = 8.0D;
//			var32 = 12.0D;
//			var47 = var22;
//			var22 = var24;
//			var24 = var47;
//			break;
//		case 1:
//			var30 = 0.0D;
//			var32 = 8.0D;
//			break;
//		case 2:
//			var30 = 8.0D;
//			var32 = 0.0D;
//			break;
//		case 3:
//			var30 = 12.0D;
//			var32 = 8.0D;
//			var47 = var22;
//			var22 = var24;
//			var24 = var47;
//		}
//
//		var34 = (double)x + var30 / 16.0D;
//		var36 = (double)x + (var30 + 4.0D) / 16.0D;
//		var38 = (double)y + 0.75D;
//		var40 = (double)y + 1.0D;
//		var42 = (double)z + var32 / 16.0D;
//		var44 = (double)z + (var32 + 4.0D) / 16.0D;
//
//		if (notchDirection != 2 && notchDirection != 0) {
//			
//			if (notchDirection == 1 || notchDirection == 3) {
//				
//				tess.addVertexWithUV(var36, var38, var42, var22, var28);
//				tess.addVertexWithUV(var34, var38, var42, var24, var28);
//				tess.addVertexWithUV(var34, var40, var42, var24, var26);
//				tess.addVertexWithUV(var36, var40, var42, var22, var26);
//				tess.addVertexWithUV(var34, var38, var42, var24, var28);
//				tess.addVertexWithUV(var36, var38, var42, var22, var28);
//				tess.addVertexWithUV(var36, var40, var42, var22, var26);
//				tess.addVertexWithUV(var34, var40, var42, var24, var26);
//			}
//			
//		} else {
//			tess.addVertexWithUV(var34, var38, var42, var24, var28);
//			tess.addVertexWithUV(var34, var38, var44, var22, var28);
//			tess.addVertexWithUV(var34, var40, var44, var22, var26);
//			tess.addVertexWithUV(var34, var40, var42, var24, var26);
//			tess.addVertexWithUV(var34, var38, var44, var22, var28);
//			tess.addVertexWithUV(var34, var38, var42, var24, var28);
//			tess.addVertexWithUV(var34, var40, var42, var24, var26);
//			tess.addVertexWithUV(var34, var40, var44, var22, var26);
//		}
//
//		return true;
//	}
//
//	@Override
//	public boolean shouldRender3DInInventory() {
//		return false;
//	}
//
//	@Override
//	public int getRenderId() {
//		return PluginExtraTrees.fruitPodRenderId;
//	}
//
//}
