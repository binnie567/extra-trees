package binnie.extratrees.block;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.oredict.OreDictionary;
import binnie.core.BinnieCore;
import binnie.core.block.ItemMetadata;
import binnie.core.block.TileEntityMetadata;
import binnie.core.liquid.ILiquidType;
import binnie.core.plugin.IBinnieModule;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;
import binnie.extratrees.block.ILogType.ExtraTreeLog;
import binnie.extratrees.block.ILogType.ForestryLog;
import binnie.extratrees.block.ILogType.VanillaLog;
import binnie.extratrees.config.ConfigurationMain;
import binnie.extratrees.item.ExtraTreeLiquid;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.registry.GameRegistry;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.RecipeManagers;

public class ModuleBlocks implements IBinnieModule {

	@Override
	public void preInit() {
		PlankType.setup();

		ExtraTrees.blockPlanks = new BlockETPlanks(ConfigurationMain.planksID);
		ExtraTrees.blockFence = new BlockFence(ConfigurationMain.fenceID);
		ExtraTrees.blockLog = new BlockETLog(ConfigurationMain.logID);
		ExtraTrees.blockGate = new BlockGate(ConfigurationMain.gateID);
		ExtraTrees.blockDoor = new BlockETDoor(ConfigurationMain.doorID);

		ExtraTrees.blockSlab = new BlockETSlab(ConfigurationMain.slabID, false);
		ExtraTrees.blockDoubleSlab = new BlockETSlab(ConfigurationMain.doubleSlabID, true);

		ExtraTrees.blockStairs = new BlockETStairs(ConfigurationMain.stairsID,
				ExtraTrees.blockPlanks);

		GameRegistry.registerBlock(ExtraTrees.blockPlanks, ItemMetadata.class,
				"extratrees.block.planks");
		GameRegistry.registerBlock(ExtraTrees.blockFence, ItemMetadata.class,
				"extratrees.block.fence");
		GameRegistry.registerBlock(ExtraTrees.blockLog, ItemMetadata.class,
				"extratrees.block.log");
		GameRegistry.registerBlock(ExtraTrees.blockGate, ItemMetadata.class,
				"extratrees.block.gate");
		GameRegistry.registerBlock(ExtraTrees.blockSlab, ItemETSlab.class,
				"extratrees.block.slab");
		GameRegistry.registerBlock(ExtraTrees.blockDoubleSlab, ItemETDoubleSlab.class,
				"extratrees.block.doubleSlab");
		GameRegistry.registerBlock(ExtraTrees.blockDoor, ItemETDoor.class,
				"extratrees.block.door");

		GameRegistry.registerBlock(ExtraTrees.blockStairs, ItemETStairs.class,
				"extratrees.block.stairs");

		for (int i = 0; i < PlankType.ExtraTreePlanks.values().length; i++)
			FMLInterModComms.sendMessage("BuildCraft|Transport", "add-facade",
					ExtraTrees.blockPlanks.blockID + "@" + i);

		BinnieCore.proxy.registerCustomItemRenderer(
				Item.itemsList[ExtraTrees.blockStairs.blockID].itemID,
				new StairItemRenderer());

		BinnieCore.proxy.registerCustomItemRenderer(
				Item.itemsList[ExtraTrees.blockGate.blockID].itemID, new GateItemRenderer());

		// RenderingRegistry.registerBlockHandler(new FruitPodRenderer());

		Block.useNeighborBrightness[ConfigurationMain.stairsID] = true;

		for(ILogType plank : ILogType.ExtraTreeLog.values())
			OreDictionary.registerOre("logWood", plank.getItemStack());
		
		FurnaceRecipes.smelting().addSmelting(ExtraTrees.blockLog.blockID,
				new ItemStack(Item.coal, 1, 1), 0.15F);
		
		for(IPlankType plank : PlankType.ExtraTreePlanks.values())
			OreDictionary.registerOre("plankWood", plank.getPlank());

		FMLInterModComms.sendMessage("Forestry", "add-fence-block",
				ExtraTrees.blockFence.blockID + "");

		FMLInterModComms.sendMessage("Forestry", "add-fence-block",
				ExtraTrees.blockGate.blockID + "");
	}

	@Override
	public void doInit() {
		PluginExtraTrees.fenceID = RenderingRegistry.getNextAvailableRenderId();
		RenderingRegistry.registerBlockHandler(new FenceRenderer());

		PluginExtraTrees.stairsID = RenderingRegistry.getNextAvailableRenderId();
		RenderingRegistry.registerBlockHandler(new StairsRenderer());

		PluginExtraTrees.doorRenderId = RenderingRegistry.getNextAvailableRenderId();
		RenderingRegistry.registerBlockHandler(new DoorBlockRenderer());

	}

	@Override
	public void postInit() {
	
		for (PlankType.ExtraTreePlanks plank : PlankType.ExtraTreePlanks.values()) {

			ItemStack planks = plank.getPlank();
			ItemStack slabs = TileEntityMetadata.getItemStack(ExtraTrees.blockSlab.blockID,
					plank.ordinal());
			ItemStack stairs = TileEntityMetadata.getItemStack(
					ExtraTrees.blockStairs.blockID, plank.ordinal());

			stairs.stackSize = 4;
			GameRegistry.addRecipe(stairs.copy(),
					new Object[] { "#  ", "## ", "###", '#', planks.copy() });

			slabs.stackSize = 6;
			GameRegistry.addRecipe(slabs.copy(),
					new Object[] { "   ", "   ", "###", '#', planks.copy() });

		}
		
		for(IPlankType plank : PlankType.getAllPlankTypes()) {
			ItemStack planks = plank.getPlank();
			ItemStack fence = PlankType.getFence(plank);
			ItemStack gate = PlankType.getGate(plank);
			ItemStack doorStandard = PlankType.getDoor(plank, DoorType.Standard);
			ItemStack doorSolid = PlankType.getDoor(plank, DoorType.Solid);
			ItemStack doorSplit = PlankType.getDoor(plank, DoorType.Double);
			ItemStack doorFull = PlankType.getDoor(plank, DoorType.Full);
			
			if(planks == null || fence == null || gate == null) continue;
			
			gate.stackSize = 2;
			GameRegistry.addRecipe(gate.copy(), new Object[] { "#p#", '#', fence.copy(), 'p',
					planks.copy() });

			fence.stackSize = 4;
			GameRegistry.addRecipe(fence.copy(), new Object[] { "###", "# #", '#', planks.copy() });

			GameRegistry.addRecipe(doorSolid.copy(),
					new Object[] { "###", "###", "###", '#', planks.copy() });
			
			GameRegistry.addRecipe(doorStandard.copy(),
					new Object[] { "# #", "###", "###", '#', planks.copy() });
			
			GameRegistry.addRecipe(doorSplit.copy(),
					new Object[] { "# #", "###", "# #", '#', planks.copy() });
			
			GameRegistry.addRecipe(doorFull.copy(),
					new Object[] { "# #", "# #", "# #", '#', planks.copy() });
			
		}

		MinecraftForge.setBlockHarvestLevel(ExtraTrees.blockCarpentry, "axe", 0);
		MinecraftForge.setBlockHarvestLevel(ExtraTrees.blockFence, "axe", 0);
		MinecraftForge.setBlockHarvestLevel(ExtraTrees.blockPlanks, "axe", 0);
		MinecraftForge.setBlockHarvestLevel(ExtraTrees.blockLog, "axe", 0);
		MinecraftForge.setBlockHarvestLevel(ExtraTrees.blockStairs, "axe", 0);

		addSqueezer(VanillaLog.Oak, ExtraTreeLiquid.Sap, 50);
		addSqueezer(VanillaLog.Spruce, ExtraTreeLiquid.Resin, 50);
		addSqueezer(VanillaLog.Birch, ExtraTreeLiquid.Sap, 50);
		addSqueezer(VanillaLog.Jungle, ExtraTreeLiquid.Sap, 50);

		addSqueezer(ForestryLog.LARCH, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ForestryLog.TEAK, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.ACACIA, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.LIME, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.CHESTNUT, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.WENGE, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.BAOBAB, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.SEQUOIA, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.KAPOK, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.EBONY, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.MAHOGANY, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.BALSA, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.WILLOW, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.WALNUT, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.GREENHEART, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.CHERRY, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.MAHOE, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.POPLAR, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ForestryLog.PALM, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.PAPAYA, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.PINE, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ForestryLog.PLUM, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.MAPLE, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ForestryLog.CITRUS, ExtraTreeLiquid.Sap, 50);

		addSqueezer(ExtraTreeLog.Apple, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Butternut, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Cherry, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Rowan, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Hemlock, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ExtraTreeLog.Ash, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Alder, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Beech, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Hawthorn, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Banana, ExtraTreeLiquid.Sap, 25);
		addSqueezer(ExtraTreeLog.Yew, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ExtraTreeLog.Cypress, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ExtraTreeLog.Fir, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ExtraTreeLog.Hazel, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Hickory, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Elm, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Elder, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Holly, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Hornbeam, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Cedar, ExtraTreeLiquid.Resin, 50);
		addSqueezer(ExtraTreeLog.Olive, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Sweetgum, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Locust, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Pear, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Maclura, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Brazilwood, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Logwood, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Rosewood, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Purpleheart, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Iroko, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Gingko, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Eucalyptus, ExtraTreeLiquid.Sap, 50, 1f);
		addSqueezer(ExtraTreeLog.Eucalyptus2, ExtraTreeLiquid.Sap, 50, 1f);
		addSqueezer(ExtraTreeLog.Box, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Syzgium, ExtraTreeLiquid.Sap, 50);
		addSqueezer(ExtraTreeLog.Eucalyptus3, ExtraTreeLiquid.Sap, 50, 1f);
		addSqueezer(ExtraTreeLog.Cinnamon, ExtraTreeLiquid.Sap, 50, 1f);

	}

	public void addSqueezer(ILogType log, ILiquidType liquid, int amount, float pulpChance) {
		LiquidStack liquidStack = liquid.get(amount);
		RecipeManagers.squeezerManager.addRecipe(10, new ItemStack[] { log.getItemStack() },
				liquidStack, ItemInterface.getItem("woodPulp"), (int) (100f * pulpChance));
	}

	public void addSqueezer(ILogType log, ILiquidType liquid, int amount) {
		addSqueezer(log, liquid, amount, 0.5f);
	}

}
