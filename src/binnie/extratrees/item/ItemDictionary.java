package binnie.extratrees.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.core.ExtraTreesGUID;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemDictionary extends Item {

//	@Override
//	@SideOnly(Side.CLIENT)
//	public int getIconFromDamage(int par1) {
//		return par1 == 0 ? 0 : 2;
//	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		this.itemIcon = ExtraTrees.proxy.getIcon(register, "arboristDatabase");
		this.iconMaster = ExtraTrees.proxy.getIcon(register, "masterArboristDatabase");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1)
	{
		return par1 == 0 ? itemIcon : iconMaster;
	}

	Icon iconMaster;

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack par1ItemStack,
			EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		super.addInformation(par1ItemStack, par2EntityPlayer, par3List, par4);
		if (par1ItemStack.getItemDamage() > 0)
			par3List.add("Sengir-in-a-can");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List par3List) {
		super.getSubItems(par1, par2CreativeTabs, par3List);
		par3List.add(new ItemStack(par1, 1, 1));
	}
	
	public ItemDictionary(int par1) {
		super(par1);
		//setTextureFile(ExtraTreeTexture.Main.getTexture());
		setCreativeTab(CreativeTabs.tabTools);
		setUnlocalizedName("database");
		setMaxStackSize(1);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world,
			EntityPlayer player) {
		if(itemstack.getItemDamage() == 0)
			ExtraTrees.proxy.openGui(ExtraTreesGUID.Database, player,
					(int) player.posX, (int) player.posY, (int) player.posZ);
		else
			ExtraTrees.proxy.openGui(ExtraTreesGUID.DatabaseNEI, player,
					(int) player.posX, (int) player.posY, (int) player.posZ);
		return itemstack;
	}

	@Override
	public String getItemDisplayName(ItemStack i) {
		return i.getItemDamage() == 0 ? "Arborist Database"
				: "Master Arborist Database";
	}
}