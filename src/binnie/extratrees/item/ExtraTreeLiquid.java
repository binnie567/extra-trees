package binnie.extratrees.item;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.liquid.ILiquidType;
import binnie.core.liquid.ItemLiquid;
import binnie.core.liquid.ItemLiquidContainer;
import binnie.core.liquid.LiquidContainer;
import binnie.core.liquid.LiquidManager;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;


public enum ExtraTreeLiquid implements ILiquidType {
	
	Sap("Sap", "sap", 0xBE7542), 
	Resin("Resin", "resin", 0xC96800), 
	Latex("Latex", "latex", 0xD8DBC9),
	Turpentine("Turpentine", "turpentine", 0x79533A);

	public String name = "";

	String ident;
	Icon icon;
	int colour;

	private ExtraTreeLiquid(String name, String ident, int colour) {
		this.name = name;
		this.ident = ident;
		this.colour = colour;
	};

	@Override
	public boolean canBePlacedInContainer(LiquidContainer container) {
		return true;
	}

	@Override
	public Icon getIcon() {
		return icon;
	}

	@Override
	public void registerIcon(IconRegister register) {
		icon = ExtraTrees.proxy.getIcon(register, "liquids/" + getIdentifier());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setContainerItem(ItemLiquidContainer itemContainer) {
		
	}

	@Override
	public String getIdentifier() {
		return ident;
	}

	@Override
	public int getColour() {
		return colour;
	}

	public LiquidStack get(int i) {
		return LiquidManager.getLiquidStack(ident, i);
	}

	@Override
	public void setItem(ItemLiquid itemLiquid) {
		
	}

}
