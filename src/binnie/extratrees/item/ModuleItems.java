package binnie.extratrees.item;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import binnie.core.Constants;
import binnie.core.item.ItemManager;
import binnie.core.liquid.LiquidManager;
import binnie.core.plugin.IBinnieModule;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.block.ILogType;
import binnie.extratrees.config.ConfigurationMain;
import forestry.api.core.ItemInterface;
import forestry.api.core.Tabs;
import forestry.api.fuels.EngineBronzeFuel;
import forestry.api.fuels.FuelManager;
import forestry.api.recipes.RecipeManagers;

public class ModuleItems implements IBinnieModule {

	@Override
	public void preInit() {
		
		ExtraTrees.itemMisc = ItemManager.registerMiscItems(ExtraTreeItems.values(),
				ConfigurationMain.itemID, Tabs.tabArboriculture);
		
		ExtraTrees.itemDictionary = new ItemDictionary(ConfigurationMain.dictionaryID);
		
		new ItemMothDatabase(ConfigurationMain.mothDictionaryID);

		LiquidManager.createLiquids(ExtraTreeLiquid.values(), ConfigurationMain.liquidID, ConfigurationMain.containerID);

		ExtraTrees.itemFood = new ItemFood(ConfigurationMain.foodID);
		
		OreDictionary.registerOre("pulpWood", ExtraTreeItems.Sawdust.get(1));
		
		ExtraTrees.itemHammer = new ItemHammer(ConfigurationMain.hammerID, false);
		ExtraTrees.itemDurableHammer = new ItemHammer(ConfigurationMain.durableHammerID, true);
		
		Food.registerOreDictionary();
	}

	@Override
	public void doInit() {

	}

	@Override
	public void postInit() {
		
		Food.Crabapple.addJuice(10, 150, 10);
		Food.Orange.addJuice(10, 400, 15);
		Food.Kumquat.addJuice(10, 300, 10);
		Food.Lime.addJuice(10, 300, 10);
		Food.WildCherry.addOil(20, 50, 5);
		Food.SourCherry.addOil(20, 50, 3);
		Food.BlackCherry.addOil(20, 50, 5);
		Food.Blackthorn.addJuice(10, 50, 5);
		Food.CherryPlum.addJuice(10, 100, 60);
		Food.Almond.addOil(20, 80, 5);
		Food.Apricot.addJuice(10, 150, 40);
		Food.Grapefruit.addJuice(10, 500, 15);
		Food.Peach.addJuice(10, 150, 40);
		
		Food.Satsuma.addJuice(10, 300, 10);
		Food.BuddhaHand.addJuice(10, 400, 15);
		
		Food.Citron.addJuice(10, 400, 15);
		Food.FingerLime.addJuice(10, 300, 10);
		Food.KeyLime.addJuice(10, 300, 10);
		
		Food.Manderin.addJuice(10, 400, 10);
		Food.Nectarine.addJuice(10, 150, 40);
		
		Food.Pomelo.addJuice(10, 300, 10);
		Food.Tangerine.addJuice(10, 300, 10);
		
		Food.Pear.addJuice(10, 300, 20);
		Food.SandPear.addJuice(10, 200, 10);
		
		Food.Hazelnut.addOil(50, 150, 5);
		Food.Butternut.addOil(50, 180, 5);
		Food.Beechnut.addOil(30, 100, 4);
		Food.Pecan.addOil(25, 50, 2);
		
		Food.Banana.addJuice(10, 100, 30);
		Food.RedBanana.addJuice(10, 100, 30);
		Food.Plantain.addJuice(10, 100, 40);
		
		Food.BrazilNut.addOil(15, 20, 2);
		
		Food.Fig.addOil(20, 50, 3);
		Food.Acorn.addOil(20, 50, 3);
		Food.Elderberry.addJuice(10, 100, 2);
		Food.Olive.addOil(20, 50, 3);
		Food.GingkoNut.addOil(40, 120, 5);
		Food.Coffee.addOil(15, 20, 2);
		Food.OsangeOrange.addJuice(10, 300, 15);
		Food.Clove.addOil(10, 25, 2);
		
		
		
		
		CraftingManager
				.getInstance()
				.getRecipeList()
				.add(new ShapedOreRecipe(new ItemStack(ExtraTrees.itemDurableHammer, 1, 0), new Object[] {
						"wiw", " s ", " s ", 'w', Block.obsidian, 'i',
						Item.ingotGold, 's', Item.stick }));
		
		CraftingManager
		.getInstance()
		.getRecipeList()
		.add(new ShapedOreRecipe(new ItemStack(ExtraTrees.itemHammer, 1, 0), new Object[] {
				"wiw", " s ", " s ", 'w', "plankWood", 'i',
				Item.ingotIron, 's', Item.stick }));

		CraftingManager
				.getInstance()
				.getRecipeList()
				.add(new ShapedOreRecipe(ExtraTreeItems.ProvenGear.get(1), new Object[] { " s ",
						"s s", " s ", 's',
						ItemInterface.getItem("stickImpregnated") }));

		RecipeManagers.carpenterManager.addRecipe(
				100,
				LiquidManager.getLiquidStack(Constants.LiquidWater, 2000),
				null,
				new ItemStack(ExtraTrees.itemDictionary),
				new Object[] { "X#X", "YEY", "RDR", Character.valueOf('#'),
						Block.thinGlass, Character.valueOf('X'),
						Item.ingotGold, Character.valueOf('Y'), "ingotCopper",
						Character.valueOf('R'), Item.redstone,
						Character.valueOf('D'), Item.diamond,
						Character.valueOf('E'), Item.emerald });
		
		RecipeManagers.stillManager.addRecipe(25, ExtraTreeLiquid.Resin.get(5), ExtraTreeLiquid.Turpentine.get(3));

		RecipeManagers.carpenterManager.addRecipe(25,
				ExtraTreeLiquid.Turpentine.get(50), null,
				ExtraTrees.itemMisc.getStack(ExtraTreeItems.WoodWax, 4),
				new Object[] { "x", 'x', ItemInterface.getItem("beeswax") });
		
		if(LiquidManager.getLiquidStack(Constants.LiquidCreosote, 100) != null)
		RecipeManagers.carpenterManager.addRecipe(25,
				LiquidManager.getLiquidStack(Constants.LiquidCreosote, 50), null,
				ExtraTrees.itemMisc.getStack(ExtraTreeItems.WoodWax, 1),
				new Object[] { "x", 'x', ItemInterface.getItem("beeswax") });
		
		RecipeManagers.stillManager.addRecipe(40, ExtraTreeLiquid.Resin.get(5), ExtraTreeLiquid.Turpentine.get(3));

		
		for(LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData()) {
			if(data.stillLiquid.isLiquidEqual(LiquidManager.getLiquidStack(Constants.LiquidWater, 0)) && data.stillLiquid.amount == 1000) {
				CraftingManager.getInstance()
				.addRecipe(ItemInterface.getItem("mulch"), new Object[] { " b ",
						"bwb", " b ", 'b',
						ExtraTreeItems.Bark.get(1), 'w', data.filled.copy() });
			}
		}
		
		
		FuelManager.bronzeEngineFuel.put(ExtraTreeLiquid.Sap.get(1).asItemStack(), new EngineBronzeFuel(ExtraTreeLiquid.Sap.get(1).asItemStack(),
				2, 10000, 1));
		
		FuelManager.bronzeEngineFuel.put(ExtraTreeLiquid.Resin.get(1).asItemStack(), new EngineBronzeFuel(ExtraTreeLiquid.Resin.get(1).asItemStack(),
				3, 10000, 1));
		
		
		for (ILogType.ExtraTreeLog log : ILogType.ExtraTreeLog.values())
			log.addRecipe();
	}

}
