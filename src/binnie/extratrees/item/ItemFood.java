package binnie.extratrees.item;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import binnie.core.item.ItemMisc;
import forestry.api.core.Tabs;

public class ItemFood extends ItemMisc {

	public ItemFood(int id) {
		super(id, Tabs.tabArboriculture, Food.values());
		setUnlocalizedName("food");
	}
	
	public EnumAction getItemUseAction(ItemStack par1ItemStack) {
		return EnumAction.eat;
	}

	public int getMaxItemUseDuration(ItemStack par1ItemStack) {
		return 32;
	}

	@Override
	public ItemStack onEaten(ItemStack par1ItemStack, World par2World,
			EntityPlayer par3EntityPlayer) {
		--par1ItemStack.stackSize;
		par3EntityPlayer.getFoodStats().addStats(
				getFood(par1ItemStack).getHealth(), 3);
		par2World.playSoundAtEntity(par3EntityPlayer, "random.burp", 0.5F,
				par2World.rand.nextFloat() * 0.1F + 0.9F);
		return par1ItemStack;
	}

	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World,
			EntityPlayer par3EntityPlayer) {
		if (par3EntityPlayer.canEat(false) && getFood(par1ItemStack).getHealth() > 0) {
			par3EntityPlayer.setItemInUse(par1ItemStack,
					this.getMaxItemUseDuration(par1ItemStack));
		}

		return par1ItemStack;
	}

	private Food getFood(ItemStack par1ItemStack) {
		return Food.values()[par1ItemStack.getItemDamage()];
	}

}
