package binnie.extratrees.item;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.api.IToolHammer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemHammer extends Item implements IToolHammer {

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		this.itemIcon = ExtraTrees.proxy.getIcon(register, isDurableHammer ? "durableHammer" : "carpentryHammer");
	}

	boolean isDurableHammer = false;
	
	public ItemHammer(int par1, boolean durable) {
		super(par1);
		isDurableHammer = durable;
		setCreativeTab(CreativeTabs.tabTools);
		setUnlocalizedName(durable ? "durableHammer" : "hammer");
		setMaxStackSize(1);
		setMaxDamage(durable ? 1562 : 251);
	}
	
	@Override
	public String getItemDisplayName(ItemStack i) {
		return isDurableHammer ? "Master Carpentry Hammer" : "Carpentry Hammer";
	}

	@Override
	public boolean isActive(ItemStack item) {
		return true;
	}

	@Override
	public void onHammerUsed(ItemStack item, EntityPlayer player) {
		item.damageItem(1, player);
	}
}