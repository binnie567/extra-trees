package binnie.extratrees.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import binnie.core.item.IItemMisc;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;

public enum ExtraTreeItems implements IItemMisc {
	
	@Deprecated
	CarpentryHammer("Fake Hammer", "carpentryHammer"),
	Sawdust("Sawdust", "sawdust"),
	Bark("Bark", "bark"),
	ProvenGear("Proven Gear", "provenGear"),
	WoodWax("Wood Polish", "woodWax"),
	;
	
	String name;
	String iconPath;
	Icon icon;
	private ExtraTreeItems(String name, String iconPath) {
		this.name = name;
		this.iconPath = iconPath;
	}
	
	@Override
	public Icon getIcon(ItemStack stack) {
		return icon;
	}
	@Override
	public void registerIcons(IconRegister register) {
		icon = ExtraTrees.proxy.getIcon(register, iconPath);
	}
	@Override
	public void addInformation(List par3List) {
	}
	@Override
	public String getName(ItemStack stack) {
		return name;
	}

	@Override
	public boolean isActive() {
		return this != CarpentryHammer;
	}

	@Override
	public ItemStack get(int i) {
		return new ItemStack(ExtraTrees.itemMisc, 1, ordinal());
	}

}
