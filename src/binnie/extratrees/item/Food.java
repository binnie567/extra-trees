package binnie.extratrees.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraftforge.oredict.OreDictionary;
import binnie.core.Constants;
import binnie.core.item.IItemMisc;
import binnie.core.liquid.LiquidManager;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.RecipeManagers;

public enum Food implements IItemMisc {

	Crabapple("Crabapple", 2), // Added
	Orange("Orange", 4), // Citrus - Added //
	Kumquat("Kumquat", 2), // Citrus - Added //
	Lime("Lime", 2), // Citrus - Added //
	WildCherry("Wild Cherry", 2), // Prune - Added //
	SourCherry("Sour Cherry", 2), // Prune - Added //
	BlackCherry("Black Cherry", 2), // Prune - Added //
	Blackthorn("Blackthorn", 3), // Prune - Added //
	CherryPlum("Cherry Plum", 3), // Prune - Added //
	Almond("Almond", 1), // Prune - Added //
	Apricot("Apricot", 4), // Prune - Added //
	Grapefruit("Grapefruit", 4), // Citrus - Added //
	Peach("Peach", 4), // Prune - Added //

	Satsuma("Satsuma", 3), // Citrus - Added //
	BuddhaHand("Buddhas Hand", 3), // Citrus - Added //

	Citron("Citron", 3), // Citrus - Added //
	FingerLime("Finger Lime", 3), // Citrus - Added //
	KeyLime("Key Lime", 2), // Citrus - Added //

	Manderin("Manderin", 3), // Citrus - Added //
	Nectarine("Nectarine", 3), // Prune - Added //

	Pomelo("Pomelo", 3), // Citrus - Added //
	Tangerine("Tangerine", 3), // Citrus - Added //

	Pear("Pear", 4), //
	SandPear("Sand Pear", 2), //

	Hazelnut("Hazelnut", 2), //
	Butternut("Butternut", 1), //
	Beechnut("Beechnut", 0), //
	Pecan("Pecan Nut", 0), //

	Banana("Banana", 4), //
	RedBanana("Red Banana", 4), //
	Plantain("Plantain", 2), //

	BrazilNut("Brazil Nut", 0),

	Fig("Fig", 2),
	Acorn("Acorn", 0),
	Elderberry("Elderberry", 1), //
	Olive("Olive", 1),
	GingkoNut("Gingko Nut", 1),
	Coffee("Coffee", 0),
	OsangeOrange("Osange Orange", 1), //
	Clove("Clove", 0),
	
	Papayimar("Papayimar", 8),

	;

	String name;

	Icon icon;

	int hunger;

	Food(String name) {
		this(name, 0);
	}

	Food(String name, int hunger) {
		this.name = name;
		this.hunger = hunger;
	}

	public boolean isEdible() {
		return hunger > 0;
	}

	public int getHealth() {
		return hunger;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public String getName(ItemStack stack) {
		return name;
	}

	@Override
	public ItemStack get(int i) {
		return new ItemStack(ExtraTrees.itemFood, i, ordinal());
	}

	@Override
	public Icon getIcon(ItemStack stack) {
		return icon;
	}

	@Override
	public void registerIcons(IconRegister register) {
		icon = ExtraTrees.proxy.getIcon(register, "food/" + toString());
	}

	@Override
	public void addInformation(List par3List) {

	}

	public void addJuice(int time, int amount, int mulch) {

		RecipeManagers.squeezerManager.addRecipe(time, new ItemStack[] { get(1) },
				LiquidManager.getLiquidStack(Constants.LiquidJuice, amount),
				ItemInterface.getItem("mulch"), mulch);

	}

	public void addOil(int time, int amount, int mulch) {

		RecipeManagers.squeezerManager.addRecipe(time, new ItemStack[] { get(1) },
				LiquidManager.getLiquidStack(Constants.LiquidSeedOil, amount),
				ItemInterface.getItem("mulch"), mulch);

	}

	public static void registerOreDictionary() {

		Crabapple.ore("Apple").ore("Crabapple");
		Orange.ore("Orange");
		Kumquat.ore("Kumquat");
		Lime.ore("Lime");
		WildCherry.ore("Cherry").ore("WildCherry");
		SourCherry.ore("Cherry").ore("SourCherry");
		BlackCherry.ore("Cherry").ore("BlackCherry");
		Blackthorn.ore("Blackthorn");
		CherryPlum.ore("Plum").ore("CherryPlum");
		Almond.ore("Almond");
		Apricot.ore("Apricot");
		Grapefruit.ore("Grapefruit");
		Peach.ore("Peach");
		Satsuma.ore("Satsuma").ore("Orange");
		BuddhaHand.ore("BuddhaHand").ore("Citron");
		Citron.ore("Citron");
		FingerLime.ore("Lime").ore("FingerLime");
		KeyLime.ore("KeyLime").ore("Lime");
		Manderin.ore("Orange").ore("Manderin");
		Nectarine.ore("Peach").ore("Nectarine");
		Pomelo.ore("Pomelo");
		Tangerine.ore("Tangerine").ore("Orange");
		Pear.ore("Pear");
		SandPear.ore("SandPear");

		Hazelnut.ore("Hazelnut");
		Butternut.ore("Butternut").ore("Walnut");
		Beechnut.ore("Beechnut");
		Pecan.ore("Pecan");

		Banana.ore("Banana");
		RedBanana.ore("RedBanana").ore("Banana");
		;
		Plantain.ore("Plantain");

		BrazilNut.ore("BrazilNut");

		Fig.ore("Fig");
		Acorn.ore("Acorn");
		Elderberry.ore("Elderberry");
		Olive.ore("Olive");
		GingkoNut.ore("GingkoNut");
		Coffee.ore("Coffee");
		OsangeOrange.ore("OsangeOrange");
		Clove.ore("Clove");
	}

	private Food ore(String string) {
		OreDictionary.registerOre("crop" + string, get(1));
		return this;
	}

}
