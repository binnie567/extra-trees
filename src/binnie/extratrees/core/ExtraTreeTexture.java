package binnie.extratrees.core;

import binnie.core.resource.BinnieResource;
import binnie.core.resource.IBinnieTexture;
import binnie.core.resource.ResourceManager;
import binnie.core.resource.ResourceType;
import binnie.extratrees.ExtraTrees;

public enum ExtraTreeTexture implements IBinnieTexture {

	Gui(ResourceType.GUI, "gui"), 
	Nursery(ResourceType.Tile, "Nursery"), 
	;

	String texture;
	ResourceType type;
	ExtraTreeTexture(ResourceType base, String texture) {
		this.texture = texture;
		this.type = base;
	}

	public BinnieResource getTexture() {
		return ResourceManager.getPNG(ExtraTrees.instance, type, texture);
	}
	
	public static String carpenterTexture = "carpenter_";
	public static String panelerTexture = "paneler_";
	public static String incubatorTexture = "incubator_";
	public static String lumbermillTexture = "sawmill_";

}
