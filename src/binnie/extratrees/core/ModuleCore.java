package binnie.extratrees.core;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Queue;

import binnie.core.BinnieCore;
import binnie.core.plugin.IBinnieModule;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.api.ICarpentryWood;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.block.ILogType;
import binnie.extratrees.block.ILogType.ExtraTreeLog;
import binnie.extratrees.block.PlankType;
import binnie.extratrees.carpentry.EnumPattern;
import binnie.extratrees.genetics.ExtraTreeFruitGene;
import binnie.extratrees.genetics.ExtraTreeSpecies;
import forestry.api.arboriculture.IAlleleTreeSpecies;

public class ModuleCore implements IBinnieModule {

	@Override
	public void preInit() {

	}

	@Override
	public void doInit() {

	}

	@Override
	public void postInit() {
		if (BinnieCore.proxy.isDebug()) {
			PrintWriter out;
			try {
				PrintWriter outputSpecies = new PrintWriter(new FileWriter("data/species.html"));
				PrintWriter outputLogs = new PrintWriter(new FileWriter("data/logs.html"));
				PrintWriter outputPlanks = new PrintWriter(new FileWriter("data/planks.html"));
				PrintWriter outputFruit = new PrintWriter(new FileWriter("data/fruit.html"));
				PrintWriter outputDesigns = new PrintWriter(new FileWriter("data/designs.html"));

				Queue<IAlleleTreeSpecies> speciesQueue = new LinkedList<IAlleleTreeSpecies>();
				for (IAlleleTreeSpecies s : ExtraTreeSpecies.values())
					speciesQueue.add(s);

				Queue<ILogType> logQueue = new LinkedList<ILogType>();
				for (ILogType wood : ILogType.ExtraTreeLog.values())
					logQueue.add(wood);
				
				Queue<ICarpentryWood> plankQueue = new LinkedList<ICarpentryWood>();
				for (ICarpentryWood wood : PlankType.ExtraTreePlanks.values())
					plankQueue.add(wood);
				
				Queue<ExtraTreeFruitGene> fruitQueue = new LinkedList<ExtraTreeFruitGene>();
				for (ExtraTreeFruitGene wood : ExtraTreeFruitGene.values())
					fruitQueue.add(wood);
				
				Queue<IDesign> designQueue = new LinkedList<IDesign>();
				for (IDesign wood : CarpentryManager.carpentryInterface.getSortedDesigns())
					designQueue.add(wood);
				
				fruitQueue.remove(ExtraTreeFruitGene.Apple);

				outputSpecies.println("<table style=\"width: 100%;\">");
				while (!speciesQueue.isEmpty()) {
					outputSpecies.println("<tr>");
					for (int i = 0; i < 4; i++) {
						IAlleleTreeSpecies species = speciesQueue.poll();
						outputSpecies.println("<td>" + (species == null ? "" : species.getName()) + "</td>");
					}
					outputSpecies.println("</tr>");
				}
				outputSpecies.println("</table>");

				
				outputLogs.println("<table style=\"width: 100%;\">");
				while (!logQueue.isEmpty()) {
					outputLogs.println("<tr>");
					for (int i = 0; i < 4; i++) {
						ILogType.ExtraTreeLog wood = (ExtraTreeLog) logQueue.poll();
						if (wood == null) {
							outputLogs.println("<td></td>");
						} else {
							String img = "<img alt=\"" + wood.getName() + "\" src=\"images/logs/"
									+ wood.toString().toLowerCase() + "Bark.png\">";
							outputLogs.println("<td>" + img + " " + wood.getName() + "</td>");

						}

					}
					outputLogs.println("</tr>");
				}
				outputLogs.println("</table>");
				
				
				outputPlanks.println("<table style=\"width: 100%;\">");
				while (!plankQueue.isEmpty()) {
					outputPlanks.println("<tr>");
					for (int i = 0; i < 4; i++) {
						ICarpentryWood wood = plankQueue.poll();
						if (wood == null) {
							outputPlanks.println("<td></td>");
						} else {
							String img = "<img alt=\"" + wood.getName() + "\" src=\"images/planks/"
									+ wood.getName() + ".png\">";
							outputPlanks.println("<td>" + img + " " + wood.getName() + "</td>");

						}

					}
					outputPlanks.println("</tr>");
				}
				outputPlanks.println("</table>");

				outputFruit.println("<table style=\"width: 100%;\">");
				while (!fruitQueue.isEmpty()) {
					outputFruit.println("<tr>");
					for (int i = 0; i < 4; i++) {
						ExtraTreeFruitGene wood = fruitQueue.poll();
						if (wood == null) {
							outputFruit.println("<td></td>");
						} else {
							String fruit = wood.getNameOfFruit();
							String img = "<img alt=\"" + wood.getName() + "\" src=\"images/fruits/"
									+ fruit + ".png\">";
							outputFruit.println("<td>" + img + " " + wood.getName() + "</td>");

						}

					}
					outputFruit.println("</tr>");
				}
				outputFruit.println("</table>");
				
				
				outputDesigns.println("<table style=\"width: 100%;\">");
				while (!designQueue.isEmpty()) {
					outputDesigns.println("<tr>");
					for (int i = 0; i < 4; i++) {
						IDesign wood = designQueue.poll();
						if (wood == null) {
							outputDesigns.println("<td></td>");
						} else {
							String texture = ((EnumPattern)wood.getTopPattern().getPattern()).toString().toLowerCase();
							String img = "<img alt=\"" + texture + "\" src=\"images/pattern/"
									+ texture + ".png\">";
							outputDesigns.println("<td>" + img + " " + wood.getName() + "</td>");

						}

					}
					outputDesigns.println("</tr>");
				}
				outputDesigns.println("</table>");
				
				outputSpecies.close();
				outputLogs.close();
				outputPlanks.close();
				outputFruit.close();
				outputDesigns.close();


			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

}
