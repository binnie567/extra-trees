package binnie.extratrees.core;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import binnie.core.gui.IBinnieGUID;
import binnie.craftgui.extratrees.dictionary.WindowLumbermill;
import binnie.craftgui.extratrees.dictionary.WindowMothDictionary;
import binnie.craftgui.extratrees.dictionary.WindowTreeDictionary;
import binnie.craftgui.extratrees.dictionary.WindowWoodworker;
import binnie.craftgui.minecraft.Window;
import cpw.mods.fml.relauncher.Side;

public enum ExtraTreesGUID implements IBinnieGUID {

	Database, Woodworker, Lumbermill, DatabaseNEI, Incubator, MothDatabase, MothDatabaseNEI, ;

	@Override
	public Window getWindow(EntityPlayer player, World world, int x, int y, int z, Side side) {
		Window window = null;

		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

		IInventory object = null;

		if (tileEntity instanceof IInventory) {
			object = (IInventory) tileEntity;
		}

		switch (this) {
		case Database:
		case DatabaseNEI:
			window = WindowTreeDictionary.create(player, side,
					this == Database ? false : true);
			break;
		case Woodworker:
			window = WindowWoodworker.create(player, object, side);
			break;
		case Lumbermill:
			window = WindowLumbermill.create(player, object, side);
			break;
		case MothDatabase:
		case MothDatabaseNEI:
			window = WindowMothDictionary.create(player, side,
					this == MothDatabase ? false : true);
			break;
		default:
			break;
		}

		return window;

	}

}
