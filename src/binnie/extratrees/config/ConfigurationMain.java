package binnie.extratrees.config;

import binnie.core.config.ConfigFile;
import binnie.core.config.ConfigProperty;
import binnie.core.config.PropBlockID;
import binnie.core.config.PropItemID;

@ConfigFile(filename = "/config/forestry/extratrees/main.conf")
public class ConfigurationMain {

	@ConfigProperty(key = "dictionary")
	@PropItemID(name = "Arborist Database")
	public static int dictionaryID = 12000;
	
	@ConfigProperty(key = "item")
	@PropItemID(name = "General Items")
	public static int itemID = 12001;
	
	@ConfigProperty(key = "food")
	@PropItemID(name = "Food")
	public static int foodID = 12002;
	
	@ConfigProperty(key = "liquid")
	@PropItemID(name = "Liquid")
	public static int liquidID = 12003;
	
	@ConfigProperty(key = "liquidContainer")
	@PropItemID(name = "Liquid Container")
	public static int containerID = 12004;
	
	@ConfigProperty(key = "mothDictionary")
	@PropItemID(name = "Lepidopterist Database")
	public static int mothDictionaryID = 12005;
	
	@ConfigProperty(key = "hammer")
	@PropItemID(name = "Carpentry Hammer")
	public static int hammerID = 12006;
	
	@ConfigProperty(key = "durableHammer")
	@PropItemID(name = "Master Carpentry Hammer")
	public static int durableHammerID = 12007;
	
	@ConfigProperty(key = "planks")
	@PropBlockID(name = "Planks")
	public static int planksID = 3700;
	
	@ConfigProperty(key = "fence")
	@PropBlockID(name = "Fence")
	public static int fenceID = 3701;
	
	@ConfigProperty(key = "stairs")
	@PropBlockID(name = "Stairs")
	public static int stairsID = 3702;
	
	@ConfigProperty(key = "tile")
	@PropBlockID(name = "Woodworker Tile")
	public static int tileID = 3703;
	
	@ConfigProperty(key = "log")
	@PropBlockID(name = "Log")
	public static int logID = 3704;
	
	@ConfigProperty(key = "machine")
	@PropBlockID(name = "Machine")
	public static int machineID = 3705;
	
	@ConfigProperty(key = "panel")
	@PropBlockID(name = "Woodworker Panel")
	public static int panelID = 3706;

	@ConfigProperty(key = "slab")
	@PropBlockID(name = "Slab")
	public static int slabID = 3707;
	
	@ConfigProperty(key = "doubleSlab")
	@PropBlockID(name = "DoubleSlab")
	public static int doubleSlabID = 3708;
	
	@ConfigProperty(key = "gate")
	@PropBlockID(name = "Gate")
	public static int gateID = 3709;
	
	@ConfigProperty(key = "door")
	@PropBlockID(name = "Door")
	public static int doorID = 3710;
	
}
