package binnie.extratrees.carpentry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;
import binnie.extratrees.api.ICarpentryInterface;
import binnie.extratrees.api.ICarpentryWood;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.IDesignCategory;
import binnie.extratrees.api.ILayout;
import binnie.extratrees.api.IPattern;

public class CarpentryInterface implements ICarpentryInterface {

	static Map<Integer, ICarpentryWood> woodMap = new LinkedHashMap<Integer, ICarpentryWood>();

	static Map<Integer, IDesign> designMap = new LinkedHashMap<Integer, IDesign>();

	@Override
	public boolean registerCarpentryWood(int index, ICarpentryWood wood) {
		if (wood != null)
			return woodMap.put(index, wood) == null;
		return false;
	}

	@Override
	public int getCarpentryWoodIndex(ICarpentryWood wood) {
		for (Integer integer : woodMap.keySet())
			if (woodMap.get(integer).equals(wood))
				return integer;
		return -1;
	}

	@Override
	public ICarpentryWood getCarpentryWood(int index) {
		return woodMap.get(index);
	}

	@Override
	public boolean registerDesign(int index, IDesign wood) {
		if (wood != null)
			return designMap.put(index, wood) == null;
		return false;
	}

	@Override
	public int getDesignIndex(IDesign wood) {
		for (Integer integer : designMap.keySet())
			if (designMap.get(integer).equals(wood))
				return integer;
		return -1;
	}

	@Override
	public IDesign getDesign(int index) {
		return designMap.get(index);
	}

	@Override
	public ICarpentryWood getCarpentryWood(ItemStack plank) {
		if(plank == null) return null;
		for (Map.Entry<Integer, ICarpentryWood> entry : woodMap.entrySet()) {
			ItemStack key = entry.getValue().getPlank();
			if (key == null)
				continue;
			if (key.isItemEqual(plank)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	public ILayout getLayout(IPattern pattern, boolean inverted) {
		return Layout.get(pattern, inverted);
	}

	static Map<String, IDesignCategory> designCategories = new HashMap<String, IDesignCategory>();

	@Override
	public boolean registerDesignCategory(IDesignCategory category) {
		if(category != null && category.getId() != null)
			return designCategories.put(category.getId(), category) == null;
		return false;
	}
	
	@Override
	public IDesignCategory getDesignCategory(String id) {
		return designCategories.get(id);
	}

	@Override
	public Collection<IDesignCategory> getAllDesignCategories() {
		List<IDesignCategory> categories = new ArrayList<IDesignCategory>();
		for(IDesignCategory category : designCategories.values())
			if(category.getDesigns().size() > 0)
				categories.add(category);
		return categories;
	}

	@Override
	public List<IDesign> getSortedDesigns() {
		List<IDesign> designs = new ArrayList<IDesign>();
		for (IDesignCategory category : getAllDesignCategories())
			designs.addAll(category.getDesigns());
		return designs;
	}

}
