package binnie.extratrees.carpentry;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import binnie.core.block.TileEntityMetadata;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockCarpentryPanel extends BlockCarpentry {

	public BlockCarpentryPanel(int id) {
		super(id);
		Block.useNeighborBrightness[id] = true;
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1/16f, 1.0F);
		setLightOpacity(0);
	}
	
	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
	}

	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y,
			int z) {
		CarpentryBlock block = getCarpentryBlock(world, x, y, z);
		//System.out.println(block.getAxis());
		switch(block.getFacing()) {
		case DOWN:
			this.setBlockBounds(0, 15/16f, 0, 1f, 1f, 1f); break;
		case EAST:
			this.setBlockBounds(0, 0, 0, 1/16f, 1f, 1f); break;
		case NORTH:
			this.setBlockBounds(0, 0, 15/16f, 1f, 1f, 1f); break;
		case SOUTH:
			this.setBlockBounds(0, 0, 0, 1f, 1f, 1/16f); break;
		case UP:
			this.setBlockBounds(0, 0, 0, 1f, 1/16f, 1f); break;
		case WEST:
			this.setBlockBounds(15/16f, 0, 0, 1f, 1f, 1f); break;
		case UNKNOWN:
		default:
			break;
		
		}
	}

	public void setBlockBoundsForItemRender() {
		this.setBlockBounds(0, 0, 0, 1, 1 / 16f, 1f);
	}

	@Override
	public String getBlockName(ItemStack stack) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(TileEntityMetadata
				.getItemDamage(stack));
		return block.getDesign().getName() + " Wooden Panel";
	}

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return AxisAlignedBB.getAABBPool().getAABB((double)par2 + this.minX, (double)par3 + this.minY, (double)par4 + this.minZ, (double)par2 + this.maxX, (double)((float)par3 + this.maxY), (double)par4 + this.maxZ);
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess world, int x, int y, int z, int side)
    {
		return super.shouldSideBeRendered(world, x, y, z, side);
    }
    
    @Override
    public CarpentryBlock getCarpentryBlock(IBlockAccess world, int x, int y, int z) {
		return ModuleCarpentry.getCarpentryPanel(TileEntityMetadata.getTileMetadata(world, x, y, z));
	}

	public static boolean isValidPanelPlacement(World world, int x, int y, int z,
			ForgeDirection facing) {
		if(facing == ForgeDirection.UNKNOWN) return false;
		int bx = x - facing.offsetX; 
		int by = y - facing.offsetY; 
		int bz = z - facing.offsetZ;
		Block block = Block.blocksList[world.getBlockId(bx, by, bz)];
		if(block == null) return false;
		return block.isBlockSolidOnSide(world, bx, by, bz, facing);
	}
	
	public int getPlacedMeta(ItemStack item, World world, int x, int y, int z, ForgeDirection clickedBlock) {
		CarpentryBlock block  = ModuleCarpentry.getCarpentryPanel(TileEntityMetadata.getItemDamage(item));
		ForgeDirection facing = clickedBlock;
		
		boolean valid = true;
		if(!isValidPanelPlacement(world, x, y, z, facing)) {
			valid = false;
			for(ForgeDirection direction : ForgeDirection.values()) {
				if(isValidPanelPlacement(world, x, y, z, direction)) {
					facing = direction;
					valid = true;
					break;
				}
			}
		}
		if(!valid) return -1;
		
		block.setFacing(facing);
		
		return block.getBlockMetadata();
	}
	
	public void onNeighborBlockChange(World world, int x, int y, int z, int par5)
    {
        super.onNeighborBlockChange(world, x, y, z, par5);
        CarpentryBlock block = getCarpentryBlock(world, x, y, z);
        if(!isValidPanelPlacement(world, x, y, z, block.getFacing())) {
        	for(ItemStack stack : this.getBlockDropped(world, x, y, z, world.getBlockId(x, y, z), 0)) {
        		this.dropBlockAsItem_do(world, x, y, z, stack);
        	}
        	world.setBlockToAir(x, y, z);
        }
    }
	
}
