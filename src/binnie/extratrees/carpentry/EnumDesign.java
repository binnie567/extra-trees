package binnie.extratrees.carpentry;

import java.util.ArrayList;
import java.util.List;

import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.IDesignCategory;
import binnie.extratrees.api.ILayout;

public enum EnumDesign implements IDesign {

	Blank("Blank"),

	// Orthogonal Patterns
	Octagon("Octagon"),
	Diamond("Diamond"),
	Ringed("Ringed"),
	Squared("Squared"),
	Multiply("Multiply"),
	
	// Stiped Patterns
	Halved("Halved"),
	Striped("Striped"),
	ThinStriped("Thin Striped"),
	
	// CHequered Patterns
	Chequered("Full Chequered"),
	Tiled("Full Tiled"),
	
	ChequeredB("Chequered"), // Formed from long strips
	TiledB("Tiled"), // Formed from long strips
	
	// Corner Patterns
	VeryThinCorner("Very Thin Cornered"),
	ThinCorner("Thin Cornered"),
	Corner("Cornered"),
	ThickCorner("Thick Cornered"),
	
	// Edged Patterns
	Edged("Edged"),
	ThinEdged("Thin Edged"),
	
	// Barred Patterns
	ThinBarred("Thin Barred"),
	Barred("Barred"),
	ThickBarred("Thick Barred"),
	
	// Diagonal Patterns
	Diagonal("Diagonal"),
	ThickDiagonal("Thick Diagonal"),
	
	// Saltire
	ThinSaltire("Thin Saltire"),
	Saltire("Saltire"),
	ThickSaltire("Thick Saltire"),
	
	// Crosses
	ThinCrossed("Thin Crossed"),
	Crossed("Crossed"),
	ThickCrossed("Thick Crossed"),
	
	// T Sections
	ThinTSection("Thin T Section"),
	TSection("T Section"),
	ThickTSection("Thick T Section"),
	
	// Barred Corners
	ThinBarredCorner("Thin Barred Corner"),
	BarredCorner("Barred Corner"),
	ThickBarredCorner("Thick Barred Corner"),
		
	// StripedCorner
	ThinStripedCorner("Thin Striped Corner"),
	StripedCorner("Striped Corner"),
		
	// Emblems
	Emblem1("Emblem 1"), 
	Emblem2("Emblem 2"), 
	Emblem3("Emblem 3"), 
	Emblem4("Emblem 4"), 
	Emblem5("Emblem 5"),
	
	// Letters
	LetterA("Letter A"),
	LetterB("Letter B"),
	LetterC("Letter C"),
	LetterD("Letter D"),
	LetterE("Letter E"),
	LetterF("Letter F"),
	LetterG("Letter G"),
	LetterH("Letter H"),
	LetterI("Letter I"),
	LetterJ("Letter J"),
	LetterK("Letter K"),
	LetterL("Letter L"),
	LetterM("Letter M"),
	LetterN("Letter N"),
	LetterO("Letter O"),
	LetterP("Letter P"),
	LetterQ("Letter Q"),
	LetterR("Letter R"),
	LetterS("Letter S"),
	LetterT("Letter T"),
	LetterU("Letter U"),
	LetterV("Letter V"),
	LetterW("Letter W"),
	LetterX("Letter X"),
	LetterY("Letter Y"),
	LetterZ("Letter Z"),
	
	ThinCurvedCrossed("Thin Curved Crossed"),
	ThinCurvedBarredCorner("Thin Curved Barred Corner"),
	CurvedBarredCorner("Curved Barred Corner"),
	ThinCurvedCorner("Thin Curved Corner"),
	CurvedCorner("Curved Corner"),
	ThinCurvedTSection("Thin Curved T Section"),
	CurvedTSection("Curved T Section"),
	
	BarredEnd("Barred End"),
	
	DiagonalCorner("Diagonal Corner"),
	DiagonalTSection("Diagonal T Section"),
	DiagonalCurvedCorner("Diagonal Curved Corner"),
	DiagonalCurvedTSection("Diagonal Curved T Section"),
	OrnateBarred("Ornate Barred"),
	SplitBarred("Split Barred"),
	SplitBarredCorner("Split Barred Corner"),
	SplitBarredTSection("Split Barred T Section"),
	SplitCrossed("Split Crossed"),
	SplitBarredEnd("Split Barred End"),
	OrnateThinBarred("Ornate Thin Barred"),
	
	Circle("Circle"),
	Plus("Plus"),
	Creeper("Creeper"),
	OrnateStripedCorner("Ornate Striped Corner"),
	
	Test("Testing Block"),
	
	DiagonalHalved("Diagonal Halved"),
	Diagonal1Edged("Cornered Diagonal"),
	Diagonal2Edged("Opposite Cornered Diagonal"),
	ThickDiagonal1Edged("Thick Cornered Diagonal"),
	
	ThinBarredEnd("Thin Barred End"),
	ThickBarredEnd("Thick Barred End"),
	
	OverlappedBarred("Overlapped Barred"),
	OverlappedSplitBarred("Overlapped Split Barred"),
	;
	
	
	enum Category implements IDesignCategory {
		Design("Designs & Emblems"),
		Stripes("Squares & Stripes"),
		Edges("Edges"),
		Barred("Bars"),
		Letters("Letters"),
		Diagonal("Diagonals"),
		;
		String name;
		List<IDesign> designs = new ArrayList<IDesign>();
		private Category(String name) {
			this.name = name;
			CarpentryManager.carpentryInterface.registerDesignCategory(this);
		}
		@Override
		public String getName() {
			return name;
		}

		@Override
		public List<IDesign> getDesigns() {
			return designs;
		}

		@Override
		public void addDesign(IDesign design) {
			designs.add(design);
		}
		@Override
		public String getId() {
			return this.toString().toLowerCase();
		}
	}
	
	static {
		Category.Design.addDesign(Blank);
		Category.Design.addDesign(Octagon);
		Category.Design.addDesign(Diamond);
		Category.Design.addDesign(Ringed);
		Category.Design.addDesign(Squared);
		Category.Design.addDesign(Multiply);
		Category.Design.addDesign(Plus);
		Category.Design.addDesign(Circle);
		
		Category.Design.addDesign(Emblem1);
		Category.Design.addDesign(Emblem2);
		Category.Design.addDesign(Emblem3);
		Category.Design.addDesign(Emblem4);
		Category.Design.addDesign(Emblem5);
		Category.Design.addDesign(Creeper);
		
		Category.Stripes.addDesign(Chequered);
		Category.Stripes.addDesign(ChequeredB);
		Category.Stripes.addDesign(Tiled);
		Category.Stripes.addDesign(TiledB);
		Category.Stripes.addDesign(Striped);
		Category.Stripes.addDesign(ThinStriped);
		Category.Stripes.addDesign(ThinStripedCorner);
		Category.Stripes.addDesign(StripedCorner);
		Category.Stripes.addDesign(OrnateStripedCorner);
		
		Category.Edges.addDesign(Halved);
		Category.Edges.addDesign(Corner);
		Category.Edges.addDesign(ThickCorner);
		Category.Edges.addDesign(Edged);
		Category.Edges.addDesign(ThinCorner);
		Category.Edges.addDesign(ThinEdged);
		Category.Edges.addDesign(VeryThinCorner);
		Category.Edges.addDesign(ThinCurvedCorner);
		Category.Edges.addDesign(CurvedCorner);
		
		Category.Barred.addDesign(ThinBarred);
		Category.Barred.addDesign(ThinBarredCorner);
		Category.Barred.addDesign(ThinTSection);
		Category.Barred.addDesign(ThinCrossed);
		Category.Barred.addDesign(ThinBarredEnd);
		Category.Barred.addDesign(OrnateThinBarred);
		
		Category.Barred.addDesign(Barred);
		Category.Barred.addDesign(BarredCorner);
		Category.Barred.addDesign(TSection);
		Category.Barred.addDesign(Crossed);
		Category.Barred.addDesign(BarredEnd);
		Category.Barred.addDesign(OverlappedBarred);
		Category.Barred.addDesign(OrnateBarred);
		
		Category.Barred.addDesign(ThickBarred);
		Category.Barred.addDesign(ThickBarredCorner);
		Category.Barred.addDesign(ThickTSection);
		Category.Barred.addDesign(ThickCrossed);
		Category.Barred.addDesign(ThickBarredEnd);
		
		Category.Barred.addDesign(ThinCurvedBarredCorner);
		Category.Barred.addDesign(CurvedBarredCorner);
		Category.Barred.addDesign(ThinCurvedTSection);
		Category.Barred.addDesign(CurvedTSection);
		Category.Barred.addDesign(ThinCurvedCrossed);
		
		Category.Barred.addDesign(SplitBarred);
		Category.Barred.addDesign(SplitBarredCorner);
		Category.Barred.addDesign(SplitBarredTSection);
		Category.Barred.addDesign(SplitCrossed);
		Category.Barred.addDesign(SplitBarredEnd);
		Category.Barred.addDesign(OverlappedSplitBarred);
		
		Category.Diagonal.addDesign(ThinSaltire);
		Category.Diagonal.addDesign(Diagonal);
		Category.Diagonal.addDesign(DiagonalCorner);
		Category.Diagonal.addDesign(DiagonalTSection);
		Category.Diagonal.addDesign(DiagonalCurvedCorner);
		Category.Diagonal.addDesign(DiagonalCurvedTSection);
		Category.Diagonal.addDesign(Saltire);
		Category.Diagonal.addDesign(ThickDiagonal);
		Category.Diagonal.addDesign(ThickSaltire);
		Category.Diagonal.addDesign(DiagonalHalved);
		Category.Diagonal.addDesign(Diagonal1Edged);
		Category.Diagonal.addDesign(Diagonal2Edged);
		Category.Diagonal.addDesign(ThickDiagonal1Edged);
		
		// Letters
		/*
		Category.Letters.addDesign(LetterA);
		Category.Letters.addDesign(LetterB);
		Category.Letters.addDesign(LetterC);
		Category.Letters.addDesign(LetterD);
		Category.Letters.addDesign(LetterE);
		Category.Letters.addDesign(LetterF);
		Category.Letters.addDesign(LetterG);
		Category.Letters.addDesign(LetterH);
		Category.Letters.addDesign(LetterI);
		Category.Letters.addDesign(LetterJ);
		Category.Letters.addDesign(LetterK);
		Category.Letters.addDesign(LetterL);
		Category.Letters.addDesign(LetterM);
		Category.Letters.addDesign(LetterN);
		Category.Letters.addDesign(LetterO);
		Category.Letters.addDesign(LetterP);
		Category.Letters.addDesign(LetterQ);
		Category.Letters.addDesign(LetterR);
		Category.Letters.addDesign(LetterS);
		Category.Letters.addDesign(LetterT);
		Category.Letters.addDesign(LetterU);
		Category.Letters.addDesign(LetterV);
		Category.Letters.addDesign(LetterW);
		Category.Letters.addDesign(LetterX);
		Category.Letters.addDesign(LetterY);
		Category.Letters.addDesign(LetterZ);
		Category.Letters.addDesign(Test);
		*/
	}
	
	void setEdgePatterns(ILayout north, ILayout east, ILayout south, ILayout west) {
		setNorthPattern(north);
		setEastPattern(east);
		setSouthPattern(south);
		setWestPattern(west);
	}
	
	void setEdgePatterns(ILayout face) {
		setEdgePatterns(face, face, face, face);
	}
	
	void setupStriped(ILayout vert) {
		setTopPattern(vert);
		setEdgePatterns(vert.invert(), EnumPattern.Blank.layout(true), vert, EnumPattern.Blank.layout());
	}
	
	void setChequered(ILayout cheq) {
		setAllPatterns(cheq);
		setNorthPattern(cheq.invert());
		setSouthPattern(cheq.invert());
		setBottomPattern(cheq.invert());
	}
	
	void setStripedChequered(ILayout cheq, ILayout stripe) {
		setAllPatterns(cheq);
		setNorthPattern(stripe.invert());
		setSouthPattern(stripe.invert());
		setEastPattern(stripe);
		setWestPattern(stripe);
	}
	
	void setCornered(ILayout corner, ILayout edge) {
		setTopPattern(corner);
		setNorthPattern(EnumPattern.Blank.layout());
		setEastPattern(edge.flipHorizontal());
		setSouthPattern(edge);
		setWestPattern(EnumPattern.Blank.layout());
	}
	
	void setEdged(ILayout edge) {
		setAllPatterns(edge);
		setWestPattern(EnumPattern.Blank.layout());
		setEastPattern(EnumPattern.Blank.layout(true));
		northPattern = northPattern.flipHorizontal();
	}
	
	void setBarred(ILayout bar) {
		setAllPatterns(bar);
		setEastPattern(EnumPattern.Blank.layout(true));
		setWestPattern(EnumPattern.Blank.layout(true));
	}
	
	void setDiagonal(ILayout diagonal, ILayout edged) {
		setAllPatterns(edged);
		setTopPattern(diagonal);
		northPattern = northPattern.flipHorizontal();
		southPattern = southPattern.flipHorizontal();
	}
	
	void setSaltire(ILayout saltire, ILayout bar) {
		setTopPattern(saltire);
		setEdgePatterns(bar.invert());
	}
	
	void setCross(ILayout saltire, ILayout bar) {
		setTopPattern(saltire);
		setEdgePatterns(bar);
	}
	
	void setTSection(ILayout tsection, ILayout bar) {
		setTopPattern(tsection);
		setEdgePatterns(bar);
		setWestPattern(EnumPattern.Blank.layout(true));
	}
	
	void setBarredCorner(ILayout corner, ILayout bar) {
		setTSection(corner, bar);
		setNorthPattern(EnumPattern.Blank.layout(true));
	}
	
	void setStripedCorner(ILayout corner, ILayout striped) {
		setCornered(corner, striped);
	}
	
	void setLetterPattern(ILayout letter) {
		setAllPatterns(EnumPattern.Blank.layout(true));
		setTopPattern(letter);
		setBottomPattern(EnumPattern.Blank.layout(true));
	}
	
	void setBarredEndPattern(ILayout end, ILayout bar) {
		setAllPatterns(EnumPattern.Blank.layout(true));
		setTopPattern(end);
		setWestPattern(bar);
	}
	

	void setDiagonalCorner(ILayout diagonal, ILayout bar, ILayout edged) {
		setAllPatterns(EnumPattern.Blank.layout(true));
		setTopPattern(diagonal);
		setWestPattern(bar.invert());
		setNorthPattern(edged.flipHorizontal());
		setSouthPattern(edged);
	}
	
	void setDiagonalTSection(ILayout diagonal, ILayout bar, ILayout edged) {
		setAllPatterns(EnumPattern.Blank.layout(true));
		setTopPattern(diagonal);
		setWestPattern(bar.invert());
		setNorthPattern(bar.invert());
		setEastPattern(edged.flipHorizontal());
		setSouthPattern(edged);
	}
	
	static {
		
		// Orthogonal
		Octagon.setAllPatterns(EnumPattern.Octagon.layout());
		Diamond.setAllPatterns(EnumPattern.Diamond.layout());
		Ringed.setAllPatterns(EnumPattern.Ringed.layout());
		Squared.setAllPatterns(EnumPattern.Squared.layout());
		Multiply.setAllPatterns(EnumPattern.Multiply.layout());
		
		// Striped Patterns
		ThinStriped.setupStriped(EnumPattern.ThinStriped.layout());
		Striped.setupStriped(EnumPattern.Striped.layout());
		Halved.setupStriped(EnumPattern.Halved.layout());
		
		// Chequered
		Chequered.setChequered(EnumPattern.Chequered.layout());
		Tiled.setChequered(EnumPattern.Tiled.layout());
		ChequeredB.setStripedChequered(EnumPattern.Chequered.layout(), EnumPattern.Halved.layout());
		TiledB.setStripedChequered(EnumPattern.Tiled.layout(), EnumPattern.Striped.layout());
		
		// Corner Patterns
		VeryThinCorner.setCornered(EnumPattern.VeryThinCorner.layout(), EnumPattern.ThinEdged.layout());
		ThinCorner.setCornered(EnumPattern.ThinCorner.layout(), EnumPattern.Edged.layout());
		Corner.setCornered(EnumPattern.Corner.layout(), EnumPattern.Halved.layout());
		ThickCorner.setCornered(EnumPattern.ThickCorner.layout(), EnumPattern.Edged.layout(true).flipHorizontal());
		
		ThinCurvedCorner.setCornered(EnumPattern.ThinCurvedCorner.layout(), EnumPattern.Edged.layout());
		CurvedCorner.setCornered(EnumPattern.CurvedCorner.layout(), EnumPattern.Halved.layout());
		
		// Edged Patterns
		Edged.setEdged(EnumPattern.Edged.layout());
		ThinEdged.setEdged(EnumPattern.ThinEdged.layout());
		
		// Barred Patterns
		ThinBarred.setBarred(EnumPattern.ThinBarred.layout());
		Barred.setBarred(EnumPattern.Barred.layout());
		ThickBarred.setBarred(EnumPattern.ThickBarred.layout());
		
		// Diagonal Patterns
		Diagonal.setDiagonal(EnumPattern.Diagonal.layout(), EnumPattern.Edged.layout());
		ThickDiagonal.setDiagonal(EnumPattern.ThickDiagonal.layout(), EnumPattern.Halved.layout());
		
		// Saltire Patterns
		ThinSaltire.setSaltire(EnumPattern.ThinSaltire.layout(), EnumPattern.ThickBarred.layout());
		Saltire.setSaltire(EnumPattern.Saltire.layout(), EnumPattern.Barred.layout());
		ThickSaltire.setSaltire(EnumPattern.ThickSaltire.layout(), EnumPattern.ThinBarred.layout());
		
		// Cross Patterns
		ThinCrossed.setCross(EnumPattern.ThinCrossed.layout(), EnumPattern.ThinBarred.layout());
		Crossed.setCross(EnumPattern.Crossed.layout(), EnumPattern.Barred.layout());
		ThickCrossed.setCross(EnumPattern.ThickCrossed.layout(), EnumPattern.ThickBarred.layout());
		
		ThinCurvedCrossed.setCross(EnumPattern.ThinCurvedCrossed.layout(), EnumPattern.ThinBarred.layout());
	
		// T Section
		ThinTSection.setTSection(EnumPattern.ThinTSection.layout(), EnumPattern.ThinBarred.layout());
		TSection.setTSection(EnumPattern.TSection.layout(), EnumPattern.Barred.layout());
		ThickTSection.setTSection(EnumPattern.ThickTSection.layout(), EnumPattern.ThickBarred.layout());
		
		ThinCurvedTSection.setTSection(EnumPattern.ThinCurvedTSection.layout(), EnumPattern.ThinBarred.layout());
		CurvedTSection.setTSection(EnumPattern.CurvedTSection.layout(), EnumPattern.Barred.layout());
		
		// Barred Corners
		ThinBarredCorner.setBarredCorner(EnumPattern.ThinBarredCorner.layout(), EnumPattern.ThinBarred.layout());
		BarredCorner.setBarredCorner(EnumPattern.BarredCorner.layout(), EnumPattern.Barred.layout());
		ThickBarredCorner.setBarredCorner(EnumPattern.ThickBarredCorner.layout(), EnumPattern.ThickBarred.layout());
			
		ThinCurvedBarredCorner.setBarredCorner(EnumPattern.ThinCurvedBarredCorner.layout(), EnumPattern.ThinBarred.layout());
		CurvedBarredCorner.setBarredCorner(EnumPattern.BarredCurvedCorner.layout(), EnumPattern.Barred.layout());
		
		// StripedCorner
		ThinStripedCorner.setStripedCorner(EnumPattern.ThinStripedCorner.layout(), EnumPattern.ThinStriped.layout());
		StripedCorner.setStripedCorner(EnumPattern.StripedCorner.layout(), EnumPattern.Striped.layout());
		OrnateStripedCorner.setStripedCorner(EnumPattern.OrnateStripedCorner.layout(), EnumPattern.ThinStriped.layout());
			
		// Emblems
		Emblem1.setAllPatterns(EnumPattern.Emblem1.layout());
		Emblem2.setAllPatterns(EnumPattern.Emblem2.layout());
		Emblem3.setAllPatterns(EnumPattern.Emblem3.layout()); 
		Emblem4.setAllPatterns(EnumPattern.Emblem4.layout());
		Emblem5.setAllPatterns(EnumPattern.Emblem5.layout());
		
		// Letter
		LetterA.setLetterPattern(EnumPattern.LetterA.layout());
		LetterB.setLetterPattern(EnumPattern.LetterB.layout());
		LetterF.setLetterPattern(EnumPattern.LetterF.layout());
		LetterS.setLetterPattern(EnumPattern.LetterS.layout());
		LetterT.setLetterPattern(EnumPattern.LetterT.layout());
		
		BarredEnd.setBarredEndPattern(EnumPattern.BarredEnd.layout(), EnumPattern.Barred.layout());
		
		DiagonalCorner.setDiagonalCorner(EnumPattern.DiagonalCorner.layout(), EnumPattern.Barred.layout(), EnumPattern.Edged.layout());
		DiagonalTSection.setDiagonalTSection(EnumPattern.DiagonalTSection.layout(), EnumPattern.Barred.layout(), EnumPattern.Edged.layout());
	
		DiagonalCurvedCorner.setDiagonalCorner(EnumPattern.DiagonalCurvedCorner.layout(), EnumPattern.Barred.layout(), EnumPattern.Edged.layout());
		DiagonalCurvedTSection.setDiagonalTSection(EnumPattern.DiagonalCurvedTSection.layout(), EnumPattern.Barred.layout(), EnumPattern.Edged.layout());
	
		OrnateBarred.setBarred(EnumPattern.OrnateBarred.layout());
		OrnateThinBarred.setBarred(EnumPattern.OrnateThinBarred.layout());
		
		SplitBarred.setBarred(EnumPattern.SplitBarred.layout());
		SplitBarredCorner.setBarredCorner(EnumPattern.SplitBarredCorner.layout(), EnumPattern.SplitBarred.layout());
		SplitBarredTSection.setTSection(EnumPattern.SplitBarredTSection.layout(), EnumPattern.SplitBarred.layout());
		SplitCrossed.setCross(EnumPattern.SplitCrossed.layout(), EnumPattern.SplitBarred.layout());
		SplitBarredEnd.setBarredEndPattern(EnumPattern.SplitBarredEnd.layout(), EnumPattern.SplitBarred.layout());
		
		
		Circle.setAllPatterns(EnumPattern.Circle.layout());
		Plus.setAllPatterns(EnumPattern.Plus.layout());
		
		Creeper.setAllPatterns(EnumPattern.Blank.layout(true));
		Creeper.setTopPattern(EnumPattern.Creeper.layout());
		
		Test.setTopPattern(EnumPattern.Top.layout());
		Test.setBottomPattern(EnumPattern.Bottom.layout());
		Test.setNorthPattern(EnumPattern.North.layout());
		Test.setSouthPattern(EnumPattern.South.layout());
		Test.setEastPattern(EnumPattern.East.layout());
		Test.setWestPattern(EnumPattern.West.layout());
		
		
		
		DiagonalHalved.setTopPattern(EnumPattern.DiagonalHalved.layout());
		DiagonalHalved.setEdgePatterns(
				EnumPattern.Blank.layout(), 
				EnumPattern.Blank.layout(true), 
				EnumPattern.Blank.layout(true), 
				EnumPattern.Blank.layout());
		
		Diagonal1Edged.setTopPattern(EnumPattern.Diagonal1Edged.layout());
		Diagonal1Edged.setEdgePatterns(
				EnumPattern.Edged.layout().flipHorizontal(), 
				EnumPattern.Blank.layout(true), 
				EnumPattern.Blank.layout(true),
				EnumPattern.Edged.layout());
		
		
		Diagonal2Edged.setTopPattern(EnumPattern.Diagonal2Edged.layout());
		Diagonal2Edged.setEdgePatterns(
				EnumPattern.Edged.layout(), 
				EnumPattern.Edged.layout().flipHorizontal(), 
				EnumPattern.Edged.layout(),
				EnumPattern.Edged.layout().flipHorizontal());
		
		ThickDiagonal1Edged.setTopPattern(EnumPattern.ThickDiagonal1Edged.layout());
		ThickDiagonal1Edged.setEdgePatterns(
				EnumPattern.Halved.layout().flipHorizontal(), 
				EnumPattern.Blank.layout(true), 
				EnumPattern.Blank.layout(true),
				EnumPattern.Halved.layout());
		
		
		ThickBarredEnd.setBarredEndPattern(EnumPattern.ThickBarredEnd.layout(), EnumPattern.ThickBarred.layout());
		ThinBarredEnd.setBarredEndPattern(EnumPattern.ThinBarredEnd.layout(), EnumPattern.ThinBarred.layout());
		

		OverlappedSplitBarred.setAllPatterns(EnumPattern.SplitBarred.layout());
		OverlappedSplitBarred.setTopPattern(EnumPattern.OverlappedSplitBarred.layout());
		
		OverlappedBarred.setAllPatterns(EnumPattern.Barred.layout());
		OverlappedBarred.setTopPattern(EnumPattern.OverlappedBarred.layout());
		
	}
	
	
	String name;
	
	private EnumDesign(String name) {
		topPattern = Layout.get(EnumPattern.Blank, false);
		bottomPattern = Layout.get(EnumPattern.Blank, false);
		northPattern = Layout.get(EnumPattern.Blank, false);
		eastPattern = Layout.get(EnumPattern.Blank, false);
		southPattern = Layout.get(EnumPattern.Blank, false);
		westPattern = Layout.get(EnumPattern.Blank, false);
		this.name = name;
	}
	
	

	

	

	private void setAllPatterns(ILayout layout) {
		setTopPattern(layout);
		setBottomPattern(layout);
		setNorthPattern(layout);
		setEastPattern(layout);
		setSouthPattern(layout);
		setWestPattern(layout);
	}

	@Override
	public String getName() {
		return name;
	}
	
	// Patterns
	
	public ILayout getTopPattern() {
		return topPattern;
	}

	public void setTopPattern(ILayout layout) {
		this.topPattern = layout;
		setBottomPattern(layout);
	}
	
	public ILayout getBottomPattern() {
		return bottomPattern;
	}

	public void setBottomPattern(ILayout layout) {
		this.bottomPattern = layout;
	}
	
	public ILayout getNorthPattern() {
		return northPattern;
	}

	public void setNorthPattern(ILayout layout) {
		this.northPattern = layout;
	}
	
	public ILayout getSouthPattern() {
		return southPattern;
	}

	public void setSouthPattern(ILayout layout) {
		this.southPattern = layout;
	}
	
	public ILayout getEastPattern() {
		return eastPattern;
	}

	public void setEastPattern(ILayout layout) {
		this.eastPattern = layout;
	}
	
	public ILayout getWestPattern() {
		return westPattern;
	}

	public void setWestPattern(ILayout layout) {
		this.westPattern = layout;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	ILayout topPattern;
	ILayout bottomPattern;
	ILayout northPattern;
	ILayout southPattern;
	ILayout eastPattern;
	ILayout westPattern;

}
