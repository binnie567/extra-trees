package binnie.extratrees.carpentry;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import binnie.core.block.TileEntityMetadata;

public class CarpentryItemRenderer implements IItemRenderer {

	private void renderStairBlock(RenderBlocks renderer, ItemStack item,
			float f, float g, float h) {
		
		GL11.glTranslatef(f, g, h);
		
		Block block = Block.blocksList[item.itemID];
		
		(new CarpentryBlockRenderer()).renderInventoryBlock(block, TileEntityMetadata.getItemDamage(item), 0, renderer);
	
		GL11.glTranslatef(-f, -g, -h);
	
	}

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case INVENTORY:
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		switch (type) {
		case ENTITY:
			renderStairBlock((RenderBlocks) data[0], item, 0, 0, 0);
			break;
		case EQUIPPED:
			renderStairBlock((RenderBlocks) data[0], item,  0.5f, 0.5f, 0.5f);
			break;
		case INVENTORY:
			renderStairBlock((RenderBlocks) data[0], item, 0f, 0f, 0f);
			break;
		default:
			break;
		}
	}

}
