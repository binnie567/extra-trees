package binnie.extratrees.carpentry;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import binnie.core.block.BlockMetadata;
import binnie.core.block.TileEntityMetadata;
import binnie.extratrees.PluginExtraTrees;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.IToolHammer;
import binnie.extratrees.block.PlankType;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.core.Tabs;

public class BlockCarpentry extends BlockMetadata {

	@ForgeSubscribe
	public void onClick(PlayerInteractEvent event) {
		if (event.action != PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK)
			return;
		World world = event.entityPlayer.worldObj;
		EntityPlayer player = event.entityPlayer;
		int x = event.x;
		int y = event.y;
		int z = event.z;

		if (!(Block.blocksList[world.getBlockId(x, y, z)] instanceof BlockCarpentry))
			return;

		BlockCarpentry blockC = (BlockCarpentry) Block.blocksList[world.getBlockId(x, y, z)];

		ItemStack item = player.getHeldItem();

		if (item == null)
			return;

		if (!(item.getItem() instanceof IToolHammer))
			return;
		
		if(!((IToolHammer) item.getItem()).isActive(item))
			return;

		CarpentryBlock block = blockC.getCarpentryBlock(world, x, y, z);

		TileEntityMetadata tile = (TileEntityMetadata) world.getBlockTileEntity(x, y, z);
		// 1, 2, 5 are normal rotation faces
		block.rotate(event.face, item, player, world, x, y, z);

		tile.setTileMetadata(block.getBlockMetadata());
		world.markBlockForUpdate(x, y, z);

	}

	public BlockCarpentry(int id) {
		super(id, Material.wood);
		setCreativeTab(Tabs.tabArboriculture);
		setUnlocalizedName("carpentry");
		setResistance(5.0F);
		setHardness(2.0F);
		setStepSound(soundWoodFootstep);
	}

	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List itemList) {
		for (IDesign design : CarpentryManager.carpentryInterface.getSortedDesigns())
			itemList.add(ModuleCarpentry.getItemStack(this, PlankType.ExtraTreePlanks.Apple,
					PlankType.VanillaPlanks.BIRCH, design));
	}

	@Override
	public int getRenderType() {
		return PluginExtraTrees.carpentryID;
	}

	@Override
	public String getBlockName(ItemStack stack) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(TileEntityMetadata
				.getItemDamage(stack));
		return block.getDesign().getName() + " Wooden Tile";
	}

	public CarpentryBlock getCarpentryBlock(IBlockAccess world, int x, int y, int z) {
		return ModuleCarpentry
				.getCarpentryBlock(TileEntityMetadata.getTileMetadata(world, x, y, z));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int colorMultiplier(IBlockAccess world, int x, int y, int z) {
		CarpentryBlock block = getCarpentryBlock(world, x, y, z);
		return CarpentryBlockRenderer.isSecondLayer() ? block.getSecondaryColour() : block
				.getPrimaryColour();
	}

	public int colorMultiplier(int meta) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(meta);
		return CarpentryBlockRenderer.isSecondLayer() ? block.getSecondaryColour() : block
				.getPrimaryColour();
	}

	@Override
	public Icon getIcon(int side, int damage) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(damage);
		Icon icon = CarpentryBlockRenderer.isSecondLayer() ? block
				.getSecondaryIcon(RENDER_DIRECTIONS[side]) : block
				.getPrimaryIcon(RENDER_DIRECTIONS[side]);
		return icon;
	}

	@Override
	public void getBlockTooltip(ItemStack stack, List par3List) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(TileEntityMetadata
				.getItemDamage(stack));
		if (block.getPrimaryWood() != block.getSecondaryWood())
			par3List.add(block.getPrimaryWood().getName() + " and "
					+ block.getSecondaryWood().getName());
		else
			par3List.add(block.getPrimaryWood().getName());
	}

	public static final ForgeDirection[] RENDER_DIRECTIONS = { ForgeDirection.DOWN,
			ForgeDirection.UP, ForgeDirection.EAST, ForgeDirection.WEST, ForgeDirection.NORTH,
			ForgeDirection.SOUTH };

	public int primaryColor(int damage) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(damage);
		return block.getPrimaryColour();
	}

	public int secondaryColor(int damage) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(damage);
		return block.getSecondaryColour();
	}

	public ItemStack getItemStack(int plank1, int plank2, int design) {
		return TileEntityMetadata.getItemStack(blockID, getMetadata(plank1, plank2, design));
	}

	public static int getMetadata(int plank1, int plank2, int design) {
		return plank1 + (plank2 << 9) + (design << 18);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		for (EnumPattern pattern : EnumPattern.values()) {
			pattern.registerIcons(register);
		}
	}

	@Override
	public int getDroppedMeta(int blockMeta, int tileMeta) {
		CarpentryBlock block = ModuleCarpentry.getCarpentryBlock(tileMeta);
		return block.getItemMetadata();
	}


}
