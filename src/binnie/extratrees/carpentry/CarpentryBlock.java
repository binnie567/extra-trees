package binnie.extratrees.carpentry;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import binnie.extratrees.api.ICarpentryWood;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.ILayout;
import binnie.extratrees.api.IToolHammer;
import binnie.extratrees.block.PlankType;
import binnie.extratrees.carpentry.ModuleCarpentry.Axis;

public class CarpentryBlock {

	IDesign design;

	ICarpentryWood primaryWood;
	ICarpentryWood secondaryWood;

	int rotation = 0;
	ForgeDirection facing = ForgeDirection.UP;
	
	public String toString() {
		return super.toString() + " { design:" + design + " }, { primary:" + primaryWood + " }, { secondary:" +
				secondaryWood + " }, { rotation:" + rotation + " }, { facing:" + facing + " }";
	}

	public IDesign getDesign() {
		return design;
	}

	public ICarpentryWood getPrimaryWood() {
		return primaryWood;
	}

	public ICarpentryWood getSecondaryWood() {
		return secondaryWood;
	}

	CarpentryBlock(ICarpentryWood primaryWood, ICarpentryWood secondaryWood, IDesign design,
			int rotation, ForgeDirection dir) {
		super();
		this.design = design;
		this.primaryWood = primaryWood;
		this.secondaryWood = secondaryWood;
		this.rotation = rotation;
		this.facing = dir;
		if(design == null)
			this.design = EnumDesign.Blank;
		if(primaryWood == null)
			this.primaryWood = PlankType.ExtraTreePlanks.Fir;
		if(secondaryWood == null)
			this.secondaryWood = PlankType.ExtraTreePlanks.Fir;
		if(this.rotation > 3 || this.rotation < 0)
			this.rotation = 0;
		if(this.facing == null || this.facing == ForgeDirection.UNKNOWN)
			this.facing = ForgeDirection.UP;
		
	}

	public int getPrimaryColour() {
		return getPrimaryWood().getColour();
	}

	public int getSecondaryColour() {
		return getSecondaryWood().getColour();
	}

	// Dice help!
	/*
	 * 1 - Up 6 - Down 2 - East 5 - West 4 - South 3 - North
	 */

	ForgeDirection getRotation(ForgeDirection dir, Axis axis) {
		if (axis == Axis.Y) {
			switch (dir) {
			case EAST:
				return ForgeDirection.NORTH;
			case NORTH:
				return ForgeDirection.WEST;
			case SOUTH:
				return ForgeDirection.EAST;
			case WEST:
				return ForgeDirection.SOUTH;
			case UP:
			case DOWN:
			case UNKNOWN:
			default:
				return dir;
			}
		} else if (axis == Axis.X) { // X
			switch (dir) {
			case EAST:
				return ForgeDirection.UP;
			case UP:
				return ForgeDirection.WEST;
			case WEST:
				return ForgeDirection.DOWN;
			case DOWN:
				return ForgeDirection.EAST;
			case NORTH:
			case SOUTH:
			case UNKNOWN:
			default:
				return dir;
			}
		} else { // Z
			switch (dir) {
			case SOUTH:
				return ForgeDirection.UP;
			case UP:
				return ForgeDirection.NORTH;
			case NORTH:
				return ForgeDirection.DOWN;
			case DOWN:
				return ForgeDirection.SOUTH;
			case EAST:
			case WEST:
			case UNKNOWN:
			default:
				return dir;
			}
		}

	}

	public ILayout getLayout(ForgeDirection dir) {

		
		// Adjusts so direction corresponds to which direction the face is
		// requested
		dir = dir.getRotation(ForgeDirection.DOWN);

		
		ForgeDirection adjustedDir = dir;

		// System.out.println(getFacing());

		switch (getFacing()) {
		case DOWN:
			adjustedDir = adjustedDir.getRotation(ForgeDirection.EAST);
			adjustedDir = adjustedDir.getRotation(ForgeDirection.EAST);
			break;
		case EAST:
			adjustedDir = adjustedDir.getRotation(ForgeDirection.EAST);
			adjustedDir = adjustedDir.getRotation(ForgeDirection.NORTH);
			break;
		case NORTH:
			adjustedDir = adjustedDir.getRotation(ForgeDirection.EAST);
			adjustedDir = adjustedDir.getRotation(ForgeDirection.SOUTH);
			adjustedDir = adjustedDir.getRotation(ForgeDirection.SOUTH);
			break;
		case SOUTH:
			adjustedDir = adjustedDir.getRotation(ForgeDirection.EAST);
			break;
		case WEST:
			adjustedDir = adjustedDir.getRotation(ForgeDirection.EAST);
			adjustedDir = adjustedDir.getRotation(ForgeDirection.SOUTH);
			break;
		case UP:
		case UNKNOWN:
		default:
			break;

		}

		/*
		 * switch(axis) { case X: //adjustedDir = getRotation(adjustedDir,
		 * Axis.X); //adjustedDir = getRotation(adjustedDir, Axis.Y);
		 * //adjustedDir = getRotation(adjustedDir, Axis.Y); //adjustedDir =
		 * getRotation(adjustedDir, Axis.Y); break; case Z: //adjustedDir =
		 * getRotation(adjustedDir, Axis.Z); break; case Y: break; }
		 */

		// Account for rotation
		for (int i = 0; i < rotation; i++)
			adjustedDir = adjustedDir.getRotation(ForgeDirection.DOWN);

		ILayout layout;
		switch (adjustedDir) {
		case EAST:
			layout = getDesign().getEastPattern();
			break;
		case NORTH:
			layout = getDesign().getNorthPattern();
			break;
		case SOUTH:
			layout = getDesign().getSouthPattern();
			break;
		case WEST:
			layout = getDesign().getWestPattern();
			break;
		case DOWN:
			layout = getDesign().getBottomPattern();
			break;
		case UP:
		default:
			layout = getDesign().getTopPattern();
			break;
		}

		switch (getFacing()) {
		case UP:
			if (dir == ForgeDirection.DOWN || dir == ForgeDirection.UP) {
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateRight();
			}
			break;
		case DOWN:
			switch (dir) {
			case DOWN: case UP: layout = layout.flipVertical(); break;
			case NORTH: case EAST: case WEST: case SOUTH:
				layout = layout.rotateRight().rotateRight(); break;
			default:
				break;
			}
			if (dir == ForgeDirection.DOWN || dir == ForgeDirection.UP) {
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateLeft();
			}
			break;
		case EAST:
			
			switch (dir) {
			case UP:
			case SOUTH:
				layout = layout.rotateRight(); break;
			case NORTH:
				layout = layout.rotateLeft(); break;
			case DOWN:
				layout = layout.rotateLeft().flipHorizontal(); break;
			case WEST:
				layout = layout.flipHorizontal(); break;
			default:
				break;
			}
			if (dir == ForgeDirection.EAST) {
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateRight();
			}
			if (dir == ForgeDirection.WEST) {
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateLeft();
			}
			break;
		case WEST:
			switch (dir) {
			case NORTH:
				layout = layout.rotateRight(); break;
			case UP:
			case SOUTH:
				layout = layout.rotateLeft(); break;
			case DOWN:
				layout = layout.rotateLeft().flipVertical(); break;
			case EAST:
				layout = layout.flipHorizontal();
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateLeft();
				break;
			case WEST:
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateRight();
			default:
				break;
			}
			break;
		case NORTH:
			switch (dir) {
			case WEST: layout = layout.rotateLeft(); break;
			case EAST: layout = layout.rotateRight(); break;
			case DOWN: layout = layout.flipHorizontal(); break;
			case SOUTH: layout = layout.flipHorizontal(); 
			for (int i = 0; i < rotation; i++)
				layout = layout.rotateLeft();
			break;
			case NORTH:; 
			for (int i = 0; i < rotation; i++)
				layout = layout.rotateRight();
			break;
			default:
				break;
			}
			break;
		case SOUTH:
			switch (dir) {
			case EAST: layout = layout.rotateLeft(); break;
			case WEST: layout = layout.rotateRight(); break;
			case UP: layout = layout.rotateRight().rotateRight(); break;
			case DOWN: layout = layout.flipVertical(); break;
			case NORTH: 
				layout = layout.flipHorizontal();
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateLeft();
			break;
			case SOUTH:
				for (int i = 0; i < rotation; i++)
					layout = layout.rotateRight();
				break;
			default:
				break;
			}
			break;
			
		default:
			break;
		}

		return layout;
	}

	public Icon getPrimaryIcon(ForgeDirection dir) {
		ILayout l = getLayout(dir);
		return l == null ? null : l.getPrimaryIcon();
	}

	public Icon getSecondaryIcon(ForgeDirection dir) {
		ILayout l = getLayout(dir);
		return l == null ? null : l.getSecondaryIcon();
	}

	public Icon getIcon(boolean secondary, ForgeDirection dir) {
		return secondary ? getSecondaryIcon(dir) : getPrimaryIcon(dir);
	}

	public ForgeDirection getFacing() {
		return facing;
	}

	public int getRotation() {
		return rotation;
	}

	public void rotate(int face, ItemStack hammer, EntityPlayer player, World world, int x, int y, int z) {

		ForgeDirection dir = ForgeDirection.getOrientation(face);

		IToolHammer hammerI = (IToolHammer) hammer.getItem();
		
		if (player.isSneaking())
			if(panel) {
				ForgeDirection newFacing = getFacing();
				do {
					newFacing = ForgeDirection.getOrientation(newFacing.ordinal()+1);
					if(newFacing == ForgeDirection.UNKNOWN) newFacing = ForgeDirection.DOWN;
				} while(newFacing != getFacing() && !BlockCarpentryPanel.isValidPanelPlacement(world, x, y, z, newFacing));
				
				if(newFacing != getFacing())
					hammerI.onHammerUsed(hammer, player);
				setFacing(newFacing);
			}
			else {
				if(dir != getFacing())
					hammerI.onHammerUsed(hammer, player);
				setFacing(dir);
			}
			
		else {
			rotation++;
			hammerI.onHammerUsed(hammer, player);
		}
		if (rotation > 3)
			rotation = 0;
		if (rotation < 0)
			rotation = 3;
		
		/*
		 * int newAxis = face / 2;
		 * 
		 * if(sneaking) { if(panel) { int validAxis = this.getAxis(); boolean
		 * validFlipped = this.getFlipped() == 1; int attempts = 0; do {
		 * if(validFlipped) { validAxis++; if(validAxis > 2) validAxis = 0; }
		 * validFlipped = !validFlipped; attempts++; } while(attempts < 10 &&
		 * !BlockCarpentryPanel.isValidPanelPlacement(world, x, y, z, validAxis,
		 * validFlipped)); if(attempts == 10) {
		 * BlockCarpentryPanel.dropInvalidBlock(world, x, y, z); return; }
		 * setAxis(validAxis); flipped = (validFlipped ? 1 : 0); } else {
		 * if(this.axis.ordinal() != newAxis) { setAxis(newAxis); return; }
		 * if(face == 1 || face == 2 || face == 5) rotation++; else rotation--;
		 * } } else { if(this.axis.ordinal() == newAxis) { if(face == 1 || face
		 * == 2 || face == 5) rotation++; else rotation--; } }
		 * 
		 * if(rotation>3) rotation = 0; if(rotation<0) rotation = 3;
		 */
	}

	public void setFacing(ForgeDirection facing) {
		this.facing = facing;
	}

	public int getBlockMetadata() {
		return ModuleCarpentry.getBlockMetadata(this);
	}

	public int getItemMetadata() {
		return ModuleCarpentry.getItemMetadata(this);
	}

	boolean panel = false;

	public void setPanel() {
		panel = true;
	}
	
	public String getString() {
		String type = "";
		if (getPrimaryWood() != getSecondaryWood())
			type = getPrimaryWood().getName() + " and "
					+ getSecondaryWood().getName();
		else
			type = getPrimaryWood().getName();
		
		return super.toString() + " " + "{" + type + " " + getDesign().getName() + " " + 
		(panel ? "Panel" : "Tile") + ", Facing:" + this.getFacing().toString() + ", Rotation:" + getRotation() + "}";
			
	}

}

/*
 * If south face is inverted, west faces disappears (only showing secondary)
 */
