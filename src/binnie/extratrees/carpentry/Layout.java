package binnie.extratrees.carpentry;

import net.minecraft.util.Icon;
import binnie.extratrees.api.ILayout;
import binnie.extratrees.api.IPattern;

public class Layout implements ILayout {
	
	IPattern pattern;
	boolean inverted;
	
	public IPattern getPattern() { return pattern; };
	public boolean isInverted() { return inverted; };
	
	private Layout(IPattern pattern, boolean inverted) {
		this.pattern = pattern;
		this.inverted = inverted;
	}
	
	private Layout(IPattern pattern) {
		this(pattern, false);
	}
	
	ILayout newLayout(ILayout newLayout) {
		return new Layout(newLayout.getPattern(), inverted ^ newLayout.isInverted());
	}
	
	public ILayout rotateRight() { return rotateLeft().rotateLeft().rotateLeft(); }
	public ILayout rotateLeft() { return newLayout(pattern.getRotation()); }
	public ILayout flipHorizontal() { return newLayout(pattern.getHorizontalFlip()); }
	public ILayout flipVertical() { return newLayout(pattern.getHorizontalFlip().rotateLeft().rotateLeft()); }
	
	public Icon getPrimaryIcon() {
		return inverted ? pattern.getSecondaryIcon() : pattern.getPrimaryIcon();
	}
	
	public Icon getSecondaryIcon() {
		return inverted ? pattern.getPrimaryIcon() : pattern.getSecondaryIcon();
	}
	public ILayout invert() {
		return new Layout(this.pattern, !this.inverted);
	}
	
	public static ILayout get(IPattern pattern, boolean inverted) {
		return new Layout(pattern, inverted);
	}
	public static ILayout get(IPattern pattern) {
		return new Layout(pattern, false);
	}
}
