package binnie.extratrees.carpentry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.common.MinecraftForge;
import binnie.core.BinnieCore;
import binnie.core.block.ItemMetadata;
import binnie.core.block.TileEntityMetadata;
import binnie.core.plugin.IBinnieModule;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.PluginExtraTrees;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.api.ICarpentryWood;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.config.ConfigurationMain;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModuleCarpentry implements IBinnieModule {

	static {
		CarpentryManager.carpentryInterface = new CarpentryInterface();
	}
	
	@Override
	public void preInit() {
		
		ExtraTrees.blockCarpentry = new BlockCarpentry(ConfigurationMain.tileID);
		ExtraTrees.blockPanel = new BlockCarpentryPanel(ConfigurationMain.panelID);
		
		GameRegistry.registerBlock(ExtraTrees.blockCarpentry, ItemMetadata.class,
				"extratrees.block.carpentry");
		GameRegistry.registerBlock(ExtraTrees.blockPanel, ItemMetadata.class,
				"extratrees.block.panel");
		
		BinnieCore.proxy.registerCustomItemRenderer(
				Item.itemsList[ExtraTrees.blockCarpentry.blockID].itemID,
				new CarpentryItemRenderer());
		MinecraftForge.EVENT_BUS.register(ExtraTrees.blockCarpentry);
		

		BinnieCore.proxy.registerCustomItemRenderer(
				Item.itemsList[ExtraTrees.blockPanel.blockID].itemID,
				new CarpentryItemRenderer());
		
	}

	@Override
	public void doInit() {
		PluginExtraTrees.carpentryID = RenderingRegistry.getNextAvailableRenderId();
		RenderingRegistry.registerBlockHandler(new CarpentryBlockRenderer());
	}

	@Override
	public void postInit() {
		for (EnumDesign design : EnumDesign.values())
			CarpentryManager.carpentryInterface.registerDesign(design.ordinal(), design);
	}

	
	
	
	
	public static ItemStack getItemStack(BlockCarpentry block, ICarpentryWood type1, ICarpentryWood type2, IDesign design) {
		return getItemStack(block, CarpentryManager.carpentryInterface.getCarpentryWoodIndex(type1),
				CarpentryManager.carpentryInterface.getCarpentryWoodIndex(type2),
				CarpentryManager.carpentryInterface.getDesignIndex(design));
	}
	
	public static ItemStack getItemStack(BlockCarpentry block, int type1, int type2, int design) {
		return TileEntityMetadata.getItemStack(block.blockID, 
				getMetadata(type1, type2, design, 0, ForgeDirection.UP.ordinal()));
	}
	
	public static ItemStack getItemStack(BlockCarpentry blockC, CarpentryBlock block) {
		return getItemStack(blockC, block.getPrimaryWood(), block.getSecondaryWood(), 
				block.getDesign());
	}
	
	enum Axis {
		Y, X, Z;
	}
	
	public static int getMetadata(int plank1, int plank2, int design, int rotation, int facing) {
		return plank1 + (plank2 << 8) + (design << 16) + (rotation << 26) + (facing << 28);
	}
	
	public static CarpentryBlock getCarpentryBlock(int meta) {
		int damage = meta;
		int plankID1 = damage & 0xFF;
		int plankID2 = (damage >> 8) & 0xFF;
		int tile = (damage >> 16) & 0x3FF; 
		int rotation = (damage >> 26) & 0x3;
		int axis = (damage >> 28) & 0x7;
		ICarpentryWood type1 = CarpentryManager.carpentryInterface.getCarpentryWood(plankID1);
		ICarpentryWood type2 = CarpentryManager.carpentryInterface.getCarpentryWood(plankID2);
		IDesign type = CarpentryManager.carpentryInterface.getDesign(tile);
		return new CarpentryBlock(type1, type2, type, rotation, ForgeDirection.getOrientation(axis));
	}
	
	public static CarpentryBlock getCarpentryPanel(int meta) {
		CarpentryBlock block = getCarpentryBlock(meta);
		block.setPanel();
		return block;
	}
	
	public static int getBlockMetadata(CarpentryBlock block) {
		int plank1 = CarpentryManager.carpentryInterface.getCarpentryWoodIndex(block.getPrimaryWood());
		int plank2 = CarpentryManager.carpentryInterface.getCarpentryWoodIndex(block.getSecondaryWood());
		int design = CarpentryManager.carpentryInterface.getDesignIndex(block.getDesign());
		int rotation = block.getRotation();
		int facing = block.getFacing().ordinal();
		return getMetadata(plank1, plank2, design, rotation, facing);
	}
	
	public static int getItemMetadata(CarpentryBlock block) {
		int plank1 = CarpentryManager.carpentryInterface.getCarpentryWoodIndex(block.getPrimaryWood());
		int plank2 = CarpentryManager.carpentryInterface.getCarpentryWoodIndex(block.getSecondaryWood());
		int design = CarpentryManager.carpentryInterface.getDesignIndex(block.getDesign());
		return getMetadata(plank1, plank2, design, 0, ForgeDirection.UP.ordinal());
	}
	
	
	

}




/*
 * IPattern
 * Represents a pair of icons, representing a pattern
 * 
 * Layout
 * Represents a combination of a design and a boolean if the colours are inverted
 * 
 * Design
 * Represents a group of layouts for a block.
 * 
 * Carpentry Block
 * Represents an instance of a design, with two woods
 * 
 * 
 * 
 * Metadata is 32 bit number
 * 
 * 12 possible orientations of the block
 * Stored in 4 bits
 * 
 * 9 bits for a type of wood
 * 512
 * 
 * 10 bits for a pattern (prehaps 9 to allow panelling with 5 bit orientation)
 * 1024
 * 
 * 
 * 8 bit wood 1
 * 8 bit wood 2
 * 10 bit pattern
 * 2 bit rotation
 * 3 bit facing
 * 
*/
