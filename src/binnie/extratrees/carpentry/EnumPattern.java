package binnie.extratrees.carpentry;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.api.ILayout;
import binnie.extratrees.api.IPattern;

public enum EnumPattern implements IPattern {

	Top, Bottom, North, East, South, West,

	// Orthogonal Patterns:
	Blank, Octagon, Diamond, Ringed, Squared, Multiply, Circle, Plus,  
	
	Creeper, Creeper2, Creeper3, Creeper4,
	

	// Striped Patterns:
	Halved, Halved2,
	Striped, Striped2,
	ThinStriped, ThinStriped2,

	// Chequered Patterns
	Chequered, Tiled,

	// Corner Patterns
	VeryThinCorner, VeryThinCorner2, VeryThinCorner3, VeryThinCorner4,
	ThinCorner, ThinCorner2, ThinCorner3, ThinCorner4,
	Corner, Corner2, Corner3, Corner4,
	ThickCorner, ThickCorner2, ThickCorner3, ThickCorner4,
	
	ThinCurvedCorner, ThinCurvedCorner2, ThinCurvedCorner3, ThinCurvedCorner4,
	CurvedCorner, CurvedCorner2, CurvedCorner3, CurvedCorner4,
	

	// Edged Patterns
	Edged, Edged2, Edged3, Edged4,
	ThinEdged, ThinEdged2, ThinEdged3, ThinEdged4,

	// Barred Patterns
	ThinBarred, ThinBarred2,
	Barred, Barred2,
	ThickBarred, ThickBarred2,
	
	BarredEnd, BarredEnd2, BarredEnd3, BarredEnd4,
	
	OrnateBarred, OrnateBarred2,
	OrnateThinBarred, OrnateThinBarred2,
	
	SplitBarred, SplitBarred2,
	SplitBarredCorner, SplitBarredCorner2, SplitBarredCorner3, SplitBarredCorner4,
	SplitBarredTSection, SplitBarredTSection2, SplitBarredTSection3, SplitBarredTSection4,
	SplitCrossed,
	SplitBarredEnd, SplitBarredEnd2, SplitBarredEnd3, SplitBarredEnd4,
	
	
	// Diagonal Patterns
	Diagonal, Diagonal2,
	ThickDiagonal, ThickDiagonal2,
	
	DiagonalCorner, DiagonalCorner2, DiagonalCorner3, DiagonalCorner4,
	DiagonalTSection, DiagonalTSection2, DiagonalTSection3, DiagonalTSection4,
	
	DiagonalCurvedCorner, DiagonalCurvedCorner2, DiagonalCurvedCorner3, DiagonalCurvedCorner4,
	DiagonalCurvedTSection, DiagonalCurvedTSection2, DiagonalCurvedTSection3, DiagonalCurvedTSection4,
	
	DiagonalStriped, DiagonalStriped2,
	ThinDiagonalStriped, ThinDiagonalStriped2,
	
	// Saltires
	ThinSaltire, Saltire, ThickSaltire,
	
	// Crosses
	ThinCrossed, Crossed, ThickCrossed,
	
	ThinCurvedCrossed,
	
	// T Sections
	ThinTSection, ThinTSection2, ThinTSection3, ThinTSection4,
	TSection, TSection2, TSection3, TSection4,
	ThickTSection, ThickTSection2, ThickTSection3, ThickTSection4,
	
	ThinCurvedTSection, ThinCurvedTSection2, ThinCurvedTSection3, ThinCurvedTSection4,
	CurvedTSection, CurvedTSection2, CurvedTSection3, CurvedTSection4,
	
	
	// Barred Corners
	ThinBarredCorner, ThinBarredCorner2, ThinBarredCorner3, ThinBarredCorner4,
	BarredCorner, BarredCorner2, BarredCorner3, BarredCorner4,
	ThickBarredCorner, ThickBarredCorner2, ThickBarredCorner3, ThickBarredCorner4,
	
	ThinCurvedBarredCorner, ThinCurvedBarredCorner2, ThinCurvedBarredCorner3, ThinCurvedBarredCorner4,
	BarredCurvedCorner, BarredCurvedCorner2, BarredCurvedCorner3, BarredCurvedCorner4,
	
	
	// StripedCorner
	ThinStripedCorner, ThinStripedCorner2, ThinStripedCorner3, ThinStripedCorner4,
	StripedCorner, StripedCorner2, StripedCorner3, StripedCorner4,
	
	OrnateStripedCorner, OrnateStripedCorner2, OrnateStripedCorner3, OrnateStripedCorner4,
	
	// Emblems
	Emblem1, Emblem2, Emblem3, Emblem4, Emblem5,
	
	// Letters
	LetterA, LetterA2, LetterA3, LetterA4,
	LetterB, LetterB2, LetterB3, LetterB4,
	LetterF, LetterF2, LetterF3, LetterF4,
	LetterS, LetterS2, LetterS3, LetterS4,
	LetterT, LetterT2, LetterT3, LetterT4,
	
	
	DiagonalHalved, DiagonalHalved2,
	
	Diagonal1Edged, Diagonal1Edged2, Diagonal1Edged3, Diagonal1Edged4,
	Diagonal2Edged, Diagonal2Edged2,
	
	ThickDiagonal1Edged, ThickDiagonal1Edged2, ThickDiagonal1Edged3, ThickDiagonal1Edged4,
	
	ThinBarredEnd, ThinBarredEnd2, ThinBarredEnd3, ThinBarredEnd4,
	ThickBarredEnd, ThickBarredEnd2, ThickBarredEnd3, ThickBarredEnd4,
	
	OverlappedSplitBarred, OverlappedSplitBarred2,
	OverlappedBarred, OverlappedBarred2,
	;

	;

	static void setupStripedPattern(EnumPattern vert, EnumPattern hori) {
		vert.setLeftRotation(hori, true);
		hori.setLeftRotation(vert, false);
		vert.setHorizontalFlip(vert, true);
	}

	static void setupChequeredPattern(EnumPattern cheq) {
		cheq.setLeftRotation(cheq, true);
		cheq.setHorizontalFlip(cheq, true);
	}

	static void setupCornerPattern(EnumPattern tl, EnumPattern tr,
			EnumPattern br, EnumPattern bl) {
		tl.setLeftRotation(bl, false);
		tr.setLeftRotation(tl, false);
		br.setLeftRotation(tr, false);
		bl.setLeftRotation(br, false);
		tl.setHorizontalFlip(tr, false);
		bl.setHorizontalFlip(br, false);
	}
	
	static void setupInvert2Rot(EnumPattern a, EnumPattern b) {
		a.setLeftRotation(b, true);
		b.setLeftRotation(a, false);
		a.setHorizontalFlip(b, false);
	}
	
	static void set2Rotation(EnumPattern a, EnumPattern b) {
		a.setLeftRotation(b, false);
		b.setLeftRotation(a, false);
		a.setHorizontalFlip(b, false);
	}

	static void setupBarredPattern(EnumPattern vert, EnumPattern hori) {
		vert.setLeftRotation(hori, false);
		hori.setLeftRotation(vert, false);
	}

	static void setupEdgedPattern(EnumPattern l, EnumPattern t, EnumPattern r,
			EnumPattern b) {
		l.setLeftRotation(b, false);
		t.setLeftRotation(l, false);
		r.setLeftRotation(t, false);
		b.setLeftRotation(r, false);
		l.setHorizontalFlip(r, false);
	}
	
	static void setupDiagonalPattern(EnumPattern a, EnumPattern b) {
		a.setLeftRotation(b, false);
		b.setLeftRotation(a, false);
		a.setHorizontalFlip(b, false);
	}
	
	
	static void setupBarredPattern(EnumPattern l, EnumPattern t, EnumPattern r,
			EnumPattern b) {
		l.setLeftRotation(b, false);
		t.setLeftRotation(l, false);
		r.setLeftRotation(t, false);
		b.setLeftRotation(r, false);
		l.setHorizontalFlip(r, false);
	}
	
	static void setupTSectionPattern(EnumPattern l, EnumPattern t, EnumPattern r, EnumPattern b) {
		setupEdgedPattern(l, t, r, b);
	}

	static {
		// Striped
		setupStripedPattern(ThinStriped, ThinStriped2);
		setupStripedPattern(Striped, Striped2);
		setupStripedPattern(Halved, Halved2);

		// Chequered
		setupChequeredPattern(Chequered);
		setupChequeredPattern(Tiled);

		// Corner
		setupCornerPattern(VeryThinCorner, VeryThinCorner2, VeryThinCorner3,
				VeryThinCorner4);
		setupCornerPattern(ThinCorner, ThinCorner2, ThinCorner3, ThinCorner4);
		setupCornerPattern(Corner, Corner2, Corner3, Corner4);
		setupCornerPattern(ThickCorner, ThickCorner2, ThickCorner3,
				ThickCorner4);
		
		setupCornerPattern(ThinCurvedCorner, ThinCurvedCorner2, ThinCurvedCorner3, ThinCurvedCorner4);
		setupCornerPattern(CurvedCorner, CurvedCorner2, CurvedCorner3, CurvedCorner4);
		
		setupBarredPattern(BarredEnd, BarredEnd2, BarredEnd3, BarredEnd4);

		// Edged
		setupEdgedPattern(ThinEdged, ThinEdged2, ThinEdged3, ThinEdged4);
		setupEdgedPattern(Edged, Edged2, Edged3, Edged4);
		
		// Barred
		setupBarredPattern(ThinBarred, ThinBarred2);
		setupBarredPattern(Barred, Barred2);
		setupBarredPattern(ThickBarred, ThickBarred2);
		
		// Diagonal
		setupDiagonalPattern(Diagonal, Diagonal2);
		setupDiagonalPattern(ThickDiagonal, ThickDiagonal2);
 
		// T Section
		setupTSectionPattern(ThinTSection, ThinTSection2, ThinTSection3, ThinTSection4);
		setupTSectionPattern(TSection, TSection2, TSection3, TSection4);
		setupTSectionPattern(ThickTSection, ThickTSection2, ThickTSection3, ThickTSection4);
		
		setupTSectionPattern(ThinCurvedTSection, ThinCurvedTSection2, ThinCurvedTSection3, ThinCurvedTSection4);
		setupTSectionPattern(CurvedTSection, CurvedTSection2, CurvedTSection3, CurvedTSection4);
		
		// Barred Corner
		setupCornerPattern(ThinBarredCorner, ThinBarredCorner2, ThinBarredCorner3, ThinBarredCorner4);
		setupCornerPattern(BarredCorner, BarredCorner2, BarredCorner3, BarredCorner4);
		setupCornerPattern(ThickBarredCorner, ThickBarredCorner2, ThickBarredCorner3, ThickBarredCorner4);
		
		setupCornerPattern(ThinCurvedBarredCorner, ThinCurvedBarredCorner2, 
				ThinCurvedBarredCorner3, ThinCurvedBarredCorner4);
		setupCornerPattern(BarredCurvedCorner, BarredCurvedCorner2, 
				BarredCurvedCorner3, BarredCurvedCorner4);
		
		// Striped Corner
		setupCornerPattern(ThinStripedCorner, ThinStripedCorner2, ThinStripedCorner3, ThinStripedCorner4);
		setupCornerPattern(StripedCorner, StripedCorner2, StripedCorner3, StripedCorner4);
		
		setupCornerPattern(OrnateStripedCorner, OrnateStripedCorner2, 
				OrnateStripedCorner3, OrnateStripedCorner4);
		
		setupRotation(LetterA, LetterA2, LetterA3, LetterA4);
		setupRotation(LetterB, LetterB2, LetterB3, LetterB4);
		setupRotation(LetterF, LetterF2, LetterF3, LetterF4);
		setupRotation(LetterS, LetterS2, LetterS3, LetterS4);
		setupRotation(LetterT, LetterT2, LetterT3, LetterT4);
		
		setupEdgedPattern(DiagonalCorner, DiagonalCorner2, DiagonalCorner3, DiagonalCorner4);
		setupCornerPattern(DiagonalTSection, DiagonalTSection2, DiagonalTSection3, DiagonalTSection4);
		
		setupEdgedPattern(DiagonalCurvedCorner, DiagonalCurvedCorner2, DiagonalCurvedCorner3, DiagonalCurvedCorner4);
		setupCornerPattern(DiagonalCurvedTSection, DiagonalCurvedTSection2, DiagonalCurvedTSection3, DiagonalCurvedTSection4);
		
		setupBarredPattern(OrnateBarred, OrnateBarred2);
		setupBarredPattern(OrnateThinBarred, OrnateThinBarred2);
		
		setupBarredPattern(SplitBarred, SplitBarred2);
		setupCornerPattern(SplitBarredCorner, SplitBarredCorner2, SplitBarredCorner3, SplitBarredCorner4);
		setupTSectionPattern(SplitBarredTSection, SplitBarredTSection2, SplitBarredTSection3, SplitBarredTSection4);
		setupBarredPattern(SplitBarredEnd, SplitBarredEnd2, SplitBarredEnd3, SplitBarredEnd4);

		setupRotation(Creeper, Creeper2, Creeper3, Creeper4);
		
		setupDiagonalPattern(EnumPattern.DiagonalStriped, EnumPattern.DiagonalStriped2);
		setupDiagonalPattern(EnumPattern.ThinDiagonalStriped, EnumPattern.ThinDiagonalStriped2);
		
		setupCornerPattern(Diagonal1Edged, Diagonal1Edged2, Diagonal1Edged3, Diagonal1Edged4);
		
		setupCornerPattern(ThickDiagonal1Edged, ThickDiagonal1Edged2, ThickDiagonal1Edged3, ThickDiagonal1Edged4);
		
		
		setupInvert2Rot(DiagonalHalved, DiagonalHalved2);
		set2Rotation(Diagonal2Edged, Diagonal2Edged2);
		
		
		setupBarredPattern(ThinBarredEnd, ThinBarredEnd2, ThinBarredEnd3, ThinBarredEnd4);
		setupBarredPattern(ThickBarredEnd, ThickBarredEnd2, ThickBarredEnd3, ThickBarredEnd4);
		
		OverlappedBarred.setLeftRotation(OverlappedBarred2, false);
		OverlappedBarred2.setLeftRotation(OverlappedBarred, false);
		
		OverlappedSplitBarred.setLeftRotation(OverlappedSplitBarred2, false);
		OverlappedSplitBarred2.setLeftRotation(OverlappedSplitBarred, false);
	}

	Icon primary;
	Icon secondary;

	@Override
	public Icon getPrimaryIcon() {
		return primary;
	}

	private static void setupRotation(EnumPattern t,
			EnumPattern r, EnumPattern b, EnumPattern l) {
		setupEdgedPattern(l, t, r, b);
	}

	private void setHorizontalFlip(EnumPattern pattern, boolean inverted) {
		this.horizontalFlip = Layout.get(pattern, inverted);
		pattern.horizontalFlip = Layout.get(this, inverted);
	}

	@Override
	public Icon getSecondaryIcon() {
		return secondary;
	}

	ILayout leftRotation = Layout.get(this, false);;
	ILayout horizontalFlip = Layout.get(this, false);

	@Override
	public ILayout getRotation() {
		return leftRotation;
	}

	@Override
	public ILayout getHorizontalFlip() {
		return horizontalFlip;
	}

	protected void setLeftRotation(EnumPattern pattern, boolean inverted) {
		leftRotation = Layout.get(pattern, inverted);
	}

	@Override
	public void registerIcons(IconRegister register) {
		primary = ExtraTrees.proxy.getIcon(register, "patterns/"
				+ this.toString().toLowerCase() + ".0");
		secondary = ExtraTrees.proxy.getIcon(register, "patterns/"
				+ this.toString().toLowerCase() + ".1");
	}
	
	public ILayout layout() {
		return layout(false);
	}

	public ILayout layout(boolean invert) {
		return Layout.get(this, invert);
	}

}
