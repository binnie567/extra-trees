package binnie.extratrees.carpentry;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;

import binnie.extratrees.PluginExtraTrees;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class CarpentryBlockRenderer implements ISimpleBlockRenderingHandler {
	
	public static CarpentryBlockRenderer instance;
	
	public CarpentryBlockRenderer() {
		instance = this;
	}

	private void setColour(Tessellator tess, int colour) {
		float var6 = (float) (colour >> 16 & 255) / 255.0F;
		float var7 = (float) (colour >> 8 & 255) / 255.0F;
		float var8 = (float) (colour & 255) / 255.0F;
		GL11.glColor3f(var6, var7, var8);
	}

	static boolean secondLayer = false;

	public static boolean isSecondLayer() {
		return secondLayer;
	};

	@Override
	public void renderInventoryBlock(Block block2, int meta, int modelID,
			RenderBlocks renderer) {
		BlockCarpentry block = (BlockCarpentry) block2;
		block.setBlockBoundsForItemRender();
		renderer.setRenderBoundsFromBlock(block);
		GL11.glTranslatef(-0.5f, -0.5f, -0.5f);
		renderItem(block, renderer, meta);
		secondLayer = true;
		renderItem(block, renderer, meta);
		secondLayer = false;

	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
			Block block, int modelId, RenderBlocks renderer) {
		renderer.renderStandardBlock(block, x, y, z);
		secondLayer = true;
		renderer.renderStandardBlock(block, x, y, z);
		secondLayer = false;
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory() {
		return true;
	}

	@Override
	public int getRenderId() {
		return PluginExtraTrees.carpentryID;
	}

	public void renderItem(BlockCarpentry block, RenderBlocks renderer, int meta) {
		
		setColor(block.colorMultiplier(meta));
		
		Tessellator tessellator = Tessellator.instance;
		
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, -1.0F, 0.0F);
		renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D,
				renderer.getBlockIconFromSideAndMetadata(block, 0, meta));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D,
				renderer.getBlockIconFromSideAndMetadata(block, 1, meta));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, -1.0F);
		renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D,
				renderer.getBlockIconFromSideAndMetadata(block, 2, meta));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, 1.0F);
		renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D,
				renderer.getBlockIconFromSideAndMetadata(block, 3, meta));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(-1.0F, 0.0F, 0.0F);
		renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D,
				renderer.getBlockIconFromSideAndMetadata(block, 4, meta));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(1.0F, 0.0F, 0.0F);
		renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D,
				renderer.getBlockIconFromSideAndMetadata(block, 5, meta));
		tessellator.draw();
	}
	
	public void setColor(int l) {
		 float f = (float)(l >> 16 & 255) / 255.0F;
        float f1 = (float)(l >> 8 & 255) / 255.0F;
        float f2 = (float)(l & 255) / 255.0F;
        GL11.glColor3f(f, f1, f2);
	}

}
