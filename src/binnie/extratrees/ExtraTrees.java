package binnie.extratrees;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

import binnie.core.BinnieCoreMod;
import binnie.core.gui.IBinnieGUID;
import binnie.core.item.ItemMisc;
import binnie.core.plugin.IBinnieModule;
import binnie.core.proxy.IProxyCore;
import binnie.core.resource.IBinnieTexture;
import binnie.extratrees.block.ModuleBlocks;
import binnie.extratrees.carpentry.ModuleCarpentry;
import binnie.extratrees.config.ConfigurationMain;
import binnie.extratrees.core.ExtraTreeTexture;
import binnie.extratrees.core.ExtraTreesGUID;
import binnie.extratrees.core.ModuleCore;
import binnie.extratrees.genetics.ModuleGenetics;
import binnie.extratrees.item.ModuleItems;
import binnie.extratrees.machines.ModuleMachine;
import binnie.extratrees.proxy.Proxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = "ExtraTrees", name = "Extra Trees", version = "@VERSION@", dependencies = "after:BinnieCore")
@NetworkMod(clientSideRequired = true, serverSideRequired = true)
public class ExtraTrees extends BinnieCoreMod {

	@Instance("ExtraTrees")
	public static ExtraTrees instance;

	@SidedProxy(clientSide = "binnie.extratrees.proxy.ProxyClient", serverSide = "binnie.extratrees.proxy.ProxyServer")
	public static Proxy proxy;

	public ExtraTrees() {
		super();
		ExtraTrees.instance = this;
	};

	@Override
	public IBinnieGUID[] getGUIDs() {
		return ExtraTreesGUID.values();
	}

	@Override
	public IBinnieTexture[] getTextures() {
		return ExtraTreeTexture.values();
	}

	@Override
	public Class[] getConfigs() {
		return new Class[] {ConfigurationMain.class};
	}

	@Override
	public String getChannel() {
		return "ET";
	}

	@Override
	public IProxyCore getProxy() {
		return proxy;
	}

	@Override
	public String getId() {
		return "extratrees";
	}
	
	@Override
	public void registerModules(List<IBinnieModule> list) {
		list.add(new ModuleBlocks());
		list.add(new ModuleItems());
		list.add(new ModuleGenetics());
		list.add(new ModuleCarpentry());
		list.add(new ModuleMachine());
		list.add(new ModuleCore());
	}
	
	

	public static Item itemDictionary;
	public static ItemMisc itemMisc;
	public static ItemMisc itemFood;

	public static Block blockStairs;

	public static Block blockLog;
	public static Block blockCarpentry;
	public static Block blockPlanks;
	public static Block blockMachine;
	public static Block blockFence;
	public static Block blockPanel;
	
	public static Block blockSlab;
	public static Block blockDoubleSlab;
	public static Item itemHammer;
	public static Item itemDurableHammer;
	public static Block blockGate;
	public static Block blockDoor;

}
