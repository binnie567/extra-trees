package binnie.craftgui.extratrees.dictionary;

import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.minecraft.MinecraftGUI;
import binnie.craftgui.mod.database.ControlIndividualDisplay;
import binnie.craftgui.mod.database.DatabaseTab;
import binnie.craftgui.mod.database.EnumDiscoveryState;
import binnie.craftgui.mod.database.PageSpecies;
import binnie.craftgui.window.Panel;
import forestry.api.genetics.IAlleleSpecies;

public class PageSpeciesImage extends PageSpecies {

	ControlIndividualDisplay display;
	
	public PageSpeciesImage(IWidget parent, DatabaseTab tab) {
		super(parent, tab);
		new Panel(this, 7, 25, 130, 120, MinecraftGUI.PanelType.Gray);
		display = new ControlIndividualDisplay(this, 12, 25, 120);
		display.hastooltip = false;
		new ControlTextCentered(this, 8, "Specimen");
	}

	@Override
	public void onSpeciesChanged(IAlleleSpecies species) {
		display.setSpecies(species, EnumDiscoveryState.Show);
	}

}
