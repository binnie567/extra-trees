package binnie.craftgui.extratrees.dictionary;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import binnie.core.IBinnieMod;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.minecraft.ControlEnergyBar;
import binnie.craftgui.minecraft.ControlErrorState;
import binnie.craftgui.minecraft.ControlLiquidTank;
import binnie.craftgui.minecraft.ControlPlayerInventory;
import binnie.craftgui.minecraft.ControlSlot;
import binnie.craftgui.minecraft.Window;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.core.ExtraTreeTexture;
import binnie.extratrees.machines.Lumbermill;
import cpw.mods.fml.relauncher.Side;

public class WindowLumbermill extends Window {

	public WindowLumbermill(EntityPlayer player,
			IInventory inventory, Side side) {
		super(220, 192, player, inventory, side);
	}

	@Override
	protected IBinnieMod getMod() {
		return ExtraTrees.instance;
	}

	@Override
	protected String getName() {
		return "Lumbermill";
	}


	@Override
	public void initialize() {
		setTitle("Lumbermill");
		new ControlSlot(this, 42, 43).create(this.getInventory(), Lumbermill.slotWood);
		new ControlSlot(this, 148, 43).create(getInventory(), Lumbermill.slotPlanks);
		new ControlSlot(this, 172, 28).create(getInventory(), Lumbermill.slotBark);
		new ControlSlot(this, 172, 58).create(getInventory(), Lumbermill.slotSawdust);
		new ControlLumbermillProgress(this, 70, 43);
		new ControlLiquidTank(this, 16, 22);
		new ControlEnergyBar(this, 8, 112, 16, 60, Direction.Upwards);
		new ControlPlayerInventory(this);
		new ControlErrorState(this, 95, 73);
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		return new WindowLumbermill(player, inventory, side);
	}
	
}
