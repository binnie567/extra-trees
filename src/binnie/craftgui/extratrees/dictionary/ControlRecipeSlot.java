package binnie.craftgui.extratrees.dictionary;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import binnie.core.machines.Machine;
import binnie.core.machines.TileEntityMachine;
import binnie.core.machines.component.IComponentRecipe;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.minecraft.ControlSlotBase;
import binnie.craftgui.minecraft.Window;

public class ControlRecipeSlot extends ControlSlotBase {
	
	 @EventHandler(origin=Origin.Self)
     public void onMouseClick(EventMouse.Down event) {
             
             TileEntity tile = (TileEntity) Window.get(this).getInventory();
             
             if(tile == null || !(tile instanceof TileEntityMachine))
                     return;
             
             NBTTagCompound nbt = new NBTTagCompound("recipe");
             nbt.setString("R", "");
             Window.get(this).sendClientAction(nbt);

     }
	
	public ControlRecipeSlot(IWidget parent, int x, int y) {
		super(parent, x, y, 50);
		
	}

	public ItemStack getItemStack() {
		IComponentRecipe recipe = Machine.getInterface(IComponentRecipe.class, Window.get(this).getInventory());
		return recipe.isRecipe() ? recipe.getProduct() : null;
	}

}
