package binnie.craftgui.extratrees.dictionary;

import net.minecraft.entity.player.EntityPlayer;
import binnie.core.IBinnieMod;
import binnie.core.genetics.BinnieGenetics;
import binnie.craftgui.minecraft.Window;
import binnie.craftgui.mod.database.DatabaseTab;
import binnie.craftgui.mod.database.PageBranchOverview;
import binnie.craftgui.mod.database.PageBranchSpecies;
import binnie.craftgui.mod.database.PageSpeciesClassification;
import binnie.craftgui.mod.database.PageSpeciesMutations;
import binnie.craftgui.mod.database.PageSpeciesOverview;
import binnie.craftgui.mod.database.PageSpeciesResultant;
import binnie.craftgui.mod.database.WindowAbstractDatabase;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.core.ExtraTreeTexture;
import cpw.mods.fml.relauncher.Side;

public class WindowMothDictionary extends WindowAbstractDatabase {

	public WindowMothDictionary(EntityPlayer player, Side side, boolean nei) {
		super(player, side, nei, BinnieGenetics.mothBreedingSystem, 160f);
	}

	public static Window create(EntityPlayer player, Side side, boolean nei) {
		return new WindowMothDictionary(player, side, nei);
	}

	@Override
	protected void addTabs() {
		speciesPages.addChild(new PageSpeciesOverview(speciesPages, new DatabaseTab(
				"Overview", 0)));
		speciesPages.addChild(new PageSpeciesClassification(speciesPages, new DatabaseTab(
				"Classification", 0)));
		speciesPages.addChild(new PageSpeciesImage(speciesPages, new DatabaseTab(
				"Specimen", 0)));
		speciesPages.addChild(new PageSpeciesResultant(speciesPages, new DatabaseTab(
				"Resultant Mutations", 0)));
		speciesPages.addChild(new PageSpeciesMutations(speciesPages, new DatabaseTab(
				"Further Muations", 0)));

		branchPages.addChild(new PageBranchOverview(branchPages, new DatabaseTab(
				"Overview", 0)));
		branchPages.addChild(new PageBranchSpecies(branchPages, new DatabaseTab(
				"Species", 0)));
	}
	
	@Override
	protected IBinnieMod getMod() {
		return ExtraTrees.instance;
	}

	@Override
	protected String getName() {
		return "MothDatabase";
	}
	/*
	@Override
	public ResourceLocation getBackgroundTextureFile() {
		return ExtraTrees.instance.getResource(Constants.PathGUI + "MothDatabase");
	}
*/
}
