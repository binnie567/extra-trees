package binnie.craftgui.extratrees.dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import binnie.core.machines.Machine;
import binnie.core.machines.TileEntityMachine;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.controls.scroll.IControlScrollable;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.minecraft.IRendererMinecraft;
import binnie.craftgui.minecraft.Window;
import binnie.craftgui.resource.minecraft.CraftGUITexture;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.IDesignCategory;
import binnie.extratrees.block.PlankType;
import binnie.extratrees.carpentry.BlockCarpentry;
import binnie.extratrees.carpentry.EnumDesign;
import binnie.extratrees.carpentry.ModuleCarpentry;
import binnie.extratrees.machines.Woodworker;
import binnie.extratrees.machines.Woodworker.ComponentWoodworkerRecipe;

public class ControlTileSelect extends Control implements IControlValue<IDesign>,
		IControlScrollable {

	public static class ControlTile extends Control implements IControlValue<IDesign>, ITooltip {

		IDesign value;

		boolean panel;
		protected ControlTile(IWidget parent, float x, float y, IDesign value) {
			super(parent, x, y, 18, 18);
			setValue(value);
			canMouseOver = true;
			panel = Machine.getInterface(ComponentWoodworkerRecipe.class, Window.get(this).getInventory()).panel;
		}

		@Override
		public void getTooltip(Tooltip tooltip) {
			tooltip.add(getValue().getName() + " Pattern");
		}

		@Override
		public IDesign getValue() {
			return value;
		}

		@EventHandler(origin = Origin.Self)
		public void onMouseClick(EventMouse.Down event) {
			TileEntityMachine tile = ((TileEntityMachine) Window.get(this).getInventory());
			if (tile == null)
				return;
			Woodworker.ComponentWoodworkerRecipe recipe = tile.getMachine().getComponent(
					ComponentWoodworkerRecipe.class);
			NBTTagCompound nbt = new NBTTagCompound("design");
			nbt.setShort("D",
					(short) CarpentryManager.carpentryInterface.getDesignIndex(getValue()));
			Window.get(this).sendClientAction(nbt);
		}

		@Override
		public void onRenderBackground() {
			getRenderer().renderTexture(CraftGUITexture.Slot, Vector2f.ZERO);
		}

		@Override
		public void onRenderForeground() {
			ItemStack image = ModuleCarpentry.getItemStack(
					(BlockCarpentry) (panel ? ExtraTrees.blockPanel
							: ExtraTrees.blockCarpentry), PlankType.ExtraTreePlanks.Apple,
					PlankType.VanillaPlanks.BIRCH, getValue());
			getRenderer().subRenderer(IRendererMinecraft.class).renderItem(new Vector2f(1, 1),
					image);
			if (((IControlValue<IDesign>) getParent()).getValue() != getValue()) {
				if (Window.get(this).getMousedOverWidget() == this)
					getRenderer().renderGradientRect(CraftGUIUtil.getPaddedArea(getArea(), 1),
							0x44FFFFFF, 0x44FFFFFF);
				else
					getRenderer().renderGradientRect(CraftGUIUtil.getPaddedArea(getArea(), 1),
							0xAA888888, 0xAA888888);
			}
		}

		@Override
		public void setValue(IDesign value) {
			this.value = value;
		}

	}

	IDesign value = EnumDesign.Blank;

	float shownHeight = 92;

	protected ControlTileSelect(IWidget parent, float x, float y) {
		super(parent, x, y, 102, 20 * (CarpentryManager.carpentryInterface.getSortedDesigns()
				.size() / 4) + 22);
		refresh("");
	}

	@Override
	public float getPercentageIndex() {
		return 0;
	}

	@Override
	public float getPercentageShown() {
		return 0;
	}

	@Override
	public IDesign getValue() {
		return value;
	}

	@Override
	public void movePercentage(float percentage) {
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
		TileEntityMachine tile = ((TileEntityMachine) Window.get(this).getInventory());
		if (tile == null)
			return;
		Woodworker.ComponentWoodworkerRecipe recipe = tile.getMachine().getComponent(
				ComponentWoodworkerRecipe.class);
		setValue(recipe.getDesign());
	}

	public void refresh(String filterText) {
		this.deleteAllChildren();
		int cx = 2;
		int cy = 2;
		
		Map<IDesignCategory, List<IDesign>> designs = new HashMap<IDesignCategory, List<IDesign>>();

		for (IDesignCategory category : CarpentryManager.carpentryInterface
				.getAllDesignCategories()) {
			designs.put(category, new ArrayList<IDesign>());
			for (IDesign tile : category.getDesigns()) {
				if(filterText == "" || tile.getName().toLowerCase().contains(filterText))
					designs.get(category).add(tile);
			}
			if(designs.get(category).isEmpty())
				designs.remove(category);
		}
		
		for (IDesignCategory category : designs.keySet()) {
			cx = 2;
			new ControlText(this, new Vector2f(cx, cy + 3), category.getName());
			cy += 16;
			for (IDesign tile : designs.get(category)) {
				if (cx > 90) {
					cx = 2;
					cy += 20;
				}
				new ControlTile(this, cx, cy, tile);
				cx += 20;
			}
			cy += 20;
		}

		int height = cy;

		this.setSize(new Vector2f(getSize().x(), height));
	}

	@Override
	public void setPercentageIndex(float index) {
	}

	@Override
	public void setValue(IDesign value) {
		this.value = value;
	}

}
