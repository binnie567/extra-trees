package binnie.craftgui.extratrees.dictionary;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import binnie.core.IBinnieMod;
import binnie.core.machines.Machine;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextEdit;
import binnie.craftgui.controls.scroll.ControlScrollableContent;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.minecraft.ControlErrorState;
import binnie.craftgui.minecraft.ControlPlayerInventory;
import binnie.craftgui.minecraft.ControlSlot;
import binnie.craftgui.minecraft.MinecraftGUI;
import binnie.craftgui.minecraft.Window;
import binnie.craftgui.window.Panel;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.core.ExtraTreeTexture;
import binnie.extratrees.machines.Woodworker;
import cpw.mods.fml.relauncher.Side;

public class WindowWoodworker extends Window {

	ControlTextEdit textEdit;
	ControlTileSelect tileSelect;
	
	@Override
	public void initialize() {
		if(((Machine) Machine.getMachine(getInventory())).getPackage() instanceof Woodworker.PackageWoodworker)
			setTitle("Woodworker");
		else 
			setTitle("Panelworker");
		new ControlText(this, new Area(190, 36, 114, 10), "Design", TextJustification.TopCenter).setColour(0x444444);
		
		new Panel(this, 188, 48, 118, 126, MinecraftGUI.PanelType.Gray);
		
		textEdit = new ControlTextEdit(this, 188, 48+126+4, 118);
		
		ControlScrollableContent scroll = new ControlScrollableContent(this, 190, 50, 114, 122, 12);
		// new Panel(this, 0, 0, getSize().x - 12, getSize().y, Panel.Type.Gray);
		// Need to add this before child content
		
		tileSelect = new ControlTileSelect(scroll, 0, 0);
		
		scroll.setScrollableContent(tileSelect);
		
		
		//new ControlText(this, 120, 94, "Preview", Alignment.Center).setColour(0x444444);
		//new ControlPreview(this, 88, 108);
		
		new ControlPlayerInventory(this).setPosition(new Vector2f(14, 96));
		
		new ControlErrorState(this, 76, 65);
		
		if(getInventory() != null) {
			ControlSlot slotWood1 = new ControlSlot(this, 22, 34);
			slotWood1.create(getInventory(), Woodworker.wood1Slot);
			ControlSlot slotWood2 = new ControlSlot(this, 62, 34);
			slotWood2.create(getInventory(), Woodworker.wood2Slot);
			
			ControlSlot slotBeeswax = new ControlSlot(this, 42, 64);
			slotBeeswax.create(getInventory(), Woodworker.beeswaxSlot);
			
			ControlRecipeSlot slotFinished = new ControlRecipeSlot(this, 112, 34);
			//slotFinished.create(getInventory(), Woodworker.finishedSlot);
		}
		
		
	}

	public WindowWoodworker(EntityPlayer player,
			IInventory inventory, Side side) {
		super(320, 216, player, inventory, side);
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		return new WindowWoodworker(player, inventory, side);
	}
	
	@Override
	protected IBinnieMod getMod() {
		return ExtraTrees.instance;
	}

	@Override
	protected String getName() {
		return "Woodworker";
	}
	/*
	@Override
	public ResourceLocation getBackgroundTextureFile() {
		return ExtraTrees.instance.getResource(Constants.PathGUI + "Woodworker");
	}
	*/

	@EventHandler(origin = Origin.DirectChild)
	public void onValueChange(EventValueChanged<String> event) {
		tileSelect.refresh(event.getValue());
	}
	
}
