package binnie.craftgui.extratrees.dictionary;

import binnie.core.genetics.BinnieGenetics;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.mod.database.DatabaseTab;
import binnie.craftgui.mod.database.PageSpecies;
import forestry.api.arboriculture.ITree;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;

public class PageSpeciesTreeGenome extends PageSpecies {

	public PageSpeciesTreeGenome(IWidget parent, DatabaseTab tab) {
		super(parent, tab);
	}

	@Override
	public void onSpeciesChanged(IAlleleSpecies species) {
		this.deleteAllChildren();
		IAllele[] template = BinnieGenetics.getTreeRoot().getTemplate(species.getUID());
		if(template == null) return;
		ITree tree = (ITree) BinnieGenetics.getTreeRoot().templateAsIndividual(template);
		if(tree == null) return;
	}

	public static String tolerated(boolean t) {
		if (t)
			return "Tolerated";
		else
			return "Not Tolerated";

	}

}
