package binnie.craftgui.extratrees.dictionary;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

import org.lwjgl.opengl.GL11;

import binnie.core.machines.Machine;
import binnie.core.machines.power.IProcess;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.minecraft.ControlProgressBase;
import binnie.craftgui.minecraft.IRendererMinecraft;
import binnie.craftgui.minecraft.MinecraftGUI;
import binnie.craftgui.minecraft.Window;
import binnie.craftgui.resource.ITexture;
import binnie.craftgui.resource.minecraft.StandardTexture;
import binnie.craftgui.window.Panel;
import binnie.extratrees.core.ExtraTreeTexture;
import binnie.extratrees.machines.Lumbermill;

public class ControlLumbermillProgress extends ControlProgressBase {

	@Override
	public void onUpdate() {
		IProcess process = Machine.getInterface(IProcess.class, Window.get(this).getInventory());
		if (process == null)
			return;
		float newProgress = (process.getProgress()) / 100f;
		// progress += 0.01f;
		if (newProgress != progress) {
			progress = newProgress;
			animation += 5f;
		}
		if (progress > 1f)
			progress = 0f;

	}

	float progress = 0f;

	float animation = 0;

	static ITexture Saw = new StandardTexture(0, 0, 6, 32, ExtraTreeTexture.Gui);
	static ITexture Saw2 = new StandardTexture(2, 0, 4, 32, ExtraTreeTexture.Gui);

	@Override
	public void onRenderForeground() {
		GL11.glDisable(GL11.GL_LIGHTING);
		int sawX = (int) (63f * progress);

		getRenderer().renderTexture(Saw, new Vector2f(sawX, -8 + 6f * (float) Math.sin(animation)));

		ItemStack item = Window.get(this).getInventory().getStackInSlot(Lumbermill.slotWood);
		if (item == null)
			return;

		GL11.glDisable(GL11.GL_LIGHTING);

		Block block = Block.blocksList[item.getItem().itemID];
		if (block == null)
			return;
		Icon icon = block.getIcon(2, item.getItemDamage());
		for (int i = 0; i < 4; i++) {
			getRenderer().subRenderer(IRendererMinecraft.class).renderBlockIcon(
					new Vector2f(1 + i * 16, 1), icon);
		}

		ItemStack result = Lumbermill.getPlankProduct(item);
		if (result == null)
			return;

		Block block2 = Block.blocksList[result.getItem().itemID];
		if (block2 == null)
			return;
		Icon icon2 = block2.getIcon(2, result.getItemDamage());

		IPosition size = this.getSize();
		IPosition pos = this.getAbsolutePosition();
		getRenderer().limitArea(
				new Area(pos.add(new Vector2f(0f, 0f)), new Vector2f(progress * 64f + 2f, 18)));
		GL11.glEnable(GL11.GL_SCISSOR_TEST);

		for (int i = 0; i < 4; i++) {
			getRenderer().subRenderer(IRendererMinecraft.class).renderBlockIcon(
					new Vector2f(1 + i * 16, 1), icon2);
		}

		GL11.glDisable(GL11.GL_SCISSOR_TEST);

		getRenderer().renderTexture(Saw2,
				new Vector2f(sawX + 2, -8 + 6f * (float) Math.sin(animation)));

		GL11.glEnable(GL11.GL_LIGHTING);

	}

	protected ControlLumbermillProgress(IWidget parent, float x, float y) {
		super(parent, x, y, 66, 18);
		new Panel(this, 0, 0, 66, 18, MinecraftGUI.PanelType.Black);
	}

}
